--daily_rewards
ALTER TABLE daily_rewards
ALTER COLUMN amount SET DATA TYPE numeric(12,3),
ADD COLUMN country_currency TEXT;

--reactivation_rewards
ALTER TABLE reactivation_rewards
ALTER COLUMN amount SET DATA TYPE numeric(12,3),
ADD COLUMN country_currency TEXT;

--rewards
ALTER TABLE rewards
ALTER COLUMN amount SET DATA TYPE numeric(12,3),
ADD COLUMN country_currency TEXT;


