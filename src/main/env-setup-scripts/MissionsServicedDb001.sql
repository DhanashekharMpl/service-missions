DROP TABLE IF EXISTS daily_rewards;

-- Common Functions --
CREATE OR REPLACE FUNCTION trf_update_modified_on_and_created_on()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.created_on = current_timestamp;
    NEW.modified_on = NEW.created_on;
    RETURN NEW;
END
$BODY$;


CREATE OR REPLACE FUNCTION trf_update_created_on()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.created_on = current_timestamp;
    RETURN NEW;
END
$BODY$;


CREATE OR REPLACE FUNCTION trf_update_modified_on()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.modified_on = current_timestamp;
    RETURN NEW;
END
$BODY$;


-- Rewards Table --
CREATE TABLE daily_rewards
(
    id SERIAL NOT NULL,
    mission_id TEXT NOT NULL,
    user_id INTEGER NOT NULL,
    currency TEXT NOT NULL,
    amount INTEGER NOT NULL,
    txn_state TEXT NOT NULL,
    txn_id TEXT,
    created_on TIMESTAMP,
    modified_on TIMESTAMP,

    CONSTRAINT pk_daily_rewards PRIMARY KEY (id),
    CONSTRAINT uk_daily_rewards_mid_uid UNIQUE (user_id, mission_id)

);

CREATE TRIGGER tr_on_insert_daily_rewards
    BEFORE INSERT
    ON daily_rewards
    FOR EACH ROW
    EXECUTE PROCEDURE trf_update_modified_on_and_created_on();

CREATE TRIGGER tr_on_update_daily_rewards
    BEFORE UPDATE
    ON daily_rewards
    FOR EACH ROW
    EXECUTE PROCEDURE trf_update_modified_on();