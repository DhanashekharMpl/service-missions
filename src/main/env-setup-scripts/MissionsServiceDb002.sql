DROP TABLE IF EXISTS reactivation_rewards;

-- Common Functions --
CREATE OR REPLACE FUNCTION trf_update_modified_on_and_created_on()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.created_on = current_timestamp;
    NEW.modified_on = NEW.created_on;
    RETURN NEW;
END
$BODY$;


CREATE OR REPLACE FUNCTION trf_update_created_on()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.created_on = current_timestamp;
    RETURN NEW;
END
$BODY$;


CREATE OR REPLACE FUNCTION trf_update_modified_on()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    NOT LEAKPROOF
AS $BODY$
BEGIN
    NEW.modified_on = current_timestamp;
    RETURN NEW;
END
$BODY$;


-- Rewards Table --
CREATE TABLE reactivation_rewards
(
    id SERIAL NOT NULL,
    user_id INTEGER NOT NULL,
    user_age_range TEXT NOT NULL,
    reward_type TEXT NOT NULL,
    amount INTEGER NOT NULL,
    currency TEXT,
    ticket_type TEXT,
    coupon_code TEXT,
    txn_state TEXT NOT NULL,
    txn_id TEXT,
    expiry_time bigint,
    created_on TIMESTAMP,
    modified_on TIMESTAMP,

    CONSTRAINT pk_reactivation_rewards PRIMARY KEY (id),
    CONSTRAINT uk_reactivation_rewards_uid UNIQUE (user_id),
    CONSTRAINT uk_reactivation_rewards UNIQUE (user_id, user_age_range ,reward_type, amount,currency)

);

CREATE TRIGGER tr_on_insert_reactivation_rewards
    BEFORE INSERT
    ON reactivation_rewards
    FOR EACH ROW
    EXECUTE PROCEDURE trf_update_modified_on_and_created_on();

CREATE TRIGGER tr_on_update_reactivation_rewards
    BEFORE UPDATE
    ON reactivation_rewards
    FOR EACH ROW
    EXECUTE PROCEDURE trf_update_modified_on();