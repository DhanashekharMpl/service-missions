package com.mpl.services.missions.core.validators.other;

import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;

/**
 * Author: Kaustubh Bhoyar
 */
public class OpenURLValidator implements RewardValidator {
    private static final MLogger logger = new MLogger(OpenURLValidator.class);

    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        return new ValidationResponse(true, "OK");
    }
}
