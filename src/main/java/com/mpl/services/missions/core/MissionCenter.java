package com.mpl.services.missions.core;

import com.mpl.commons.lci.utils.CountryInfoUtil;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.accounts.AccountsServiceError;
import com.mpl.services.accounts.TransactionResponse;
import com.mpl.services.accounts.UpdateBalanceRequest;
import com.mpl.services.missions.Mission;
import com.mpl.services.missions.core.validators.GenericRewardValidator;
import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.missions.core.validators.accounts.*;
import com.mpl.services.missions.core.validators.fantasy.TeamsRegisteredValidator;
import com.mpl.services.missions.core.validators.kyc.KycAadharValidator;
import com.mpl.services.missions.core.validators.kyc.KycEmailValidator;
import com.mpl.services.missions.core.validators.kyc.KycPANValidator;
import com.mpl.services.missions.core.validators.other.ContactsPermissionValidator;
import com.mpl.services.missions.core.validators.other.DefaultValidator;
import com.mpl.services.missions.core.validators.other.GamePlayValidator;
import com.mpl.services.missions.core.validators.other.OpenURLValidator;
import com.mpl.services.missions.core.validators.profile.*;
import com.mpl.services.missions.core.validators.random.OpenFromShortcutValidator;
import com.mpl.services.missions.core.validators.referral.InviteAndEarnValidator;
import com.mpl.services.missions.core.validators.ugx.GroupChallengeValidator;
import com.mpl.services.missions.core.validators.ugx.OneVOneChallengeValidator;
import com.mpl.services.missions.core.validators.userRelations.ChatInitiatedValidator;
import com.mpl.services.missions.core.validators.userRelations.FollowUsersValidator;
import com.mpl.services.missions.core.validators.userRelations.GroupCreatedValidator;
import com.mpl.services.missions.core.validators.video.VideoUploadCount;
import com.mpl.services.missions.db.RewardsTable;
import com.mpl.services.missions.db.models.RewardsTableEntry;
import com.mpl.services.missions.models.MissionConfig;
import com.mpl.services.missions.models.MissionsServiceException;
import com.mpl.services.missions.utils.Configurations;
import com.mpl.services.missions.utils.CountryWiseConfig;
import com.mpl.services.missions.utils.ResourceManager;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.*;

/**
 * Author: Kaustubh Bhoyar
 */
public class MissionCenter {
    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(MissionCenter.class);
    private static final Map<String, Class> VALIDATORS = new HashMap<>();
    public static final String GENERIC = "GENERIC";

    private Configurations config = Configurations.getInstance();
    private ResourceManager resourceManager = ResourceManager.getInstance();

    /**************************************** Constructors ****************************************/
    public static void resetValidators() {
        VALIDATORS.clear();
        VALIDATORS.put("PROFILE_SET_PHOTO", ProfilePhotoValidator.class);
        VALIDATORS.put("PROFILE_SET_USERNAME", ProfileNameValidator.class);
        VALIDATORS.put("PROFILE_SET_GENDER", ProfileGenderValidator.class);
        VALIDATORS.put("PROFILE_SET_DOB", ProfileDOBValidator.class);
        VALIDATORS.put("PROFILE_SET_STATE", ProfileStateValidator.class);
        VALIDATORS.put("PROFILE_SET_COVER", ProfileCoverPhotoValidator.class);
        VALIDATORS.put("KYC_SET_EMAIL", KycEmailValidator.class);
        VALIDATORS.put("KYC_SET_PAN", KycPANValidator.class);
        VALIDATORS.put("KYC_SET_AADHAR", KycAadharValidator.class);
        VALIDATORS.put("ACCOUNTS_WITHDRAW_MONEY_PAYTM", WithdrawPaytmValidator.class);
        VALIDATORS.put("ACCOUNTS_WITHDRAW_MONEY_GOPAY", WithdrawGoPayValidator.class);
        VALIDATORS.put("ACCOUNTS_WITHDRAW_MONEY_UPI", WithdrawUpiValidator.class);
        VALIDATORS.put("ACCOUNTS_WITHDRAW_MONEY_BANK_ACCOUNT", WithdrawBankValidator.class);
        VALIDATORS.put("ACCOUNTS_ADD_MONEY", PayerPaytmValidator.class);
        VALIDATORS.put("RATING_FEEDBACK", ProfileRatingValidator.class);
        VALIDATORS.put("PROFILE_FOLLOW", ProfileFollow.class);
        VALIDATORS.put("PROFILE_GET_FOLLOWED", ProfileGetFollowed.class);
        VALIDATORS.put("VIDEO_UPLOAD", VideoUploadCount.class);
        VALIDATORS.put("INVITE_10_FRIENDS", InviteAndEarnValidator.class);
        VALIDATORS.put("OPEN_FROM_SHORTCUT", OpenFromShortcutValidator.class);
        VALIDATORS.put("ACCESS_FINE_LOCATION", DefaultValidator.class);
        VALIDATORS.put("RUN_BACKGROUND", DefaultValidator.class);
        VALIDATORS.put("GENERIC", GamePlayValidator.class);
        VALIDATORS.put("FANTASY_REGISTER_TEAM", TeamsRegisteredValidator.class);
        VALIDATORS.put("READ_CONTACTS", ContactsPermissionValidator.class);
        VALIDATORS.put("OPEN_DEEPLINK_FOLLOW_USERS", FollowUsersValidator.class);
        VALIDATORS.put("OPEN_DEEPLINK_CHAT_SESSIONS", ChatInitiatedValidator.class);
        VALIDATORS.put("OPEN_DEEPLINK_GROUP_CREATION", GroupCreatedValidator.class);
        VALIDATORS.put("OPEN_DEEPLINK_CHALLANGE_PLAYED", OneVOneChallengeValidator.class);
        VALIDATORS.put("OPEN_DEEPLINK_GROUP_CHALLENGE_PLAYED", GroupChallengeValidator.class);
        VALIDATORS.put("FILL_FORM_EMAIL", ProfileEmailValidator.class);
    }

    public static void registerURLValidator(String configName) {
        VALIDATORS.put(configName, OpenURLValidator.class);
    }

    /************************************** Public Functions **************************************/
    public List<Mission> getActiveMissions(String requestId, int userId, String appType, int appVersion, String countryId) throws MissionsServiceException {
        RewardsTable rt = new RewardsTable(resourceManager.getPgConnectionPool());
        Set<String> completedMissions;
        try {
            completedMissions = rt.selectCompletedMissions(userId, CountryWiseConfig.getInstance(countryId).getActiveMissionIds());
        } catch (SQLException e) {
            logger.e(requestId, "Failed to fetch completed missions", e);
            throw new MissionsServiceException(MissionsServiceException.Reason.INTERNAL_ERROR, "DB Call Failed");
        }

        if(countryId == null || countryId.isEmpty()) {
            countryId = config.getDefaultCountryId();
        }
        countryId = countryId.toUpperCase();

        List<Mission> result = new ArrayList<>();
        for(MissionConfig mc : CountryWiseConfig.getInstance(countryId).getMissionConfigs()) {
            if(!mc.showOnAppType(appType)) continue;
            if(mc.getReactVersion() > appVersion) continue;
            if(!mc.getSupportedCountries().contains(countryId)) continue;
            result.add(
                    Mission.newBuilder()
                    .setId(mc.getId())
                    .setName(mc.getName())
                    .setCtaLabel(mc.getCtaLabel())
                    .setRewardCurrency(mc.getRewardCurrency())
                    .setRewardAmount(mc.getRewardAmount().intValue())
                    .setIsRewardClaimed(completedMissions.contains(mc.getId()))
                    .setExtraData(mc.getExtraData() == null ? "" : mc.getExtraData().toString())
                    .setImageUrl(mc.getImageUrl())
                    .setCurrency(CountryInfoUtil.getCountryInfoProto(countryId).getCurrency().getCurrencyId())
                    .setRewardAmountV2(mc.getRewardAmount().toString())
                    .build()
            );
        }
        return result;
    }

    public RewardsTableEntry claimMissionRewards(String requestId, String missionId, int userId, String verificationData, String countryId) throws Exception {
        if(!VALIDATORS.containsKey(missionId) && !missionId.contains(GENERIC)) {
            logger.e(requestId, "Validator Not Registered for", missionId);
            throw new MissionsServiceException(MissionsServiceException.Reason.BAD_REQUEST);
        }

        ValidationResponse vr = null;
        if (missionId.startsWith(GENERIC)){
            MissionConfig mc = CountryWiseConfig.getInstance(countryId).getMissionConfigById(missionId);
            if(mc == null) {
                throw new MissionsServiceException(
                        MissionsServiceException.Reason.INTERNAL_ERROR,
                        "Missing Mission Config"
                );
            }
            String genericName = missionId.substring(0, missionId.indexOf("_"));
            GenericRewardValidator validator = (GenericRewardValidator) VALIDATORS.get(genericName)
                                                                                  .getConstructor()
                                                                                  .newInstance();
            vr = validator.validate(requestId, userId, mc.getExtraData());
        } else {
            RewardValidator validator = (RewardValidator) VALIDATORS.get(missionId)
                    .getConstructor().newInstance();

            vr = validator.validate(requestId, userId, countryId, verificationData);
        }

        if( vr != null && !vr.isValid()) {
            throw new MissionsServiceException(
                    MissionsServiceException.Reason.VALIDATION_FAILED,
                    vr.getMessage()
            );
        }

        MissionConfig mc = null;
        for(MissionConfig c : CountryWiseConfig.getInstance(countryId).getMissionConfigs()) {
            if(!c.getId().equals(missionId)) continue;
            if(!c.getSupportedCountries().contains(countryId)) continue;
            mc = c;
            break;
        }
        if(mc == null) {
            throw new MissionsServiceException(
                    MissionsServiceException.Reason.INTERNAL_ERROR,
                    "Missing Mission Config"
            );
        }

        RewardsTable rt = new RewardsTable(resourceManager.getPgConnectionPool());
        RewardsTableEntry entry;
        try {
            //todo:change reward_currency
            entry = rt.insert(missionId, userId, mc.getRewardCurrency(), mc.getRewardAmount(),
                    CountryInfoUtil.getCountryInfoProto(countryId).getCurrency().getCurrencyId());
        } catch (SQLException e) {
            if(e.getMessage().contains("duplicate key value violates unique constraint \"uk_rewards_mid_uid\"")) {
                throw new MissionsServiceException(MissionsServiceException.Reason.ALREADY_CLAIMED);
            }
            throw e;
        }
        String txnId = transfer(
                "REWARD_" + missionId,
                "Reward for " + missionId,
                userId,
                mc.getRewardCurrency().equals("CASH") ? "Bonus" : "Token",
                mc.getRewardAmount(),
                "MSN" + entry.getId(),
                countryId
        );
        rt.setTransactionIdAndState(entry.getId(), txnId, "COMPLETE");
        return entry;
    }

    /************************************* Core Private Logic *************************************/
    private String transfer(String referenceType, String description, int userId, String currency, BigDecimal amount, String rsTransactionId, String countryId) throws Exception {
        Map<Integer, String> bonusMap = new HashMap<>();
        bonusMap.put(userId, String.valueOf(amount));
        UpdateBalanceRequest creditRequest = UpdateBalanceRequest.newBuilder()
                .putAllUserAmount(bonusMap)
                .setTransactionType("CREDIT")
                .setMoneyType(currency)
                .setReferenceId(rsTransactionId)
                .setReferenceType(referenceType)
                .setDescription(description)
                .build();
        TransactionResponse response = resourceManager.getAccountsServiceBlocking(countryId)
                .processUserBalance(creditRequest);
        if(response.getError().getReason() != AccountsServiceError.Reason.NONE) {
            throw new Exception("Credit request failed. Reason: "
                    + response.getError().getReason() +
                    " Message: " + response.getError().getMessage());
        }
        return ("" + response.getNewTransactionId());
    }

    /************************************** Private Classes ***************************************/
}
