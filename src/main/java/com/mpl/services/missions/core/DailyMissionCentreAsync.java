package com.mpl.services.missions.core;

import com.amazonaws.util.StringUtils;
import com.mpl.commons.lci.utils.CountryInfoUtil;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.client.MqttService;
import com.mpl.services.missions.client.PaymentClient;
import com.mpl.services.missions.client.UserDataClient;
import com.mpl.services.missions.core.DailyMissionStrategies.SegmentationHelper;
import com.mpl.services.missions.db.DailyRewardsTable;
import com.mpl.services.missions.db.models.DailyRewardsTableEntry;
import com.mpl.services.missions.events.EventPublisher;
import com.mpl.services.missions.events.models.InternalEventKey;
import com.mpl.services.missions.events.models.UserCompletedMissionEvent;
import com.mpl.services.missions.events.models.UserMissionsAdded;
import com.mpl.services.missions.models.*;
import com.mpl.services.missions.utils.*;
import com.mpl.services.userdata.grpc.BasicProfile;

import java.sql.SQLException;
import java.util.List;

import static com.mpl.services.missions.utils.Constants.bucketEnum;
import static com.mpl.services.missions.utils.Constants.missionStatusEnum;
import static com.mpl.services.missions.utils.CountryWiseSegmentedConfigs.getDailyMissionConfigOrDefault;

/**
 * Author: Srijan Singh
 */
public class DailyMissionCentreAsync {

    private static final MLogger logger = new MLogger(DailyMissionCentreAsync.class);
    private final ResourceManager resourceManager = ResourceManager.getInstance();

    public void addNewUserToDailyMissions(Integer userId, String countryCode , String apkType , String appType) {
        //add key in redis
        RedisData redisData = new RedisData();
//        Long now = DateTimeUtils.getEpochTime(DateTimeUtils.roundOffTime());
        Long now = System.currentTimeMillis();
        try {
            DailyMissionConfig config = CountryWiseConfig.getInstance(countryCode).getDailyMissionConfigs();
            List<String> disabledMissionsBuckets = CountryWiseConfig.getInstance(countryCode).getDisabledMissionsBuckets();
            String segment = SegmentationHelper.validateUserSegmentBeforeFtueAddition(userId, countryCode);
            if (!StringUtils.isNullOrEmpty(segment)) {
                SegmentedConfigurationsDTO configs = CountryWiseSegmentedConfigs.getSegmentInstance(countryCode, segment);
                if (configs != null && configs.getDailyMissionConfigs() != null) {
                    config = configs.getDailyMissionConfigs();
                    disabledMissionsBuckets = configs.getDisabledMissionsBuckets();
                }
            }
            redisData.setGames(handleAdd(config.getGames(), now, countryCode, bucketEnum.GAMES, disabledMissionsBuckets, config.getTimeDiff()));
            redisData.setFantasy(handleAdd(config.getFantasy(), now, countryCode, bucketEnum.FANTASY, disabledMissionsBuckets, config.getTimeDiff()));
            redisData.setWallet(handleAdd(config.getWallet(), now, countryCode, bucketEnum.WALLET, disabledMissionsBuckets, config.getTimeDiff()));
            redisData.setRummy(handleAdd(config.getRummy(), now, countryCode, bucketEnum.RUMMY, disabledMissionsBuckets, config.getTimeDiff()));
            int size = config.getDays();
            redisData.setEndDate((DateTimeUtils.getEpochTime(DateTimeUtils.addMinutes(now, size))));
            redisData.setSegmentId(segment);
            CacheUtils.addObject(String.valueOf(userId), redisData, ++size);
            logger.i("Adding New User to Daily Missions", userId);
            EventPublisher.publish(InternalEventKey.MS_USER_MISSIONS_ADDED, new UserMissionsAdded(userId,
                    redisData.getSegmentId(), countryCode, apkType, appType));
        } catch (Exception e) {
            logger.e("Error in Adding New User to Daily Missions", userId, e);
        }
    }

    public RedisData.Bucket handleAdd(List<DailyMissionConfig.ZkBucket> zkBucketList, Long now, String countryCode,
                                      bucketEnum bucketEnum, List<String> missionsDisabledBuckets, Integer timeDiff) {
        RedisData.Bucket bucket = new RedisData.Bucket();
        if (!missionsDisabledBuckets.contains(bucketEnum.toString())) {
            int index = 0;
            boolean flag = false;
            if (zkBucketList != null && zkBucketList.size() > 0) {
                while ((!flag) || (index >= zkBucketList.size())) {
                    DailyMissionConfig.ZkBucket zkBucket = zkBucketList.get(index);
                    if (zkBucket.getEnabled()) {
                        flag = true;
                        bucket.setStart(now);
                        bucket.setEnd(DateTimeUtils.getEpochTime(DateTimeUtils.addMinutes(now, timeDiff)));
                        bucket.setLastIndex(index);
                        bucket.setStatus(String.valueOf(missionStatusEnum.ACTIVE));
                        bucket.setMissionId(zkBucket.getMissionId());
                        bucket.setEventCount(0);
                        bucket.setEventThreshold(zkBucket.getEventCount());
                    }
                    else { index++;}
                }
            }
        }
        return bucket;
    }

    public void updateMissionEventsToRedis(EventValidationResponse event) throws Exception {
        // update redis
        try {
            logger.i("Updating Event Details to Redis for user", event.getUserId(), "for Mission", event.getMissionId());
            Long now = System.currentTimeMillis();
            RedisData redisData = CacheUtils.getMissionsObject(String.valueOf(event.getUserId()));
            //todo:check if we can minimise user data call here
            BasicProfile basicProfile = UserDataClient.getBasicProfile(event.getUserId());
            if (redisData != null) {
                DailyMissionConfig config = getDailyMissionConfigOrDefault(basicProfile.getCountryCode(), redisData.getSegmentId());
                switch (event.getBucketEnum()) {
                    case GAMES:
                        redisData.setGames(updateHelper(redisData.getGames(), now, event.getIncrementValue(),
                                event.getUserId(), event.getMissionId(), config.getGames(),
                                bucketEnum.GAMES, redisData.getSignupDate(), basicProfile.getCountryCode(),redisData.getSegmentId()));
                        break;
                    case FANTASY:
                        redisData.setFantasy(updateHelper(redisData.getFantasy(), now, event.getIncrementValue(),
                                event.getUserId(), event.getMissionId(), config.getFantasy(),
                                bucketEnum.FANTASY, redisData.getSignupDate(), basicProfile.getCountryCode(),redisData.getSegmentId()));
                        break;
                    case WALLET:
                        redisData.setWallet(updateHelper(redisData.getWallet(), now, event.getIncrementValue(),
                                event.getUserId(), event.getMissionId(), config.getWallet(),
                                bucketEnum.WALLET, redisData.getSignupDate(), basicProfile.getCountryCode(),redisData.getSegmentId()));
                        break;

                    case RUMMY:
                        redisData.setRummy(updateHelper(redisData.getRummy(), now, event.getIncrementValue(),
                                event.getUserId(), event.getMissionId(), config.getRummy(),
                                bucketEnum.RUMMY, redisData.getSignupDate(), basicProfile.getCountryCode(),redisData.getSegmentId()));
                        break;
                }

                CacheUtils.updateObject(String.valueOf(event.getUserId()), redisData);
            }
        } catch (Exception e) {
            logger.e("Error in updating Mission Events to Redis for UserId", event.getUserId(), "missionId",
                    event.getMissionId(), "in Bucket", event.getBucketEnum(), e);
        }

    }

    public RedisData.Bucket updateHelper(RedisData.Bucket bucket, Long now, int incrementValue, int userId,
                                         String missionId, List<DailyMissionConfig.ZkBucket> zkBucketList,
                                         Constants.bucketEnum bucketEnum, Long signupDate, String countryCode ,String segmentId) {
        try {
            if (bucket != null && !StringUtils.isNullOrEmpty(bucket.getMissionId())) {
                DailyMissionCentre dailyMissionCentre = new DailyMissionCentre();
                int nextIndex = dailyMissionCentre.getNextIndex(bucket.getLastIndex(), zkBucketList);
                DailyMissionConfig.ZkBucket missionConfig = null;
                missionConfig = zkBucketList.stream().filter(i -> i.getMissionId().equalsIgnoreCase(missionId))
                        .findFirst().orElse(null);
                DailyMissionConfig.ZkBucket nextMissionConfig = nextIndex == -1 ? null : zkBucketList.get(nextIndex);
                if (missionConfig != null && bucket.getStatus().equalsIgnoreCase(String.valueOf(Constants.missionStatusEnum.ACTIVE))) {
                    bucket.setEventCount(bucket.getEventCount() + incrementValue);
                    if (bucket.getEventCount() >= bucket.getEventThreshold()) {
                        bucket.setStatus(String.valueOf(Constants.missionStatusEnum.COMPLETED));
                        bucket.setEnd(DateTimeUtils.getEndTime(now, bucket.getStart(), bucket.getEnd(), countryCode));
                        try {
                            claimRewards(bucket.getMissionId(), userId, missionConfig, countryCode);
                            new MqttService().sendMqttHelper(userId, bucket, missionConfig, nextMissionConfig, bucketEnum, Constants.mqttType.MISSION_COMPLETED, countryCode);
                            EventPublisher.publish(InternalEventKey.MS_USER_COMPLETED_MISSION,
                                    new UserCompletedMissionEvent(userId, DateTimeUtils.getDaysElapsed(signupDate), bucketEnum.toString(), missionConfig.getSubTitle(), bucket.getEventCount()
                                            , bucket.getEventThreshold(), missionConfig.getRewardCurrency(), missionConfig.getRewardAmount(), bucket.getStatus(), segmentId));

                        } catch (MissionsServiceException e) {
                            logger.e("Error in sending MQTT Notification for UserId", userId, e);
                        } catch (Exception e) {
                            logger.e("Error in claiming Daily Mission Rewards for UserId", userId, e);
                        }
                    }
                    else {
                        try {
                            logger.i("User Completed Subtask UserId", userId, "Mission", missionId);
                            new MqttService().sendMqttHelper(userId, bucket, missionConfig, nextMissionConfig, bucketEnum, Constants.mqttType.MISSION_SUB_TASK_COMPLETED, countryCode);
                        } catch (Exception e) {
                            logger.e("Error in sending MQTT Notification for UserId", userId, e);
                        }
                    }
                }
            }
            return bucket;
        } catch (Exception e) {
            logger.e("Error in processing update for mission state for userId", userId, e);
            return bucket;
        }
    }

    public void claimRewards(String missionId, int userId, DailyMissionConfig.ZkBucket missionConfig, String countryCode)
            throws Exception {
        if (missionConfig == null) {
            throw new MissionsServiceException(
                    MissionsServiceException.Reason.INTERNAL_ERROR,
                    "Missing Mission Config"
            );
        }
        String description = String.format("Mission reward for '%s'", missionConfig.getSubTitle());
        try {
            DailyRewardsTable rt = new DailyRewardsTable(resourceManager.getPgConnectionPool());
            DailyRewardsTableEntry entry;
            try {
                entry = rt.insert(missionId, userId, missionConfig.getRewardCurrency(), missionConfig.getRewardAmount(),
                        CountryInfoUtil.getCountryInfoProto(countryCode).getCurrency().getCurrencyId());
            } catch (SQLException e) {
                if (e.getMessage().contains("duplicate key value violates unique constraint \"uk_daily_rewards_mid_uid\"")) {
                    throw new MissionsServiceException(MissionsServiceException.Reason.ALREADY_CLAIMED);
                }
                throw e;
            }
            String txnId = new PaymentClient().transfer(
                    "REWARD_" + missionId,
                    description,
                    userId,
                    missionConfig.getRewardCurrency(),
                    missionConfig.getRewardAmount(),
                    "MSN_DMS" + entry.getId()
            );
            logger.i("User", userId, "Completed Mission", missionId, "and Rewards Claimed Transaction id", txnId);
            rt.setTransactionIdAndState(entry.getId(), txnId, "COMPLETE");
//        return entry;
        } catch (MissionsServiceException e) {
            logger.w("Mission Service Exception for userId", userId, e);
        } catch (Exception e) {
            logger.e("Error in claiming Daily Missions Rewards for User", userId, e);
        }
    }
}
