package com.mpl.services.missions.core.validators.kyc;

import com.mpl.services.kyc.CombineKycResponse;
import com.mpl.services.kyc.KycServiceError;
import com.mpl.services.kyc.KycServiceGrpc;
import com.mpl.services.kyc.UserId;
import com.mpl.services.missions.utils.ResourceManager;

/**
 * Author: Kaustubh Bhoyar
 */
public abstract class KycValidatorBase {
    /******************************** Global Variables & Constants ********************************/
    /**************************************** Constructors ****************************************/
    /************************************** Public Functions **************************************/
    public CombineKycResponse getKYCDetails(String requestId, int userId) throws Exception {
        KycServiceGrpc.KycServiceBlockingStub stub = ResourceManager.getInstance().getKYCServiceBlocking();
        CombineKycResponse response = stub.getCombinedKycResponse(UserId.newBuilder().setUserId(userId).build());
        if(response.getError().getReason() != KycServiceError.Reason.NONE) {
            throw new Exception(String.format("Failed to fetch KYC details %s %s",
                    response.getError().getReason(), response.getError().getMessage()));
        }
        return response;
    }

    /************************************* Core Private Logic *************************************/
    /************************************** Private Classes ***************************************/
}
