package com.mpl.services.missions.core;

import com.amazonaws.util.StringUtils;
import com.mpl.commons.lci.utils.CountryInfoUtil;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.DailyMission;
import com.mpl.services.missions.GetDailyMissionsForUserResponse;
import com.mpl.services.missions.client.UserDataClient;
import com.mpl.services.missions.db.DailyRewardsTable;
import com.mpl.services.missions.models.DailyMissionConfig;
import com.mpl.services.missions.models.DailyMissionConfig.ZkBucket;
import com.mpl.services.missions.models.MissionsServiceException;
import com.mpl.services.missions.models.RedisData;
import com.mpl.services.missions.models.RedisData.Bucket;
import com.mpl.services.missions.utils.CacheUtils;
import com.mpl.services.missions.utils.Constants.bucketEnum;
import com.mpl.services.missions.utils.Constants.missionStatusEnum;
import com.mpl.services.missions.utils.CountryWiseConfig;
import com.mpl.services.missions.utils.DateTimeUtils;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.userdata.grpc.BasicProfile;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static com.mpl.services.missions.utils.Constants.bucketEnum.*;
import static com.mpl.services.missions.utils.CountryWiseSegmentedConfigs.getDailyMissionConfigOrDefault;

/**
 * Author: Srijan Singh
 */
public class DailyMissionCentre {
    public static final String GENERIC = "GENERIC";
    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(MissionCenter.class);
    private final ResourceManager resourceManager = ResourceManager.getInstance();

    /************************************** Public Functions **************************************/

    public GetDailyMissionsForUserResponse buildDailyMissionsResponse(String requestId, int userId) throws Exception {
        RedisData redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
        if (redisData == null) {
            throw new MissionsServiceException(MissionsServiceException.Reason.BAD_REQUEST, "User Not Registered in Daily Missions or key not found in Redis");
        }
        BasicProfile basicProfile = UserDataClient.getBasicProfile(userId);
        Long now = System.currentTimeMillis();
        AtomicInteger ts = new AtomicInteger(0);
        AtomicInteger cs = new AtomicInteger(0);

        DailyMissionConfig config = getDailyMissionConfigOrDefault(basicProfile.getCountryCode(), redisData.getSegmentId());

        getAllMissionsSize(config, redisData, ts, cs);
        if ((now > redisData.getEndDate()) || (hasUserFinishedAllMissions(redisData, basicProfile.getCountryCode(), config))) {
            //show final rewards as missions have ended
            GetDailyMissionsForUserResponse response = GetDailyMissionsForUserResponse
                    .newBuilder()
                    .setHeader(config.getHeader())
                    .setDays(redisData.getEndDate())
                    .setFinalRewards(showFinalRewards(userId))
                    .setCompletedMissionsCount(cs.intValue())
                    .setTotalMissionsCount(ts.intValue())
                    .build();
            return response;
        }
        else {
            GetDailyMissionsForUserResponse response = GetDailyMissionsForUserResponse
                    .newBuilder()
                    .setHeader(config.getHeader())
                    .setDays(redisData.getEndDate())
                    .addAllMissions(getDailyMissionsForUser(userId, redisData, now, config, basicProfile.getCountryCode()))
                    .setCompletedMissionsCount(cs.intValue())
                    .setTotalMissionsCount(ts.intValue())
                    .build();
            return response;
        }

    }

    public GetDailyMissionsForUserResponse buildGetAllDailyMissionsResponse(String requestId, int userId) throws Exception {
        RedisData redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
        if (redisData == null) {
            throw new MissionsServiceException(MissionsServiceException.Reason.BAD_REQUEST,
                    "User Not Registered in Daily Missions or key not found in Redis");
        }
        BasicProfile basicProfile = UserDataClient.getBasicProfile(userId);
        DailyMissionConfig config = getDailyMissionConfigOrDefault(basicProfile.getCountryCode(), redisData.getSegmentId());
        String currency = CountryInfoUtil.getCountryInfoProto(basicProfile.getCountryCode()).getCurrency().getCurrencyId();
        List<DailyMission> dailyMissionList = new ArrayList<>();
        for (bucketEnum s : bucketEnum.values()) {
            switch (s) {
                case GAMES:
                    helper(config.getGames(), redisData.getGames(), s, dailyMissionList, currency);
                    break;
                case FANTASY:
                    helper(config.getFantasy(), redisData.getFantasy(), s, dailyMissionList, currency);
                    break;
                case WALLET:
                    helper(config.getWallet(), redisData.getWallet(), s, dailyMissionList, currency);
                    break;
                case RUMMY:
                    helper(config.getRummy(), redisData.getRummy(), s, dailyMissionList, currency);
                    break;
            }
        }
        AtomicInteger ts = new AtomicInteger(0);
        AtomicInteger cs = new AtomicInteger(0);
        getAllMissionsSize(config, redisData, ts, cs);
        return GetDailyMissionsForUserResponse
                .newBuilder()
                .setHeader(config.getHeader())
                .setDays(redisData.getEndDate())
                .addAllMissions(dailyMissionList)
                .setCompletedMissionsCount(cs.intValue())
                .setTotalMissionsCount(ts.intValue())
                .build();
    }

    /************************************* Core Private Logic *************************************/


    /************************************** Private Classes ***************************************/

    private void helper(List<ZkBucket> zkBucketList, Bucket bucket, bucketEnum bucketEnum, List<DailyMission> dailyMissionList, String currency) {
        if (zkBucketList != null && zkBucketList.size() > 0 && bucket != null
                && !StringUtils.isNullOrEmpty(bucket.getMissionId())) {
            missionStatusEnum status = missionStatusEnum.COMPLETED;
            for (ZkBucket zkBucket : zkBucketList) {
                if (zkBucket.getMissionId().equals(bucket.getMissionId())) {
                    dailyMissionList.add(buildAllMissionObject(zkBucket, bucket, bucketEnum, missionStatusEnum.valueOf(bucket.getStatus()), currency));
                    status = missionStatusEnum.LOCKED;
                }
                else {
                    dailyMissionList.add(buildAllMissionObject(zkBucket, bucket, bucketEnum, status, currency));
                }
            }
        }
    }

    private void getAllMissionsSize(DailyMissionConfig dailyMissionConfig, RedisData redisData,
                                    AtomicInteger totalSum, AtomicInteger completedSum) {
        for (bucketEnum s : bucketEnum.values()) {
            switch (s) {
                case GAMES:
                    if (dailyMissionConfig.getGames() != null && redisData.getGames() != null && !StringUtils.isNullOrEmpty(redisData.getGames().getMissionId())) {
                        totalSum.getAndAdd(dailyMissionConfig.getGames().size());
                        completedSum.getAndAdd(getIndex(dailyMissionConfig.getGames(), redisData.getGames()));
                    }
                    break;
                case FANTASY:
                    if (dailyMissionConfig.getFantasy() != null && redisData.getFantasy() != null && !StringUtils.isNullOrEmpty(redisData.getFantasy().getMissionId())) {
                        totalSum.getAndAdd(dailyMissionConfig.getFantasy().size());
                        completedSum.getAndAdd(getIndex(dailyMissionConfig.getFantasy(), redisData.getFantasy()));
                    }
                    break;
                case WALLET:
                    if (dailyMissionConfig.getWallet() != null && redisData.getWallet() != null && !StringUtils.isNullOrEmpty(redisData.getWallet().getMissionId())) {
                        totalSum.getAndAdd(dailyMissionConfig.getWallet().size());
                        completedSum.getAndAdd(getIndex(dailyMissionConfig.getWallet(), redisData.getWallet()));
                    }
                    break;
                case RUMMY:
                    if (dailyMissionConfig.getRummy() != null && redisData.getRummy() != null && !StringUtils.isNullOrEmpty(redisData.getRummy().getMissionId())) {
                        totalSum.getAndAdd(dailyMissionConfig.getRummy().size());
                        completedSum.getAndAdd(getIndex(dailyMissionConfig.getRummy(), redisData.getRummy()));
                    }
                    break;
            }
        }
    }

    public List<DailyMission> getDailyMissionsForUser(int userId, RedisData redisData, Long now,
                                                      DailyMissionConfig config, String countryCode) throws Exception {
        //fixme:check this call
        if (StringUtils.isNullOrEmpty(countryCode)) {
            countryCode = UserDataClient.getBasicProfile(userId).getCountryCode();
        }
        if (config == null) {
            config = getDailyMissionConfigOrDefault(countryCode, redisData.getSegmentId());
        }
        List<DailyMission> dailyMissionList = new ArrayList<>();
        if (config != null) {
            try {
                redisData.setGames(refreshRedisData(now, redisData.getGames(), config.getGames(), dailyMissionList, GAMES, userId, countryCode, config));
                redisData.setFantasy(refreshRedisData(now, redisData.getFantasy(), config.getFantasy(), dailyMissionList, FANTASY, userId, countryCode, config));
                redisData.setWallet(refreshRedisData(now, redisData.getWallet(), config.getWallet(), dailyMissionList, WALLET, userId, countryCode, config));
                redisData.setRummy(refreshRedisData(now, redisData.getRummy(), config.getRummy(), dailyMissionList, RUMMY, userId, countryCode, config));

                CacheUtils.updateObject(String.valueOf(userId), redisData);
            } catch (Exception e) {
                logger.e("Error in getting daily missions for user", userId, e);
            }
        }
        return dailyMissionList;
    }

    private Bucket refreshRedisData(Long now, Bucket bucket, List<ZkBucket> zkBucketList,
                                    List<DailyMission> dailyMissionList, bucketEnum bucketEnum,
                                    Integer userId, String countryCode, DailyMissionConfig config) {
        try {
            if (bucket != null && !StringUtils.isNullOrEmpty(bucket.getMissionId())) {
                if (now > bucket.getEnd()) {
                    if (bucket.getStatus().equalsIgnoreCase(String.valueOf(missionStatusEnum.COMPLETED))) {
                        //mission completed on time unlock new mission
                        logger.d("Unlocking new mission for User", userId);
                        int index = getNextIndex(bucket.getLastIndex(), zkBucketList);
                        if (index != -1) {
                            ZkBucket zkBucket = zkBucketList.get(index);
                            bucket.setLastIndex(index);
                            bucket.setStatus(String.valueOf(missionStatusEnum.ACTIVE));
                            bucket.setMissionId(zkBucket.getMissionId());
                            bucket.setEventCount(0);
                            bucket.setEventThreshold(zkBucket.getEventCount());
                        }

                    }
                    bucket.setEnd(DateTimeUtils.getEndTime(now, bucket.getStart(), bucket.getEnd(), countryCode));
                    bucket.setStart(DateTimeUtils.getEpochTime(DateTimeUtils.minusMinutes(bucket.getEnd(), config.getTimeDiff())));
                    buildMissionObject(bucket, zkBucketList, dailyMissionList, bucketEnum, countryCode);
                    return bucket;
                }
                else
                    buildMissionObject(bucket, zkBucketList, dailyMissionList, bucketEnum, countryCode);
                return bucket;
            }
        } catch (Exception e) {
            logger.e("Error in Refreshing Redis Data for User", userId, e);
            return bucket;
        }
        return bucket;
    }

    public int getNextIndex(int index, List<ZkBucket> zkBucketList) {
        ++index;
        boolean flag = false;
        while ((!flag) && (index < zkBucketList.size())) {
            ZkBucket zkBucket = zkBucketList.get(index);
            if (zkBucket.getEnabled()) {
                flag = true;
            }
            else {
                index++;
            }
        }
        return flag ? index : -1;
    }

    private double showFinalRewards(int userId) {
        DailyRewardsTable rt = new DailyRewardsTable(resourceManager.getPgConnectionPool());
        BigDecimal bd = BigDecimal.valueOf(0);
        try {
            bd = rt.getAllRewardsForUser(userId) != null ? rt.getAllRewardsForUser(userId) : bd;
            logger.d("Final Rewards for user", userId, "Won", bd);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return bd.doubleValue();
    }

    private Boolean hasUserFinishedAllMissions(RedisData redisData, String countryCode, DailyMissionConfig config) {
        boolean flag = true;
        for (bucketEnum bucketEnum : bucketEnum.values()) {
            switch (bucketEnum) {
                case GAMES:
                    flag = checkUserCompletedMissionsInBucket(redisData.getGames(), config.getGames());
                    break;
                case FANTASY:
                    flag = checkUserCompletedMissionsInBucket(redisData.getFantasy(), config.getFantasy());
                    break;
                case WALLET:
                    flag = checkUserCompletedMissionsInBucket(redisData.getWallet(), config.getWallet());
                    break;
                case RUMMY:
                    flag = checkUserCompletedMissionsInBucket(redisData.getRummy(), config.getRummy());
                    break;
            }
            if (!flag) return false;
        }
        return true;
    }

    private Boolean checkUserCompletedMissionsInBucket(Bucket bucket, List<ZkBucket> zkBucketList) {
        if (bucket != null && !StringUtils.isNullOrEmpty(bucket.getMissionId()) &&
                zkBucketList != null && zkBucketList.size() > 0) {
            return getNextIndex(bucket.getLastIndex(), zkBucketList) == -1
                    && bucket.getStatus().equalsIgnoreCase(String.valueOf(missionStatusEnum.COMPLETED));
        }
        else return true;
    }

    /**************************************** Constructors ****************************************/

    private DailyMission buildAllMissionObject(ZkBucket zkBucket, Bucket bucket, bucketEnum bucketEnum, missionStatusEnum status, String currency) {
        return DailyMission.newBuilder()
                .setTitle(zkBucket.getTitle())
                .setSubtitle(zkBucket.getSubTitle())
                .setState(status.toString())
                .setButtonText(zkBucket.getButtonText())
                .setBucket(String.valueOf(bucketEnum))
                .setType(zkBucket.getType())
                .setRewardType(zkBucket.getRewardCurrency())
                .setRewardAmount(zkBucket.getRewardAmount().intValue())
                .setImageUrl(zkBucket.getImageUrl())
                .setTotalTasks(zkBucket.getEventCount())
                .setCompletedTasks(missionStatusEnum.COMPLETED == status ? zkBucket.getEventCount() :
                        (missionStatusEnum.ACTIVE == status ? bucket.getEventCount() : 0))
                .setExtraData(zkBucket.getExtraData() == null ? "" : zkBucket.getExtraData().toString())
                .setCurrency(currency)
                .setRewardAmountV2(zkBucket.getRewardAmount().toString())
                .build();
    }

    private Integer getIndex(List<ZkBucket> bucketList, Bucket bucket) {
        Integer index = IntStream.range(0, bucketList.size())
                .filter(i -> bucketList.get(i).getMissionId().equals(bucket.getMissionId()))
                .findFirst()
                .orElse(0);
        return bucket.getStatus().equals(missionStatusEnum.COMPLETED.toString()) ? index + 1 : index;
    }

    private void buildMissionObject(Bucket bucket, List<ZkBucket> zkBucketList, List<DailyMission> dailyMissionList,
                                    bucketEnum bucketEnum, String countryCode) {

        try {
            //builds first mission which can be active or completed
            if (zkBucketList != null && zkBucketList.size() > 0 && bucket != null && !StringUtils.isNullOrEmpty(bucket.getMissionId())) {
                ZkBucket zkBucket = zkBucketList.get(bucket.getLastIndex());
                dailyMissionList.add(DailyMission.newBuilder()
                        .setTitle(zkBucket.getTitle())
                        .setSubtitle(zkBucket.getSubTitle())
                        .setState(bucket.getStatus())
                        .setStartTime(bucket.getStart())
                        .setEndTime(bucket.getEnd())
                        .setButtonText(zkBucket.getButtonText())
                        .setBucket(String.valueOf(bucketEnum))
                        .setType(zkBucket.getType())
                        .setRewardType(zkBucket.getRewardCurrency())
                        .setRewardAmount(zkBucket.getRewardAmount().intValue())
                        .setImageUrl(zkBucket.getImageUrl())
                        .setTotalTasks(bucket.getEventThreshold())
                        .setCompletedTasks(bucket.getEventCount())
                        .setExtraData(zkBucket.getExtraData() == null ? "" : zkBucket.getExtraData().toString())
                        .setCurrency(CountryInfoUtil.getCountryInfoProto(countryCode).getCurrency().getCurrencyId())
                        .setRewardAmountV2(zkBucket.getRewardAmount().toString())
                        .build());

                int index = getNextIndex(bucket.getLastIndex(), zkBucketList);
                if (index != -1) {
                    CountryWiseConfig countryWiseConfig = CountryWiseConfig.getInstance(countryCode);
                    //builds second mission which is locked
                    //todo:this second obj is no longer used , validate and delete
                    zkBucket = zkBucketList.get(index);
                    dailyMissionList.add(DailyMission.newBuilder()
                            .setTitle(zkBucket.getTitle())
                            .setSubtitle(zkBucket.getSubTitle())
                            .setState(String.valueOf(missionStatusEnum.LOCKED))
                            .setStartTime(DateTimeUtils.getEpochTime(DateTimeUtils.addMinutes(bucket.getStart(), countryWiseConfig.getDailyMissionConfigs().getTimeDiff())))
                            .setEndTime(DateTimeUtils.getEpochTime(DateTimeUtils.addMinutes(bucket.getEnd(), countryWiseConfig.getDailyMissionConfigs().getTimeDiff())))
                            .setButtonText(zkBucket.getButtonText())
                            .setBucket(String.valueOf(bucketEnum))
                            .setType(zkBucket.getType())
                            .setRewardType(zkBucket.getRewardCurrency())
                            .setRewardAmount(zkBucket.getRewardAmount().intValue())
                            .setImageUrl(zkBucket.getImageUrl())
                            .setTotalTasks(zkBucket.getEventCount())
                            .setCompletedTasks(0)
                            .setExtraData(zkBucket.getExtraData() == null ? "" : zkBucket.getExtraData().toString())
                            .setCurrency(CountryInfoUtil.getCountryInfoProto(countryCode).getCurrency().getCurrencyId())
                            .setRewardAmountV2(zkBucket.getRewardAmount().toString())
                            .build());
                }
            }
        } catch (Exception e) {
            logger.e("Error in building daily missions Object", e);
        }
    }
}
