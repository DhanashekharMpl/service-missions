package com.mpl.services.missions.core.validators.userRelations;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.missions.models.MissionConfig;
import com.mpl.services.missions.utils.Configurations;
import com.mpl.services.missions.utils.CountryWiseConfig;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.relation.grpc.GetUserIdListRequest;
import com.mpl.services.relation.grpc.GetUserIdsResponse;
import com.mpl.services.relation.grpc.UserRelationServiceGrpc;

public class FollowUsersValidator implements RewardValidator {
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        UserRelationServiceGrpc.UserRelationServiceBlockingStub stub = ResourceManager.getInstance().getUserRelationsV2ServiceBlockingStub();
        GetUserIdListRequest request = GetUserIdListRequest.newBuilder()
                .setUserId(userId)
                .setRequestId("Missions")
                .build();
        GetUserIdsResponse response = stub.getUserFollowingIds(request);
        MissionConfig missionConfig = CountryWiseConfig.getInstance(countryId).getMissionConfigById("OPEN_DEEPLINK_FOLLOW_USERS");
        boolean isValid = missionConfig.getExtraData().get("count").intValue()<=response.getUserIdsList().size()?true:false;
        return new ValidationResponse(isValid, "OK");
    }
}
