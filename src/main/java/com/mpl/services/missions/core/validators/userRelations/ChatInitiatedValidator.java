package com.mpl.services.missions.core.validators.userRelations;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.missions.models.MissionConfig;
import com.mpl.services.missions.utils.Configurations;
import com.mpl.services.missions.utils.CountryWiseConfig;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.relation.grpc.GetUserChatSessionsStatusRequest;
import com.mpl.services.relation.grpc.GetUserChatSessionsStatusResponse;
import com.mpl.services.relation.grpc.UserRelationServiceGrpc;

public class ChatInitiatedValidator implements RewardValidator {

    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        UserRelationServiceGrpc.UserRelationServiceBlockingStub stub = ResourceManager.getInstance().getUserRelationsV2ServiceBlockingStub();
        MissionConfig missionConfig = CountryWiseConfig.getInstance(countryId).getMissionConfigById("OPEN_DEEPLINK_CHAT_SESSIONS");
        GetUserChatSessionsStatusRequest request = GetUserChatSessionsStatusRequest.newBuilder()
                .setUserId(userId)
                .setRequestId("Missions")
                .setSessionCount(missionConfig.getExtraData().get("count").intValue())
                .build();
        GetUserChatSessionsStatusResponse response = stub.getUserChatSessionsStatus(request);
        return new ValidationResponse(response.getSessionCountStatus(), "OK");
    }
}
