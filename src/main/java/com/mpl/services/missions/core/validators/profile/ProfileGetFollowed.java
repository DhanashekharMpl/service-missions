package com.mpl.services.missions.core.validators.profile;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;

/**
 * Author: Kaustubh Bhoyar
 */
public class ProfileGetFollowed extends ProfileValidatorBase implements RewardValidator {
    /******************************** Global Variables & Constants ********************************/
    private static final int CONFIG_FOLLOWERS_COUNT = 5;

    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        int followersSize = super.getFollowersList(
                requestId, userId);
        if(followersSize < CONFIG_FOLLOWERS_COUNT) {
            return new ValidationResponse(false, "Get followed by "
                    + CONFIG_FOLLOWERS_COUNT + " users to get reward");
        }
        return new ValidationResponse(true, "OK");

    }
}
