package com.mpl.services.missions.core.validators.ugx;

import com.mpl.services.grpc.battlechallenges.BattleChallengeServiceGrpc;
import com.mpl.services.grpc.battlechallenges.ChallengeCountRequest;
import com.mpl.services.grpc.battlechallenges.ChallengeCountResponse;
import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.missions.models.MissionConfig;
import com.mpl.services.missions.utils.CountryWiseConfig;
import com.mpl.services.missions.utils.ResourceManager;

public class OneVOneChallengeValidator implements RewardValidator {
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        BattleChallengeServiceGrpc.BattleChallengeServiceBlockingStub stub = ResourceManager.getInstance().getBattleChallengeServiceBlockingStub();
        MissionConfig missionConfig = CountryWiseConfig.getInstance(countryId).getMissionConfigById("OPEN_DEEPLINK_CHALLANGE_PLAYED");

        ChallengeCountRequest request = ChallengeCountRequest.newBuilder()
                .setUserId(userId)
                .setRequestId("Missions")
                .setFinished(true)
                .build();
        ChallengeCountResponse response = stub.getUserChallengeCount(request);
        boolean isValid= missionConfig.getExtraData().get("count").intValue()<=response.getCount()?true:false;
        return new ValidationResponse(isValid, "OK");
    }
}
