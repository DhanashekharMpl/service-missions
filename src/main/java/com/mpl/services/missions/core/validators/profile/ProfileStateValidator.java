package com.mpl.services.missions.core.validators.profile;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.userdata.grpc.ExtendedProfileField;

/**
 * Author: Kaustubh Bhoyar
 */
public class ProfileStateValidator extends ProfileValidatorBase implements RewardValidator {
    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        String state = super.getExtendedFieldValue(requestId, userId, ExtendedProfileField.STATE);
        if(state != null && !state.isEmpty()) {
            return new ValidationResponse(true, "OK");
        }
        if(verificationData == null || verificationData.isEmpty()) {
            return new ValidationResponse(false, "State is not set");
        }
        super.setExtendedFieldValue(requestId, userId, ExtendedProfileField.STATE, verificationData);
        return new ValidationResponse(true, "OK");
    }
}
