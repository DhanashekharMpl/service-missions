package com.mpl.services.missions.core.validators.profile;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;

/**
 * Author: Kaustubh Bhoyar
 */
public class ProfileFollow extends ProfileValidatorBase implements RewardValidator {
    /******************************** Global Variables & Constants ********************************/
    private static final int CONFIG_FOLLOW_COUNT = 10;

    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        int followingList = super.getFollowersList(
                requestId, userId);
        if(followingList < CONFIG_FOLLOW_COUNT) {
            return new ValidationResponse(false, "Follow " + CONFIG_FOLLOW_COUNT + " users to get reward");
        }
        return new ValidationResponse(true, "OK");
    }
}
