package com.mpl.services.missions.core;

import com.mpl.commons.lci.utils.CountryInfoUtil;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.ReactivationReward;
import com.mpl.services.missions.ReactivationRewardResponse;
import com.mpl.services.missions.client.PaymentClient;
import com.mpl.services.missions.client.SegmentationClient;
import com.mpl.services.missions.client.UserDataClient;
import com.mpl.services.missions.db.ReactivationRewardsTable;
import com.mpl.services.missions.db.models.ReactivationRewardsTableEntry;
import com.mpl.services.missions.models.MissionsServiceException;
import com.mpl.services.missions.models.ReactivationRewards;
import com.mpl.services.missions.utils.*;
import com.mpl.services.offers.*;
import com.mpl.services.userdata.grpc.BasicProfile;
import org.postgresql.util.PSQLException;

import java.math.BigDecimal;
import java.sql.SQLException;

import static com.mpl.services.missions.core.ReactivationCentre.rewardTypeEnum.*;

/**
 * Author: Srijan Singh
 */

public class ReactivationCentre {
    public static final String PREFIX = "MSN_RR";

    private static final MLogger logger = new MLogger(ReactivationCentre.class);
    private final Configurations config = Configurations.getInstance();
    private final ResourceManager resourceManager = ResourceManager.getInstance();

    public ReactivationRewardResponse getReactivationReward(Integer userId) throws Exception {
        BasicProfile basicProfile = UserDataClient.getBasicProfile(userId);
        String currency = CountryInfoUtil.getCountryInfoProto(basicProfile.getCountryCode()).getCurrency().getCurrencyId();
        if (!CountryWiseConfig.getInstance(basicProfile.getCountryCode()).getReactivationRewardsActive()) {
            throw new MissionsServiceException(MissionsServiceException.Reason.VALIDATION_FAILED, "RTUE Disabled");
        }
        ReactivationRewards reward = null;
        try {
//        ReactivationRewards reward = config.getReactivationRewards().stream().filter(i -> i.getMinAgeInMins() < age).filter(i -> i.getMaxAgeInMins() > age).findFirst().orElse(null);
            reward = CountryWiseConfig.getInstance(basicProfile.getCountryCode()).getReactivationRewards().stream()
                    .filter(i -> SegmentationClient.getSegmentsForUser(userId).contains(i.getSegmentId()))
                    .filter(ReactivationRewards::getEnabled).findFirst().orElse(null);
        } catch (Exception e) {
            logger.e("Error getting Segment for User", userId, e);
            throw new MissionsServiceException(MissionsServiceException.Reason.INTERNAL_ERROR, "Error getting Segment for User");
        }
        if (reward == null) {
            logger.e("Reward Config not Available for segment ,userId:", userId);
            throw new MissionsServiceException(MissionsServiceException.Reason.BAD_REQUEST, "Reward Config not Available for segment id");
        }
        try {
            rewardTypeEnum rewardTypeEnum = NIL;
            rewardTypeEnum = ReactivationCentre.rewardTypeEnum.valueOf(reward.getRewardType());
            String userAge = String.format("%d-%d", (reward.getMinAgeInMins() / 1440), (reward.getMaxAgeInMins() / 1440));
            String referenceId = String.format("%s-%s", PREFIX, GeneralUtils.getUniqueId());
            ReactivationReward reactivationReward = null;
            ReactivationRewardResponse reactivationRewardResponse = null;
            ReactivationRewardsTableEntry entry = null;
            switch (rewardTypeEnum) {
                case TICKET:
                    entry = persist(userId, userAge, reward.getRewardType(), reward.getRewardAmount(), reward.getRewardCurrency(), reward.getTicketType(), null, 0L, currency);
                    reactivationReward = getTicket(userId, reward, referenceId, currency);
                    logger.i("User", userId, "received Reactivation Reward", TICKET.name(), "Ticket Type:", reward.getTicketType());
                    updateTransactionIdAndState(userId, entry.getId(), referenceId, null);
                    reactivationRewardResponse = ReactivationRewardResponse.newBuilder()
                            .setType(TICKET.name())
                            .setUserAge(userAge)
                            .setData(reactivationReward)
                            .build();
                    return reactivationRewardResponse;

                case COUPON:
                    entry = persist(userId, userAge, reward.getRewardType(), reward.getRewardAmount(), reward.getRewardCurrency(), reward.getTicketType(), null, 0L, currency);
                    reactivationReward = getCoupon(userId, reward, referenceId, basicProfile.getCountryCode());
                    logger.i("User", userId, "received Reactivation Reward", COUPON.name());
                    updateTransactionIdAndState(userId, entry.getId(), referenceId, reactivationReward.getCoupons().getCouponCode());
                    reactivationRewardResponse = ReactivationRewardResponse.newBuilder()
                            .setType(COUPON.name())
                            .setUserAge(userAge)
                            .setData(reactivationReward)
                            .build();
                    return reactivationRewardResponse;
                case CASH:
                    entry = persist(userId, userAge, reward.getRewardType(), reward.getRewardAmount(), reward.getRewardCurrency(), reward.getTicketType(), null, 0L, currency);
                    DailyMissionCentre missionCentre = new DailyMissionCentre();
                    String txnId = new PaymentClient().transfer("RTUE reward", "RTUE reward", userId,
                            reward.getRewardCurrency(), reward.getRewardAmount(), referenceId + entry.getId()
                    );
                    reactivationReward = ReactivationReward.newBuilder()
                            .setRewardAmount(reward.getRewardAmount().intValue())
                            .setRewardCurrency(reward.getRewardCurrency())
                            .setExtraData(reward.getExtraData() == null ? "" : reward.getExtraData().toString())
                            .setCurrency(currency)
                            .setRewardAmountV2(reward.getRewardAmount().toString())
                            .build();
                    logger.i("User", userId, "received Reactivation Reward", CASH.name());
                    updateTransactionIdAndState(userId, entry.getId(), txnId, null);
                    reactivationRewardResponse = ReactivationRewardResponse.newBuilder()
                            .setType(CASH.name())
                            .setUserAge(userAge)
                            .setData(reactivationReward)
                            .build();
                    return reactivationRewardResponse;
                default: {
                    logger.e("Reactivation Reward Not Found for userId", userId);
                    throw new MissionsServiceException(MissionsServiceException.Reason.BAD_REQUEST, "Reactivation Reward Not Found");
                }
            }
        } catch (Exception e) {
            if (e instanceof MissionsServiceException) {
                throw e;
            }
            else {
                logger.e("Error in Fetching Reactivation Rewards for user", userId, e);
                throw new MissionsServiceException(MissionsServiceException.Reason.INTERNAL_ERROR, "Error in Fetching Reactivation Rewards");
            }
        }
    }

    /************************************* Core Private Logic *************************************/

    private ReactivationReward getTicket(int userId, ReactivationRewards reward, String referenceId, String currency) throws MissionsServiceException {
        try {
            OffersServiceGrpc.OffersServiceBlockingStub stub = ResourceManager.getInstance()
                    .getOffersServiceBlockingStub();
            long expiry = DateTimeUtils.getEpochTime(DateTimeUtils.addMinutes(System.currentTimeMillis(), reward.getExpiryTimeInMins()));
            CreateBulkUserTournamentTicketRequest request = CreateBulkUserTournamentTicketRequest.newBuilder()
                    .addUserIds(userId)
                    .setBusinessDomain(reward.getTicketType())
                    .setTicketValue(String.valueOf(reward.getRewardAmount()))
                    .setTicketExpireAt(String.valueOf(expiry))
                    .setReferenceId(referenceId)
                    .setSendNotification(false)
                    .setTicketCreatedSource("RTUE reward")
                    .build();
            logger.d("Request for Ticket creation : ", request);
            CreateBulkUserTournamentTicketResponse response = stub.createBulkUserTournamentTicket(request);
            if (response.getIsCreated()) {
                logger.d("Received Success Response for CreateBulkUserTournamentTicketResponse for userId", userId);
                ReactivationReward reactivationReward = ReactivationReward.newBuilder()
                        .setRewardAmount(reward.getRewardAmount().intValue())
                        .setTicketType(reward.getTicketType())
                        .setExpiryTime(expiry)
                        .setExtraData(reward.getExtraData() == null ? "" : reward.getExtraData().toString())
                        .setRewardAmountV2(reward.getRewardAmount().toString())
                        .setRewardCurrency(currency)
                        .build();
                return reactivationReward;
            }
            else {
                throw new MissionsServiceException(MissionsServiceException.Reason.EXTERNAL_ERROR, "Received Failure Response from Offer Service");
            }
        } catch (Exception e) {
            if (e instanceof MissionsServiceException) {
                logger.e("Received Failure Response from Offer Service for user:", userId);
                throw e;
            }
            else {
                logger.e("Error occurred processing Ticket for user:", userId, e);
                throw new MissionsServiceException(MissionsServiceException.Reason.INTERNAL_ERROR, "Error calling Offers Service for Get Ticket");
            }
        }
    }

    private ReactivationReward getCoupon(int userId, ReactivationRewards reward, String referenceId, String countryCode) throws MissionsServiceException {
        CreateUserCouponResponseV2 response = null;
        try {
            OffersServiceV2Grpc.OffersServiceV2BlockingStub stub = ResourceManager.getInstance()
                    .getOffersServiceV2BlockingStub();
            CreateUserCouponRequestV2 request = CreateUserCouponRequestV2.newBuilder()
                    .setUserId(userId)
                    .setCashbackPercentage(reward.getCouponData().getCashbackPercentage().toString())
                    .setMaximumCashback(reward.getCouponData().getMaximumCashback().toString())
                    .setReferenceId(referenceId)
                    .setValidForMinutes(reward.getCouponData().getValidForMinutes())
                    .setDescription("Cashback Coupon for Reactivated Customer" + referenceId)
                    .setOfferName("RTUE")
                    .setMinAmount(reward.getCouponData().getMinAmount().toString())
                    .setToBalance(reward.getCouponData().getToBalance())
                    .setSource("RTUE reward")
                    .setCountryInfo(CountryInfoUtil.getCountryInfoProto(countryCode))
                    .build();
            logger.d("request for coupon creation : ", request);
            response = stub.createUserCouponV2(request);
            logger.d("Response for coupon creation in getCoupon : ", response);
            ReactivationReward reactivationReward = ReactivationReward.newBuilder()
                    .setCoupons(response.getCoupon())
                    .setExtraData(reward.getExtraData() == null ? "" : reward.getExtraData().toString())
                    .build();
            return reactivationReward;
        } catch (Exception e) {
            logger.e("Error occurred processing Coupon for user:", userId, e);
            throw new MissionsServiceException(MissionsServiceException.Reason.EXTERNAL_ERROR, "Error in getting Coupon");
        }
    }

    /************************************* Database Interactions *************************************/

    public ReactivationRewardsTableEntry persist(int userId, String userAge, String rewardType, BigDecimal amount, String rewardCurrency,
                                                 String ticketType, String couponCode, long expiryTime, String currency) throws MissionsServiceException, SQLException {
        ReactivationRewardsTable rrt = new ReactivationRewardsTable(resourceManager.getPgConnectionPool());
        ReactivationRewardsTableEntry entry;
        try {
            entry = rrt.insert(userId, userAge, rewardType, amount, rewardCurrency, ticketType, couponCode, expiryTime, currency);
            logger.d("DB Update: Insert RTUE details for User", userId);
        } catch (PSQLException e) {
            //Used Sql state check for constraint violation :https://stackoverflow.com/a/58985208
            //Error states :https://www.postgresql.org/docs/11/errcodes-appendix.html
            if (e.getSQLState().equals("23505")) {
                logger.e("duplicate key value violates unique constraint \"uk_reactivation_rewards_uid\"");
                throw new MissionsServiceException(MissionsServiceException.Reason.ALREADY_CLAIMED);
            }
            else {
                throw new MissionsServiceException(MissionsServiceException.Reason.INTERNAL_ERROR, "Error Saving Reward in db");
            }
        }
        logger.d("DB SAVE: User", userId, "received Reactivation Reward", rewardType);
        return entry;
    }

    public void updateTransactionIdAndState(Integer userId, Integer id, String txnId, String couponCode) throws MissionsServiceException {
        ReactivationRewardsTable rrt = new ReactivationRewardsTable(resourceManager.getPgConnectionPool());
        try {
            rrt.setTransactionIdAndState(id, txnId, "COMPLETE", couponCode);
            logger.d("DB Update: Updated Transaction Status for User", userId);
        } catch (Exception e) {
            logger.e("Error in Updating Transaction Details to Db for user", userId, e);
            throw new MissionsServiceException(MissionsServiceException.Reason.INTERNAL_ERROR);
        }
    }

    enum rewardTypeEnum {TICKET, COUPON, CASH, NIL}

}

