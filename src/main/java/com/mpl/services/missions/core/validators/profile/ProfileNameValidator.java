package com.mpl.services.missions.core.validators.profile;

import com.mpl.services.missions.client.UserDataClient;
import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.userdata.grpc.BasicProfile;

import java.util.regex.Pattern;

/**
 * Author: Kaustubh Bhoyar
 */
public class ProfileNameValidator extends ProfileValidatorBase implements RewardValidator {
    /******************************** Global Variables & Constants ********************************/
    private static final Pattern PATTERN_EMPTY_NAME = Pattern.compile("^\\d{2}\\*{5}\\d{3}$");

    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        BasicProfile profile = UserDataClient.getBasicProfile(userId);
        String displayName = profile.getDisplayName();
        if(displayName.isEmpty() || PATTERN_EMPTY_NAME.matcher(displayName).find()) {
            return new ValidationResponse(false, "Username not set");
        }
        return new ValidationResponse(true, "OK");
    }
}
