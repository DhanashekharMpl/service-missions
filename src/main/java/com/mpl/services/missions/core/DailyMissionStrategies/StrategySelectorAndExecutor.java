package com.mpl.services.missions.core.DailyMissionStrategies;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.DailyMissionStrategies.Fantasy.CashFantasyContestValidator;
import com.mpl.services.missions.core.DailyMissionStrategies.Fantasy.FantasyContestValidator;
import com.mpl.services.missions.core.DailyMissionStrategies.Games.PlayCashGameValidator;
import com.mpl.services.missions.core.DailyMissionStrategies.Games.PlayFruitChopGameValidator;
import com.mpl.services.missions.core.DailyMissionStrategies.Games.PlayGameEntryFeeValidator;
import com.mpl.services.missions.core.DailyMissionStrategies.Games.PlayGameValidator;
import com.mpl.services.missions.core.DailyMissionStrategies.Wallet.DepositValidator;
import com.mpl.services.missions.core.DailyMissionStrategies.Wallet.ReferralValidator;
import com.mpl.services.missions.core.DailyMissionStrategies.Wallet.WithdrawalValidator;
import com.mpl.services.missions.models.EventValidationResponse;
import com.mpl.services.missions.utils.Constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: Srijan Singh
 */
public class StrategySelectorAndExecutor {
    private static final Map<String, Class> DAILY_MISSION_VALIDATORS = new HashMap<>();
    private static final MLogger logger = new MLogger(StrategySelectorAndExecutor.class);

    public static void resetValidators() {
        DAILY_MISSION_VALIDATORS.clear();
        //GAMES BUCKET
        DAILY_MISSION_VALIDATORS.put("PLAY_FRUIT_CHOP_BATTLE", PlayFruitChopGameValidator.class);
        DAILY_MISSION_VALIDATORS.put("PLAY_2_GAMES", PlayGameValidator.class);
        DAILY_MISSION_VALIDATORS.put("PLAY_CASH_GAME", PlayCashGameValidator.class);
        DAILY_MISSION_VALIDATORS.put("PLAY_3_GAMES", PlayGameValidator.class);
        DAILY_MISSION_VALIDATORS.put("PLAY_FIRST_GAME", PlayGameValidator.class);
        DAILY_MISSION_VALIDATORS.put("PLAY_50_GAMES", PlayGameValidator.class);
        DAILY_MISSION_VALIDATORS.put("PLAY_100_GAMES", PlayGameValidator.class);

        //FANTASY BUCKET
        DAILY_MISSION_VALIDATORS.put("JOIN_FANTASY_CONTEST", FantasyContestValidator.class);
        DAILY_MISSION_VALIDATORS.put("JOIN_1_CASH_CONTEST", CashFantasyContestValidator.class);
        DAILY_MISSION_VALIDATORS.put("JOIN_3_CASH_CONTEST", CashFantasyContestValidator.class);
        DAILY_MISSION_VALIDATORS.put("JOIN_5_CASH_CONTEST", CashFantasyContestValidator.class);
        DAILY_MISSION_VALIDATORS.put("JOIN_2_FANTASY_CONTEST", FantasyContestValidator.class);
        DAILY_MISSION_VALIDATORS.put("JOIN_3_FANTASY_CONTEST", FantasyContestValidator.class);
        DAILY_MISSION_VALIDATORS.put("JOIN_5_FANTASY_CONTEST", FantasyContestValidator.class);
        DAILY_MISSION_VALIDATORS.put("JOIN_10_FANTASY_CONTEST", FantasyContestValidator.class);

        //WALLET BUCKET
        DAILY_MISSION_VALIDATORS.put("REFER_FRIEND", ReferralValidator.class);
        DAILY_MISSION_VALIDATORS.put("WITHDRAW_MONEY", WithdrawalValidator.class);
        DAILY_MISSION_VALIDATORS.put("DEPOSIT_FIRST_TIME", DepositValidator.class);
        DAILY_MISSION_VALIDATORS.put("DEPOSIT_SECOND_TIME", DepositValidator.class);

        //RUMMY BUCKET
        DAILY_MISSION_VALIDATORS.put("PLAY_RUMMY", PlayGameValidator.class);
        DAILY_MISSION_VALIDATORS.put("PLAY_3_RUMMY", PlayGameValidator.class);
        DAILY_MISSION_VALIDATORS.put("PLAY_5_RUMMY_FEE_50", PlayGameEntryFeeValidator.class);
        DAILY_MISSION_VALIDATORS.put("PLAY_7_RUMMY_FEE_100", PlayGameEntryFeeValidator.class);

    }

    public void select(String missionId, String key, ObjectNode payload, Constants.bucketEnum bucket, String segmentId) {
        try {
            if (!DAILY_MISSION_VALIDATORS.containsKey(missionId)) {
                throw new Exception("Validator not found for Daily Mission: " + missionId);
            }
            DailyMissionStrategy strategy = (DailyMissionStrategy) DAILY_MISSION_VALIDATORS.get(missionId)
                    .getConstructor().newInstance();
            logger.d("Selecting Strategy", strategy.getClass().getSimpleName(), "for Daily mission:", missionId);
            if (strategy.validate(missionId, key, payload, bucket, segmentId)) {
                EventValidationResponse eventValidationResponse = strategy.operate(missionId, key, payload, bucket);
                strategy.process(eventValidationResponse);
            }
        } catch (Exception e) {
            logger.e("Error in StrategySelectorAndExecutor Class ", e);
        }
    }
}
