package com.mpl.services.missions.core.validators;

/**
 * Author: Kaustubh Bhoyar
 */
public interface RewardValidator {
    ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception;
}
