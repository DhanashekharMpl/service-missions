package com.mpl.services.missions.core.validators.profile;

import com.mpl.services.missions.client.UserDataClient;
import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.userdata.grpc.BasicProfile;

/**
 * Author: Kaustubh Bhoyar
 */
public class ProfilePhotoValidator extends ProfileValidatorBase implements RewardValidator {
    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        BasicProfile profile = UserDataClient.getBasicProfile(userId);
        if(profile.getAvatarUrl().isEmpty()) {
            return new ValidationResponse(false, "Profile photo is not set");
        }
        return new ValidationResponse(true, "OK");
    }
}
