package com.mpl.services.missions.core.DailyMissionStrategies.Wallet;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.DailyMissionStrategies.DailyMissionStrategy;
import com.mpl.services.missions.models.EventValidationResponse;
import com.mpl.services.missions.utils.Constants;

import java.math.BigDecimal;

/**
 * Author: Srijan Singh
 */
public class DepositValidator implements DailyMissionStrategy {

    private static final MLogger logger = new MLogger(WithdrawalValidator.class);
    private static final String DEPOSIT_EVENT = "ACCOUNT_DEPOSITED";

    @Override
    public EventValidationResponse operate(String missionId, String key, ObjectNode payload, Constants.bucketEnum bucket) {
        logger.d("Deposit event : ", payload);
        BigDecimal amountDeposited = new BigDecimal(payload.get("amount").asText());
        EventValidationResponse eventValidationResponse = new EventValidationResponse();
        eventValidationResponse.setIncrementValue(amountDeposited.intValue());
        eventValidationResponse.setUserId(payload.get("userId").asInt());
        eventValidationResponse.setMissionId(missionId);
        eventValidationResponse.setBucketEnum(Constants.bucketEnum.WALLET);
        return eventValidationResponse;
    }

    @Override
    public Boolean validate(String missionId, String key, ObjectNode payload, Constants.bucketEnum bucket, String segmentId) {
        return key.equals(DEPOSIT_EVENT) && payload.has("amount");
    }
}
