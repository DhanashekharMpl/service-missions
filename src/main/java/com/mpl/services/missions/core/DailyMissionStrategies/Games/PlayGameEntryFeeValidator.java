package com.mpl.services.missions.core.DailyMissionStrategies.Games;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.client.UserDataClient;
import com.mpl.services.missions.core.DailyMissionStrategies.DailyMissionStrategy;
import com.mpl.services.missions.models.DailyMissionConfig;
import com.mpl.services.missions.models.EventValidationResponse;
import com.mpl.services.missions.utils.Constants;

import java.math.BigDecimal;

import static com.mpl.services.missions.utils.CountryWiseSegmentedConfigs.getDailyMissionConfigOrDefault;

/**
 * Author: Srijan Singh
 */
public class PlayGameEntryFeeValidator implements DailyMissionStrategy {
    private static final MLogger logger = new MLogger(PlayCashGameValidator.class);
    private static final String EVENT_USER_PLAYED_GAME = "USER_PLAYED_GAME";
    private static final String EVENT_USER_WON = "USER_WON";

    @Override
    public EventValidationResponse operate(String missionId, String key, ObjectNode payload, Constants.bucketEnum bucket) {
        logger.d("PLay Cash Game event : ", payload);
        EventValidationResponse eventValidationResponse = new EventValidationResponse();
        eventValidationResponse.setIncrementValue(1);
        eventValidationResponse.setUserId(payload.get("User ID").asInt());
        eventValidationResponse.setBucketEnum(bucket);
        eventValidationResponse.setMissionId(missionId);
        return eventValidationResponse;
    }

    @Override
    public Boolean validate(String missionId, String key, ObjectNode payload, Constants.bucketEnum bucket, String segmentId) {
        if (key.equals(EVENT_USER_WON) && payload.has("Game Format")
                && payload.has("\tEntry Currency") && payload.has("\tGame ID")
                && payload.has("Cash Entry Fee")) {

            String gameFormat = payload.get("Game Format").asText();
            String entryCurrency = payload.get("\tEntry Currency").asText();

            return ((gameFormat.equalsIgnoreCase("Deals Rummy")
                    || gameFormat.equalsIgnoreCase("Pool Rummy")
                    || gameFormat.equalsIgnoreCase("POINTS RUMMY"))
                    && entryCurrency.equalsIgnoreCase("CASH")
                    && rummyChecks(payload, missionId, segmentId));

        }
        else if (key.equals(EVENT_USER_PLAYED_GAME) && payload.has("\tTournament Type") &&
                payload.has("\tEntry Currency") && payload.has("Single Entry")
                && payload.has("\tGame ID") && payload.has("Cash Entry Fee")) {

            String tournamentType = payload.get("\tTournament Type").asText();
            String entryCurrency = payload.get("\tEntry Currency").asText();
            boolean singleEntry = payload.get("Single Entry").asBoolean();

            return ((tournamentType.equals("1V1") || tournamentType.equals("Default") || tournamentType.equals("1v1 Async")) &&
                    entryCurrency.equalsIgnoreCase("CASH") && !singleEntry && rummyChecks(payload, missionId, segmentId));

        }
        else
            return Boolean.FALSE;
    }

    public Boolean rummyChecks(ObjectNode payload, String missionId, String segmentId) {
        try {
            int userId = payload.get("User ID").asInt();
            String gameId = payload.get("\tGame ID").asText();
            BigDecimal entryFee = new BigDecimal(payload.get("Cash Entry Fee").asText());
            com.mpl.services.userdata.grpc.BasicProfile profile = UserDataClient.getBasicProfile(userId);
            DailyMissionConfig dailyMissionConfig = getDailyMissionConfigOrDefault(profile.getCountryCode(), segmentId);
            DailyMissionConfig.ZkBucket bucket = dailyMissionConfig.getRummy()
                    .stream().filter(i -> i.getMissionId().equals(missionId)).findFirst().orElse(null);
            if (bucket == null) return false;
            if (bucket.getIncludedGameIds() != null && bucket.getIncludedGameIds().size() > 0)
                return bucket.getIncludedGameIds().contains(gameId) && entryFee.compareTo(bucket.getSecondaryEventCount()) > 0;
            else if (bucket.getExcludedGameIds() != null && bucket.getExcludedGameIds().size() > 0 && bucket.getExcludedGameIds().contains(gameId)) {
                return false;
            }
            else
                return entryFee.compareTo(bucket.getSecondaryEventCount()) > 0;
        } catch (Exception e) {
            logger.e("Error in fetching User Profile", e);
            return false;
        }
    }

}
