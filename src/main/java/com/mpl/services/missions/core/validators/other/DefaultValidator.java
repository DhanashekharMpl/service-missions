package com.mpl.services.missions.core.validators.other;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;

public class DefaultValidator implements RewardValidator {
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        return new ValidationResponse(true, "OK");
    }
}
