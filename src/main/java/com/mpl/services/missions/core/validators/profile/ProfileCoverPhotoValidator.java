package com.mpl.services.missions.core.validators.profile;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.userdata.grpc.GetExtendedProfileRequest;
import com.mpl.services.userdata.grpc.GetExtendedProfileResponse;
import com.mpl.services.userdata.grpc.UserDataServiceGrpc;

/**
 * Author: Kaustubh Bhoyar
 */
public class ProfileCoverPhotoValidator implements RewardValidator {
    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        GetExtendedProfileRequest rq = GetExtendedProfileRequest.newBuilder()
                .setUserId(userId)
                .build();
        UserDataServiceGrpc.UserDataServiceBlockingStub stub = ResourceManager
                .getInstance().getUserDataServiceBlocking();
        GetExtendedProfileResponse response = stub.getExtendedProfile(rq);
        if(response.getExtendedProfile().getCoverPhotosMap().isEmpty()) {
            return new ValidationResponse(false, "Cover photo is not set");
        }
        return new ValidationResponse(true, "OK");
    }
}
