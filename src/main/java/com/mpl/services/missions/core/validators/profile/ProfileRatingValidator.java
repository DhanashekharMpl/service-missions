package com.mpl.services.missions.core.validators.profile;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.userdata.grpc.ExtendedProfileField;

/**
 * Author: Kaustubh Bhoyar
 */
public class ProfileRatingValidator extends ProfileValidatorBase implements RewardValidator {
    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        String rating = super.getExtendedFieldValue(requestId, userId, ExtendedProfileField.RATING);
        if(rating != null && !rating.isEmpty()) {
            return new ValidationResponse(true, "OK");
        }
        if(verificationData == null || verificationData.isEmpty()) {
            return new ValidationResponse(false, "Please rate the app");
        }
        super.setExtendedFieldValue(requestId, userId, ExtendedProfileField.RATING, verificationData);
        return new ValidationResponse(true, "OK");
    }
}
