package com.mpl.services.missions.core.validators.accounts;

import com.mpl.services.accounts.WithdrawalMode;
import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;

/**
 * Author: Vishal Pahuja
 */

public class WithdrawBankValidator extends AccountsValidatorBase implements RewardValidator {

    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        boolean response = super.isWithdrawer(requestId, userId, WithdrawalMode.banktransfer, countryId);
        if(!response) {
            return new ValidationResponse(false, "User has not withdrawn using Bank Account");
        }
        return new ValidationResponse(true, "OK");
    }
}
