package com.mpl.services.missions.core.validators.ugx;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.missions.models.MissionConfig;
import com.mpl.services.missions.utils.Configurations;
import com.mpl.services.missions.utils.CountryWiseConfig;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.uge.EventType;
import com.mpl.services.uge.GetHostFinishedCountRequest;
import com.mpl.services.uge.GetHostFinishedCountResponse;
import com.mpl.services.uge.UserGeneratedServiceGrpc;

public class GroupChallengeValidator implements RewardValidator {
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {

        UserGeneratedServiceGrpc.UserGeneratedServiceBlockingStub stub = ResourceManager.getInstance().getUserGeneratedServiceBlockingStub();
      MissionConfig missionConfig=  CountryWiseConfig.getInstance(countryId).getMissionConfigById("OPEN_DEEPLINK_GROUP_CHALLENGE_PLAYED");
        GetHostFinishedCountRequest request = GetHostFinishedCountRequest.newBuilder()
                .setUserId(userId)
                .setRequestId("Missions")
                .setEventType(EventType.ALL)
                .build();
        GetHostFinishedCountResponse response = stub.getHostFinishedCount(request);
        boolean isValid= missionConfig.getExtraData().get("count").intValue()<=response.getFinishedCount()?true:false;
        return new ValidationResponse(isValid, "OK");

    }
}
