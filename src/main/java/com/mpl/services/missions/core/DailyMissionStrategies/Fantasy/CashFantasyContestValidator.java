package com.mpl.services.missions.core.DailyMissionStrategies.Fantasy;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.DailyMissionStrategies.DailyMissionStrategy;
import com.mpl.services.missions.models.EventValidationResponse;
import com.mpl.services.missions.utils.Constants;

import static com.mpl.services.missions.utils.Constants.bucketEnum.FANTASY;

/**
 * Author: Srijan Singh
 */
public class CashFantasyContestValidator implements DailyMissionStrategy {
    private static final MLogger logger = new MLogger(CashFantasyContestValidator.class);

    @Override
    public EventValidationResponse operate(String missionId, String key, ObjectNode payload, Constants.bucketEnum bucket) {
        logger.d("PLay Cash Fantasy event : ", payload);
        EventValidationResponse eventValidationResponse = new EventValidationResponse();
        eventValidationResponse.setIncrementValue(1);
        eventValidationResponse.setUserId(payload.get("User ID").asInt());
        eventValidationResponse.setBucketEnum(FANTASY);
        eventValidationResponse.setMissionId(missionId);
        return eventValidationResponse;
    }

    @Override
    public Boolean validate(String missionId, String key, ObjectNode payload, Constants.bucketEnum bucket, String segmentId) {
        if (payload.has("Entry Currency") && payload.has("Is Success")) {
            String tournamentType = payload.get("Tournament Type").asText();
            String entryCurrency = payload.get("Entry Currency").asText();
            boolean isSuccess = payload.get("Is Success").asBoolean();

            return (tournamentType.equals("Fantasy") && entryCurrency.equalsIgnoreCase("CASH") && isSuccess);
        }
        return Boolean.FALSE;
    }
}
