package com.mpl.services.missions.core.validators.video;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.video.GetVideosByUserIdRequest;
import com.mpl.services.video.GetVideosByUserIdResponse;
import com.mpl.services.video.VideoServiceGrpc;

/**
 * Author: Kaustubh Bhoyar
 */
public class VideoUploadCount implements RewardValidator {
    /******************************** Global Variables & Constants ********************************/
    private static final int COUNT_VIDEOS = 20;

    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        VideoServiceGrpc.VideoServiceBlockingStub stub = ResourceManager
                .getInstance().getVideoServiceBlocking();
        GetVideosByUserIdRequest request = GetVideosByUserIdRequest.newBuilder()
                .setRequestId(requestId)
                .setUserId(userId)
                .setCount(COUNT_VIDEOS + 1).build();
        GetVideosByUserIdResponse response = stub.getVideosByUserId(request);
        if(!response.getError().getMessage().equals("")) {
            throw new Exception("Failed to fetch videos from VideoService MSG: "
                    + response.getError().getMessage());
        }

        if(response.getVideosList().size() < COUNT_VIDEOS) {
            return new ValidationResponse(false, "Upload "
                    + COUNT_VIDEOS + " videos to get reward");
        }
        return new ValidationResponse(true, "OK");
    }
}
