package com.mpl.services.missions.core.validators.kyc;

import com.mpl.services.kyc.CombineKycResponse;
import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;

/**
 * Author: Kaustubh Bhoyar
 */
public class KycEmailValidator extends KycValidatorBase implements RewardValidator {
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        CombineKycResponse response = super.getKYCDetails(requestId, userId);
        if(!response.getEmailKycResponse().getVerified()) {
            return new ValidationResponse(false, "KYC email is not verified");
        }
        return new ValidationResponse(true, "OK");
    }
}
