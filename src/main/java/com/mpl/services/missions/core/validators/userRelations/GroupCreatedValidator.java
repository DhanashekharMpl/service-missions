package com.mpl.services.missions.core.validators.userRelations;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.missions.models.MissionConfig;
import com.mpl.services.missions.utils.Configurations;
import com.mpl.services.missions.utils.CountryWiseConfig;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.relation.grpc.GetUserGroupsCountRequest;
import com.mpl.services.relation.grpc.GetUserGroupsCountResponse;
import com.mpl.services.relation.grpc.UserRelationServiceGrpc;

public class GroupCreatedValidator implements RewardValidator {

    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        UserRelationServiceGrpc.UserRelationServiceBlockingStub stub = ResourceManager.getInstance().getUserRelationsV2ServiceBlockingStub();
        GetUserGroupsCountRequest request = GetUserGroupsCountRequest.newBuilder()
                .setUserId(userId)
                .setRequestId("Missions")
                .build();
        GetUserGroupsCountResponse response = stub.getUserGroupsCount(request);
        MissionConfig missionConfig = CountryWiseConfig.getInstance(countryId).getMissionConfigById("OPEN_DEEPLINK_GROUP_CREATION");
        boolean isValid =  missionConfig.getExtraData().get("count").intValue()<=response.getCount()?true:false;

        return new ValidationResponse(isValid, "OK");
    }
}
