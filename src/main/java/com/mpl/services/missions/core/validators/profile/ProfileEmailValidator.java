package com.mpl.services.missions.core.validators.profile;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.userdata.grpc.ExtendedProfileField;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author: Kaustubh Bhoyar
 */
public class ProfileEmailValidator extends ProfileValidatorBase implements RewardValidator {
    /******************************** Global Variables & Constants ********************************/
    // RFC 5322 - Official Email Regex //
    private static final Pattern REGEX = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");

    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        String email = super.getExtendedFieldValue(requestId, userId, ExtendedProfileField.EMAIL);
        if(email != null && !email.isEmpty()) {
            return new ValidationResponse(true, "OK");
        }
        if(verificationData == null || verificationData.isEmpty()) {
            return new ValidationResponse(false, "Email is not set");
        }
        verificationData = verificationData.toLowerCase();

        Matcher matcher = REGEX.matcher(verificationData);
        if(matcher.matches()) {
            super.setExtendedFieldValue(requestId, userId, ExtendedProfileField.EMAIL, verificationData);
            return new ValidationResponse(true, "OK");
        }
        return new ValidationResponse(false, "Invalid value " + verificationData + " for field email");
    }
}
