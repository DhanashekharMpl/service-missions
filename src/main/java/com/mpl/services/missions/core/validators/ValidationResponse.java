package com.mpl.services.missions.core.validators;

/**
 * Author: Kaustubh Bhoyar
 */
public class ValidationResponse {
    /******************************** Global Variables & Constants ********************************/
    private boolean isValid;
    private String message;

    /**************************************** Constructors ****************************************/
    public ValidationResponse(boolean isValid, String message) {
        this.isValid = isValid;
        this.message = message;
    }

    /************************************** Public Functions **************************************/
    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
