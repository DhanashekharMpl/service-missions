package com.mpl.services.missions.core.validators;


import com.fasterxml.jackson.databind.node.ObjectNode;

public interface GenericRewardValidator {
    ValidationResponse validate(String requestId, int userId, ObjectNode verificationData) throws Exception;
}
