package com.mpl.services.missions.core.validators.fantasy;

import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.MissionCenter;
import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.superteam.SuperTeamServiceGrpc;
import com.mpl.services.superteam.ValidateIfUserAlreadyRegisteredForRequest;
import com.mpl.services.superteam.ValidateIfUserAlreadyRegisteredForResponse;
import com.mpl.services.userdata.grpc.*;

import java.util.ArrayList;
import java.util.List;

public class TeamsRegisteredValidator implements RewardValidator {
    private static final MLogger logger = new MLogger(MissionCenter.class);
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        List<Segment> segmentList = new ArrayList<>();
        segmentList.add(Segment.ACCOUNT_INFO);
        segmentList.add(Segment.FANTASY_STATS);

        UserDataServiceGrpc.UserDataServiceBlockingStub stub = ResourceManager.getInstance().getUserDataServiceBlocking();
        com.mpl.services.userdata.grpc.GetUserDataRequest getUserDataRequest = GetUserDataRequest.newBuilder().setUserId(userId).addAllIncludeSegments(segmentList).build();
        GetUserDataResponse getUserDataResponse = stub.getUserData(getUserDataRequest);

        int totalPlayed = getTotalFantasyPlayed(getUserDataResponse.getUserData().getFantasyStats());
        logger.i("Total Fantasy played from User data history for user Id ", userId, totalPlayed);
        if(totalPlayed<=0){
            ValidateIfUserAlreadyRegisteredForRequest rq = ValidateIfUserAlreadyRegisteredForRequest.newBuilder().setUserId(userId).setSportId(7).build();
            SuperTeamServiceGrpc.SuperTeamServiceBlockingStub fantasyServiceBlockingStub = ResourceManager.getInstance().getSuperteamCricketServiceBlockingStub();
            ValidateIfUserAlreadyRegisteredForResponse response = fantasyServiceBlockingStub.validateIfUserAlreadyRegisteredForMatch(rq);
            logger.i("Total Fantasy played from Fantasy data for user Id ", userId, response.getIsSuperTeamPlayed());
            if(response.getIsSuperTeamPlayed()){
                return new ValidationResponse(true, "OK");
            }else{
                return new ValidationResponse(false, "Not Played Fantasy");
            }
        }else{
            return new ValidationResponse(true, "OK");
        }
    }

    private int getTotalFantasyPlayed(FantasyStats fantasyStats) {
        int total = 0;
        logger.i("Total Fantasy played fantasyStats ", fantasyStats);
        total += fantasyStats.getBaseballPlayed();
        total += fantasyStats.getBasketballPlayed();
        total += fantasyStats.getCricketPlayed();
        total += fantasyStats.getFootballPlayed();
        total += fantasyStats.getHockeyPlayed();
        total += fantasyStats.getKabaddiPlayed();
        total += fantasyStats.getStockPlayed();

        return total;
    }
}
