package com.mpl.services.missions.core.validators.accounts;

import com.mpl.services.accounts.PaymentMode;
import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;

/**
 * Author: Vishal Pahuja
 */

public class PayerPaytmValidator extends AccountsValidatorBase implements RewardValidator {

    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        boolean response = super.isPayer(requestId, userId, PaymentMode.Paytm, countryId);
        if(!response) {
            return new ValidationResponse(false, "User has not payed using Paytm");
        }
        return new ValidationResponse(true, "OK");
    }
}
