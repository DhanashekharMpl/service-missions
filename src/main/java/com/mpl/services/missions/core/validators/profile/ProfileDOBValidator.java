package com.mpl.services.missions.core.validators.profile;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.userdata.grpc.ExtendedProfileField;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Author: Kaustubh Bhoyar
 */
public class ProfileDOBValidator extends ProfileValidatorBase implements RewardValidator {
    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        String dob = super.getExtendedFieldValue(requestId, userId, ExtendedProfileField.DOB);
        if(dob != null && !dob.isEmpty()) {
            return new ValidationResponse(true, "OK");
        }
        if(verificationData == null || verificationData.isEmpty()) {
            return new ValidationResponse(false, "DOB is not set");
        }
        try {
            DateTime.parse(verificationData, DateTimeFormat.forPattern("YYYY-MM-DD"));
        } catch (Exception ignored) {
            return new ValidationResponse(false, "Invalid DOB");
        }
        super.setExtendedFieldValue(requestId, userId, ExtendedProfileField.DOB, verificationData);
        return new ValidationResponse(true, "OK");
    }
}
