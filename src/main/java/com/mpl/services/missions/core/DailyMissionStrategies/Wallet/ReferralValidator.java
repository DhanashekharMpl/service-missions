package com.mpl.services.missions.core.DailyMissionStrategies.Wallet;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.DailyMissionCentre;
import com.mpl.services.missions.core.DailyMissionStrategies.DailyMissionStrategy;
import com.mpl.services.missions.models.EventValidationResponse;
import com.mpl.services.missions.utils.Constants;

/**
 * Author: Srijan Singh
 */
public class ReferralValidator implements DailyMissionStrategy {
    private static final MLogger logger = new MLogger(ReferralValidator.class);
    private static final String USER_GOT_REFERRED = "RS_USER_GOT_REFERRED";

    @Override
    public EventValidationResponse operate(String missionId, String key, ObjectNode payload , Constants.bucketEnum bucket) {
        logger.d("Referral event : ", payload);
        EventValidationResponse eventValidationResponse = new EventValidationResponse();
        eventValidationResponse.setIncrementValue(1);
        eventValidationResponse.setUserId(payload.get("referrerId").asInt());
        eventValidationResponse.setBucketEnum(Constants.bucketEnum.WALLET);
        eventValidationResponse.setMissionId(missionId);
        return eventValidationResponse;
    }

    @Override
    public Boolean validate(String missionId, String key, ObjectNode payload , Constants.bucketEnum bucket ,String segmentId) {
        return key.equals(USER_GOT_REFERRED);
    }
}
