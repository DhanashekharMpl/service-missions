package com.mpl.services.missions.core.validators.profile;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.userdata.grpc.ExtendedProfileField;

/**
 * Author: Kaustubh Bhoyar
 */
public class ProfileGenderValidator extends ProfileValidatorBase implements RewardValidator {
    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        String gender = super.getExtendedFieldValue(requestId, userId, ExtendedProfileField.GENDER);
        if(gender != null && !gender.isEmpty()) {
            return new ValidationResponse(true, "OK");
        }
        if(verificationData == null || verificationData.isEmpty()) {
            return new ValidationResponse(false, "Gender is not set");
        }
        verificationData = verificationData.toUpperCase();
        if(verificationData.equals("M") || verificationData.equals("F") || verificationData.equals("O")) {
            super.setExtendedFieldValue(requestId, userId, ExtendedProfileField.GENDER, verificationData);
            return new ValidationResponse(true, "OK");
        }
        return new ValidationResponse(false, "Invalid value " + verificationData + " for field gender");
    }
}
