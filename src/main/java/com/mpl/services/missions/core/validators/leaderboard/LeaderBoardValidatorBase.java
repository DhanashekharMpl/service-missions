package com.mpl.services.missions.core.validators.leaderboard;

import com.mpl.services.leaderboards.grpc.*;
import com.mpl.services.missions.utils.ResourceManager;

import java.util.HashMap;
import java.util.Map;

public class LeaderBoardValidatorBase {

    protected Map<Integer, Integer> getGameCount(String requestId, int userId) throws Exception {

        LeaderBoardServiceGrpc.LeaderBoardServiceBlockingStub stub = ResourceManager.getInstance()
                                                                                    .getLeaderBoardServiceBlocking();
        GameCountRequest gameCountRequest = GameCountRequest.newBuilder()
                                                            .setRequestId(requestId)
                                                            .setUserId(userId)
                                                            .build();
        GameCountResponse response = stub.getGameCounts(gameCountRequest);

        if(response.getError().getReason() != LeaderboardServiceError.Reason.NONE) {
            throw new Exception(String.format("Failed to fetch following %s %s",
                    response.getError().getReason(), response.getError().getMessage()));
        }

        Map<Integer, Integer> gameIdGameCountMap = new HashMap<>();
        for (GameCount gameCount : response.getGamecountsList()) {
            gameIdGameCountMap.put(gameCount.getGameId(), gameCount.getCount());
        }

        return gameIdGameCountMap;
    }
}
