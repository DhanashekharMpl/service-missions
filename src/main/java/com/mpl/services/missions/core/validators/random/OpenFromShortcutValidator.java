package com.mpl.services.missions.core.validators.random;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;

/**
 * Author: Kaustubh Bhoyar
 */
public class OpenFromShortcutValidator implements RewardValidator {
    /******************************** Global Variables & Constants ********************************/
    private static final int MIN_UNIQUE_GAME_COUNT = 2;

    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        int uniqueGamesOpened = Integer.parseInt(verificationData);
        return uniqueGamesOpened >= MIN_UNIQUE_GAME_COUNT
                ? new ValidationResponse(true, "OK")
                : new ValidationResponse(false, "Task Not Complete");
    }

    /************************************* Core Private Logic *************************************/
    /************************************** Private Classes ***************************************/
}
