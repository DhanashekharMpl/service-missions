package com.mpl.services.missions.core.DailyMissionStrategies;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.services.missions.core.DailyMissionCentre;
import com.mpl.services.missions.core.DailyMissionCentreAsync;
import com.mpl.services.missions.models.EventValidationResponse;
import com.mpl.services.missions.utils.Constants;

/**
 * Author: Srijan Singh
 */
public interface DailyMissionStrategy {
    EventValidationResponse operate(String missionId, String key, ObjectNode payload , Constants.bucketEnum bucket);

    Boolean validate(String missionId, String key, ObjectNode payload , Constants.bucketEnum bucket, String segmentId);

    default void process(EventValidationResponse eventValidationResponse) throws Exception {
        DailyMissionCentreAsync dailyMissionCentre = new DailyMissionCentreAsync();
        dailyMissionCentre.updateMissionEventsToRedis(eventValidationResponse);
    }
}
