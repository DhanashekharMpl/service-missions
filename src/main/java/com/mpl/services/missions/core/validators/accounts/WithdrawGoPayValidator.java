package com.mpl.services.missions.core.validators.accounts;

import com.mpl.services.accounts.WithdrawalMode;
import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;

/**
 * Author: Vishal Pahuja
 */

public class WithdrawGoPayValidator extends AccountsValidatorBase implements RewardValidator {

    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        boolean response = super.isWithdrawer(requestId, userId, WithdrawalMode.gopay, countryId);
        if(!response) {
            return new ValidationResponse(false, "User has not withdrawn using GoPay");
        }
        return new ValidationResponse(true, "OK");
    }
}