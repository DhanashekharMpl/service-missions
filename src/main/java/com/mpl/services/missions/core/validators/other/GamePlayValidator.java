package com.mpl.services.missions.core.validators.other;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.services.missions.core.validators.GenericRewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;
import com.mpl.services.missions.core.validators.leaderboard.LeaderBoardValidatorBase;

import java.util.Map;

public class GamePlayValidator extends LeaderBoardValidatorBase implements GenericRewardValidator {

    @Override
    public ValidationResponse validate(String requestId, int userId, ObjectNode verificationData) throws Exception {

        Map<Integer, Integer> map = super.getGameCount(requestId, userId);

        Integer gameId = verificationData.get("gameId").asInt();
        if (map.get(gameId) != null){
            Integer gamePlayedCount = map.get(gameId);
            Integer receivedCount = verificationData.get("gameCount")
                                                    .asInt();
            if (gamePlayedCount >= receivedCount) {
                return new ValidationResponse(true, "OK");
            }
        }

        return new ValidationResponse(false, "Condition not satisfied");

    }
}
