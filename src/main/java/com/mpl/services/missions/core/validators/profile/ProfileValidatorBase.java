package com.mpl.services.missions.core.validators.profile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.relation.grpc.GetBasicFollowDetailsRequest;
import com.mpl.services.relation.grpc.UserRelationServiceGrpc;
import com.mpl.services.userdata.grpc.*;
import com.mpl.services.userdata.grpc.ExtendedProfileField;
import com.mpl.services.userdata.grpc.GetExtendedFieldRequest;
import com.mpl.services.userdata.grpc.GetExtendedFieldResponse;
import com.mpl.services.userdata.grpc.SetExtendedFieldRequest;
import io.grpc.StatusRuntimeException;

/**
 * Author: Kaustubh Bhoyar
 */
public abstract class ProfileValidatorBase {
    /******************************** Global Variables & Constants ********************************/
    /**************************************** Constructors ****************************************/
    /************************************** Public Functions **************************************/
    protected String getExtendedFieldValue(String requestId, int userId, ExtendedProfileField field) throws Exception {
        UserDataServiceGrpc.UserDataServiceBlockingStub stub = ResourceManager
                .getInstance().getUserDataServiceBlocking();
        GetExtendedFieldRequest request = GetExtendedFieldRequest.newBuilder()
                .setRequestId(requestId)
                .setFieldName(field)
                .setUserId(userId)
                .build();
        String msg = "INTERNAL_SERVER_ERROR";
        String reason = "Internal Server error. Please try again later.";
        try {
            GetExtendedFieldResponse response = stub.getExtendedField(request);
            return response.getFieldValue();
        }
        catch (Exception e){
            e.printStackTrace();
            if (e instanceof StatusRuntimeException) {
                    ObjectNode o = new ObjectMapper().readValue(
                            e.getMessage().substring("INTERNAL: ".length()),
                            ObjectNode.class
                    );
                    String c = o.get("code").asText();

                    switch (c) {
                        case "EXTENDED_FIELD_NOT_SET":
                            return null;
                    }
                }
            }
        throw new Exception(String.format("Failed to fetch extended field %s %s",
                reason, msg));
    }

    protected void setExtendedFieldValue(String requestId, int userId, ExtendedProfileField field, String value) throws Exception {
        UserDataServiceGrpc.UserDataServiceBlockingStub stub = ResourceManager
                .getInstance().getUserDataServiceBlocking();
        SetExtendedFieldRequest request = SetExtendedFieldRequest
                .newBuilder()
                .setUserId(userId)
                .setRequestId(requestId)
                .setFieldName(field)
                .setFieldValue(value)
                .build();
        try {
            stub.setExtendedField(request);
        }
        catch (Exception e){
            e.printStackTrace();
            String msg = "INTERNAL_SERVER_ERROR";
            String reason = "Internal Server error. Please try again later.";
            throw new Exception(String.format("Failed to fetch extended field %s %s",
                    reason, msg));
        }
    }

    protected int getFollowersList(String requestId, int userId) throws Exception {
        UserRelationServiceGrpc.UserRelationServiceBlockingStub stub = ResourceManager
                .getInstance().getUserRelationsV2ServiceBlockingStub();
        GetBasicFollowDetailsRequest request = GetBasicFollowDetailsRequest.newBuilder()
                .setRequestId(requestId)
                .setUserId(userId)
                .build();
        try {
            com.mpl.services.relation.grpc.UserFollowDetails response = stub.getBasicFollowDetails(request);
            return response.getFollowers();
        } catch (Exception e) {
            String msg = "INTERNAL_SERVER_ERROR";
            String reason = "Internal Server error. Please try again later.";
            throw new Exception(String.format("Failed to fetch followers %s %s",
                    msg, reason));
        }
    }

    /************************************* Core Private Logic *************************************/
    /************************************** Private Classes ***************************************/
}
