package com.mpl.services.missions.core.validators.referral;

import com.mpl.services.missions.core.validators.RewardValidator;
import com.mpl.services.missions.core.validators.ValidationResponse;

/**
 * Author: Kaustubh Bhoyar
 */
public class InviteAndEarnValidator implements RewardValidator {
    /******************************** Global Variables & Constants ********************************/
    private static final int MIN_INVITES = 10;

    /************************************** Public Functions **************************************/
    @Override
    public ValidationResponse validate(String requestId, int userId, String countryId, String verificationData) throws Exception {
        int invitesSent = Integer.parseInt(verificationData);
        return invitesSent >= MIN_INVITES
                ? new ValidationResponse(true, "OK")
                : new ValidationResponse(false, "Invite 10 friends to earn rewards");
    }
}
