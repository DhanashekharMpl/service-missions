package com.mpl.services.missions.core.DailyMissionStrategies.Wallet;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.DailyMissionCentre;
import com.mpl.services.missions.core.DailyMissionStrategies.DailyMissionStrategy;
import com.mpl.services.missions.models.EventValidationResponse;
import com.mpl.services.missions.utils.Constants;

import java.math.BigDecimal;

/**
 * Author: Srijan Singh
 */
public class WithdrawalValidator implements DailyMissionStrategy {

    private static final MLogger logger = new MLogger(DepositValidator.class);
    private static final String WITHDRAWAL_EVENT = "USER_MONEY_WITHDRAWAL";

    @Override
    public EventValidationResponse operate(String missionId, String key, ObjectNode payload , Constants.bucketEnum bucket) {
        logger.d("Withdraw event : ", payload);
        BigDecimal amount = new BigDecimal(payload.get("amount").asText());
        EventValidationResponse eventValidationResponse = new EventValidationResponse();
        eventValidationResponse.setIncrementValue(amount.intValue());
        eventValidationResponse.setUserId(payload.get("userId").asInt());
        eventValidationResponse.setMissionId(missionId);
        eventValidationResponse.setBucketEnum(Constants.bucketEnum.WALLET);
        return eventValidationResponse;
    }

    @Override
    public Boolean validate(String missionId, String key, ObjectNode payload , Constants.bucketEnum bucket, String segmentId) {
        return key.equals(WITHDRAWAL_EVENT) && payload.has("amount")
                && payload.has("isSuccess") && payload.get("isSuccess").asBoolean();
    }
}
