package com.mpl.services.missions.core.DailyMissionStrategies.Games;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.client.UserDataClient;
import com.mpl.services.missions.core.DailyMissionStrategies.DailyMissionStrategy;
import com.mpl.services.missions.models.DailyMissionConfig;
import com.mpl.services.missions.models.EventValidationResponse;
import com.mpl.services.missions.utils.Constants;

import java.util.Collection;
import java.util.stream.Stream;

import static com.mpl.services.missions.utils.CountryWiseSegmentedConfigs.getDailyMissionConfigOrDefault;


/**
 * Author: Srijan Singh
 */
public class PlayGameValidator implements DailyMissionStrategy {
    private static final MLogger logger = new MLogger(PlayGameValidator.class);
    private static final String EVENT_USER_PLAYED_GAME = "USER_PLAYED_GAME";
    private static final String EVENT_USER_REGISTERED = "USER_REGISTERED";
    private static final String EVENT_USER_WON = "USER_WON";

    @Override
    public EventValidationResponse operate(String missionId, String key, ObjectNode payload, Constants.bucketEnum bucket) {
        logger.d("PLay Game event : ", payload);
        EventValidationResponse eventValidationResponse = new EventValidationResponse();
        eventValidationResponse.setIncrementValue(1);
        eventValidationResponse.setUserId(payload.get("User ID").asInt());
        eventValidationResponse.setBucketEnum(bucket);
        eventValidationResponse.setMissionId(missionId);
        return eventValidationResponse;
    }

    @Override
    public Boolean validate(String missionId, String key, ObjectNode payload, Constants.bucketEnum bucket, String segmentId) {
        if (key.equals(EVENT_USER_WON) && payload.has("Game Format")
                && payload.has("\tEntry Currency")) {

            String gameFormat = payload.get("Game Format").asText();

            return ((gameFormat.equalsIgnoreCase("Deals Rummy") ||
                    gameFormat.equalsIgnoreCase("Pool Rummy") ||
                    gameFormat.equalsIgnoreCase("POINTS RUMMY")) && gameIdChecks(payload, missionId, segmentId));
        }
        else if (key.equals(EVENT_USER_PLAYED_GAME) && payload.has("Single Entry")) {

            String tournamentType = payload.get("\tTournament Type").asText();
            boolean singleEntry = payload.get("Single Entry").asBoolean();

            return ((tournamentType.equals("1V1") || tournamentType.equals("Default") || tournamentType.equals("1v1 Async")) && !singleEntry
                    && gameIdChecks(payload, missionId, segmentId));
        }
        else if (key.equals(EVENT_USER_REGISTERED) && payload.has("\tTournament Type") && payload.has("Is Success")) {

            String tournamentType = payload.get("\tTournament Type").asText();
            boolean isSuccess = payload.get("Is Success").asBoolean();
            return ((tournamentType.equals("1V1") || tournamentType.equals("Default") || tournamentType.equals("1v1 Async"))
                    && isSuccess && gameIdChecks(payload, missionId, segmentId));
        }
        else
            return Boolean.FALSE;
    }

    public Boolean gameIdChecks(ObjectNode payload, String missionId, String segmentId) {
        try {
            int userId = payload.get("User ID").asInt();
            String gameId = payload.get("\tGame ID").asText();
            com.mpl.services.userdata.grpc.BasicProfile profile = UserDataClient.getBasicProfile(userId);
            DailyMissionConfig dailyMissionConfig = getDailyMissionConfigOrDefault(profile.getCountryCode(), segmentId);
            Stream<DailyMissionConfig.ZkBucket> resultStream = Stream.of(dailyMissionConfig.getGames(),
                    dailyMissionConfig.getRummy()).flatMap(Collection::stream);

            DailyMissionConfig.ZkBucket bucket = resultStream.filter(i -> i.getMissionId().equals(missionId))
                    .findFirst().orElse(null);
            if (bucket == null) return false;
            if (bucket.getIncludedGameIds() != null && bucket.getIncludedGameIds().size() > 0)
                return bucket.getIncludedGameIds().contains(gameId);
            else return bucket.getExcludedGameIds() == null || bucket.getExcludedGameIds().size() <= 0
                    || !bucket.getExcludedGameIds().contains(gameId);
        } catch (Exception e) {
            logger.e("Error in fetching User Profile", e);
            return false;
        }
    }
}
