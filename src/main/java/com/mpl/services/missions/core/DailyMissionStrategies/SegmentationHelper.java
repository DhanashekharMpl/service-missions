package com.mpl.services.missions.core.DailyMissionStrategies;

import com.mpl.services.missions.client.SegmentationClient;
import com.mpl.services.missions.utils.CountryWiseSegmentedConfigs;

import java.util.List;

/**
 * Author: Srijan Singh
 */
public class SegmentationHelper {
//    public static String checkUserInFTUE(Integer userId, String countryId) {
//        List<String> segmentList = SegmentationClient.getSegmentsForUser(userId);
//        if (segmentList != null) {
//            CountryWiseSegmentedConfigs configs = CountryWiseSegmentedConfigs.getInstance(countryId);
//            String segment = segmentList.stream().filter(i -> configs.getExistingSegments().contains(i))
//                    .findFirst().orElse(null);
//            return segment;
//        }
//        return null;
//    }

    public static String validateUserSegmentBeforeFtueAddition(Integer userId, String countryId) {
        List<String> segmentList = SegmentationClient.getSegmentsForUser(userId);
        if (segmentList != null) {
            CountryWiseSegmentedConfigs configs = CountryWiseSegmentedConfigs.getInstance(countryId);
            String segment = segmentList.stream().filter(i -> configs.getEnabledSegments().contains(i))
                    .findFirst().orElse(null);
            return segment;
        }
        //todo: check for DEFAULT
        return null;
    }
}
