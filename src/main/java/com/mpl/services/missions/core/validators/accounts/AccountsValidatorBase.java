package com.mpl.services.missions.core.validators.accounts;

import com.mpl.services.accounts.*;
import com.mpl.services.missions.utils.ResourceManager;

/**
 * Author: Vishal Pahuja
 */

public class AccountsValidatorBase {

    public boolean isPayer(String requestId, int userId, PaymentMode mode, String countryId) throws Exception {

        AccountsServiceGrpc.AccountsServiceBlockingStub stub =
                ResourceManager.getInstance().getAccountsServiceBlocking(countryId);

        IsPayerRequest request = IsPayerRequest.newBuilder()
                .setUserId(userId)
                .setMode(mode)
                .build();

        IsPayerResponse response = stub.isPayer(request);
        if(response.hasError()){
            throw new Exception(String.format("Failed to fetch KYC details %s %s",
                    response.getError().getReason(), response.getError().getMessage()));
        }
        return response.getIsPayer();

    }


    public boolean isWithdrawer(String requestId, int userId, WithdrawalMode mode, String countryId) throws Exception {

        AccountsServiceGrpc.AccountsServiceBlockingStub stub =
                ResourceManager.getInstance().getAccountsServiceBlocking(countryId);

        IsWithdrawerRequest request = IsWithdrawerRequest.newBuilder()
                .setUserId(userId)
                .setMode(mode)
                .build();

        IsWithdrawerResponse response = stub.isWithdrawer(request);
        if(response.hasError()){
            throw new Exception(String.format("Failed to fetch KYC details %s %s",
                    response.getError().getReason(), response.getError().getMessage()));
        }
        return response.getIsWithdrawer();

    }
}
