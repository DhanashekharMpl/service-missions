package com.mpl.services.missions.impl;

import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.*;
import com.mpl.services.missions.core.DailyMissionCentre;
import com.mpl.services.missions.core.ReactivationCentre;
import com.mpl.services.missions.models.MissionsServiceException;
import com.mpl.services.missions.utils.GeneralUtils;
import io.grpc.stub.StreamObserver;

/**
 * Author: Srijan Singh
 */
public class DailyMissionsService extends DailyMissionsServiceGrpc.DailyMissionsServiceImplBase {

    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(DailyMissionsService.class);

    /************************************** Public Functions **************************************/
    @Override
    public void getDailyMissionsForUser(GetDailyMissionsForUserRequest request, StreamObserver<GetDailyMissionsForUserResponse> responseObserver) {
        String requestId = request.getRequestId();
        if (requestId.isEmpty()) requestId = GeneralUtils.getUniqueId();
        logger.i("RQ INT", requestId, "getDailyMissionsForUser", request.getUserId(), request.getAppType());
        try {
            DailyMissionCentre missionCenter = new DailyMissionCentre();
            GetDailyMissionsForUserResponse response = missionCenter.buildDailyMissionsResponse(requestId, request.getUserId());
            responseObserver.onNext(response);
            logger.i("RQ OKK", requestId, "getDailyMissionsForUser UserId:", request.getUserId());
        } catch (Exception e) {
            logger.e("RQ ERR", requestId, "getDailyMissionsForUser UserId:", request.getUserId(), e);
            DailyMissionsServiceError err = e instanceof MissionsServiceException
                    ? ((MissionsServiceException) e).toDailyMissionGrpcError()
                    : MissionsServiceException.getUnknownErrorx();
            GetDailyMissionsForUserResponse response = GetDailyMissionsForUserResponse.newBuilder()
                    .setError(err).build();
            responseObserver.onNext(response);
        } finally {
            responseObserver.onCompleted();
        }
    }

    @Override
    public void getReactivationReward(ReactivationRewardRequest request, StreamObserver<ReactivationRewardResponse> responseObserver) {
        String requestId = request.getRequestId();
        if (requestId.isEmpty()) requestId = GeneralUtils.getUniqueId();
        logger.i("RQ INT", requestId, "getReactivationRewardForUser", request.getUserId(), request.getAppType());
        try {
            ReactivationCentre reactivationCentre = new ReactivationCentre();
            ReactivationRewardResponse response = reactivationCentre.getReactivationReward(request.getUserId());
            responseObserver.onNext(response);
            logger.i("RQ OKK", requestId, "getReactivationRewardForUser UserId:", request.getUserId());
        } catch (Exception e) {
            logger.e("RQ ERR", requestId, "getReactivationRewardUser UserId:", request.getUserId(), e);
            DailyMissionsServiceError err = e instanceof MissionsServiceException
                    ? ((MissionsServiceException) e).toDailyMissionGrpcError()
                    : MissionsServiceException.getUnknownErrorx();
            ReactivationRewardResponse response = ReactivationRewardResponse.newBuilder()
                    .setError(err)
                    .build();
            responseObserver.onNext(response);
        } finally {
            responseObserver.onCompleted();
        }
    }

    @Override
    public void getAllDailyMissionsForUser(GetDailyMissionsForUserRequest request, StreamObserver<GetDailyMissionsForUserResponse> responseObserver) {
        String requestId = request.getRequestId();
        if (requestId.isEmpty()) requestId = GeneralUtils.getUniqueId();
        logger.i("RQ INT", requestId, "getAllDailyMissionsForUser", request.getUserId(), request.getAppType());
        try {
            DailyMissionCentre missionCenter = new DailyMissionCentre();
            GetDailyMissionsForUserResponse response = missionCenter.buildGetAllDailyMissionsResponse(requestId, request.getUserId());
            responseObserver.onNext(response);
            logger.i("RQ OKK", requestId, "getAllDailyMissionsForUser UserId:", request.getUserId());
        } catch (Exception e) {
            logger.e("RQ ERR", requestId, "getAllDailyMissionsForUser UserId:", request.getUserId(), e);
            DailyMissionsServiceError err = e instanceof MissionsServiceException
                    ? ((MissionsServiceException) e).toDailyMissionGrpcError()
                    : MissionsServiceException.getUnknownErrorx();
            GetDailyMissionsForUserResponse response = GetDailyMissionsForUserResponse.newBuilder()
                    .setError(err).build();
            responseObserver.onNext(response);
        } finally {
            responseObserver.onCompleted();
        }
    }

}