package com.mpl.services.missions.impl;

import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.*;
import com.mpl.services.missions.core.MissionCenter;
import com.mpl.services.missions.db.models.RewardsTableEntry;
import com.mpl.services.missions.models.MissionsServiceException;
import com.mpl.services.missions.utils.GeneralUtils;
import io.grpc.stub.StreamObserver;

/**
 * Author: Kaustubh Bhoyar
 */
public class MissionsService extends MissionsServiceGrpc.MissionsServiceImplBase {
    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(MissionsService.class);

    /************************************** Public Functions **************************************/
    @Override
    public void getMissionsForUser(GetMissionsForUserRequest request, StreamObserver<GetMissionsForUserResponse> responseObserver) {
        String requestId = request.getRequestId();
        if(requestId.isEmpty()) requestId = GeneralUtils.getUniqueId();
        logger.d("RQ INT", requestId, "getMissionsForUser", request.getUserId(), request.getAppType());
        try {
            MissionCenter missionCenter = new MissionCenter();
            GetMissionsForUserResponse response = GetMissionsForUserResponse
                    .newBuilder()
                    .addAllMissions(missionCenter.getActiveMissions(requestId, request.getUserId(), request.getAppType(),request.getAppVersion(), request.getCountryId()))
                    .build();
            responseObserver.onNext(response);
            logger.d("RQ OKK", requestId, "getMissionsForUser");
        } catch (Exception e) {
            logger.d("RQ ERR", requestId, "getMissionsForUser", e);
            MissionsServiceError err = e instanceof MissionsServiceException
                    ? ((MissionsServiceException)e).toGrpcError()
                    : MissionsServiceException.getUnknownError();
            GetMissionsForUserResponse response = GetMissionsForUserResponse.newBuilder()
                    .setError(err).build();
            responseObserver.onNext(response);
        } finally {
            responseObserver.onCompleted();
        }
    }

    @Override
    public void claimMissionRewards(ClaimMissionReqardsRequest request, StreamObserver<ClaimMissionReqardsResponse> responseObserver) {
        String requestId = request.getRequestId();
        if(requestId.isEmpty()) requestId = GeneralUtils.getUniqueId();
        logger.d("RQ INT", requestId, "claimMissionRewards", request.getUserId(), request.getMissionId(), request.getVerificationData());
        try {
            MissionCenter missionCenter = new MissionCenter();
            RewardsTableEntry entry = missionCenter.claimMissionRewards(requestId, request.getMissionId(),
                    request.getUserId(), request.getVerificationData(), request.getCountryId());
            ClaimMissionReqardsResponse response = ClaimMissionReqardsResponse
                    .newBuilder()
                    .setRewardCurrency(entry.getCurrency())
                    .setRewardAmount(entry.getAmount().intValue())
                    .setRewardAmountV2(entry.getAmount().toString())
                    .setCurrency(entry.getCountryCurrency())
                    .build();
            responseObserver.onNext(response);
            logger.d("RQ OKK", requestId, "claimMissionRewards", response.getRewardAmount(), response.getRewardCurrency());
        } catch (Exception e) {
            if(!(e instanceof MissionsServiceException)) {
                logger.d("RQ ERR", requestId, "claimMissionRewards", e);
            } else {
                logger.d("RQ ERR", requestId, "claimMissionRewards", e.getMessage());
            }
            MissionsServiceError err = e instanceof MissionsServiceException
                    ? ((MissionsServiceException)e).toGrpcError()
                    : MissionsServiceException.getUnknownError();
            ClaimMissionReqardsResponse response = ClaimMissionReqardsResponse.newBuilder()
                    .setError(err).build();
            responseObserver.onNext(response);
        } finally {
            responseObserver.onCompleted();
        }
    }
}
