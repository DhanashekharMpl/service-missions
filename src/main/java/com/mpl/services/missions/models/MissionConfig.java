package com.mpl.services.missions.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Author: Kaustubh Bhoyar
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MissionConfig {
    /******************************** Global Variables & Constants ********************************/
    private String id;
    private String name;
    private String ctaLabel;
    private String showOnApp;
    private String rewardCurrency;
    private BigDecimal rewardAmount;
    private String imageUrl;
    private ObjectNode extraData;
    private int reactVersion;
    private Set<String> supportedCountries = new HashSet<>();

    /************************************** Public Functions **************************************/
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCtaLabel() {
        return ctaLabel;
    }

    public void setCtaLabel(String ctaLabel) {
        this.ctaLabel = ctaLabel;
    }

    public String getShowOnApp() {
        return showOnApp;
    }

    public void setShowOnApp(String showOnApp) {
        this.showOnApp = showOnApp;
    }

    public String getRewardCurrency() {
        return rewardCurrency;
    }

    public void setRewardCurrency(String rewardCurrency) {
        this.rewardCurrency = rewardCurrency;
    }

    public BigDecimal getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public ObjectNode getExtraData() {
        return extraData;
    }

    public void setExtraData(ObjectNode extraData) {
        this.extraData = extraData;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getReactVersion() {
        return reactVersion;
    }

    public void setReactVersion(int reactVersion) {
        this.reactVersion = reactVersion;
    }

    public Set<String> getSupportedCountries() {
        return supportedCountries;
    }

    public void setSupportedCountries(Set<String> supportedCountries) {
        this.supportedCountries = supportedCountries;
    }

    @JsonIgnore
    public boolean showOnAppType(String appType) {
        if(showOnApp == null) return false;
        if(showOnApp.equals("ALL")) return true;
        if(showOnApp.equals("ANDROID") && (appType.equals("CASH") || appType.equals("PLAY_STORE"))) return true;
        return showOnApp.equals(appType);
    }
}
