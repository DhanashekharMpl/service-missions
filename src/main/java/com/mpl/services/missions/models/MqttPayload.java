package com.mpl.services.missions.models;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.services.missions.utils.MoneyDTO;

import java.math.BigDecimal;

import static com.mpl.services.missions.utils.MoneyDTO.getMoneyDTOObject;

/**
 * Author: Srijan Singh
 */
public class MqttPayload {
    String type;
    MqttDailyMissionDTO dailyMissionDTO;
    MqttDailyMissionDTO nextDailyMissionDTO;

    public MqttPayload() {
    }

    public MqttPayload(String type, MqttDailyMissionDTO dailyMissionDTO, MqttDailyMissionDTO nextDailyMissionDTO) {
        this.type = type;
        this.dailyMissionDTO = dailyMissionDTO;
        this.nextDailyMissionDTO = nextDailyMissionDTO;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MqttDailyMissionDTO getDailyMissionDTO() {
        return dailyMissionDTO;
    }

    public void setDailyMissionDTO(MqttDailyMissionDTO dailyMissionDTO) {
        this.dailyMissionDTO = dailyMissionDTO;
    }

    public MqttDailyMissionDTO getNextDailyMissionDTO() {
        return nextDailyMissionDTO;
    }

    public void setNextDailyMissionDTO(MqttDailyMissionDTO nextDailyMissionDTO) {
        this.nextDailyMissionDTO = nextDailyMissionDTO;
    }

    public static class MqttDailyMissionDTO {
        String subtaskTitle;
        String bucket;
        Integer rewardAmount;
        String rewardType;
        String imageUrl;
        Integer totalTasks;
        Integer completedTasks;
        String title;
        String subtitle;
        String buttonText;
        String currency;
        String state;
        String type;
        String extraData;
        MoneyDTO rewardAmountV2;


        public MqttDailyMissionDTO() {
        }

        public MqttDailyMissionDTO(String subtaskTitle, String bucket, BigDecimal rewardAmount, String rewardType,
                                   String imageUrl, Integer totalTasks, Integer completedTasks, String countryCode) {
            this.subtaskTitle = subtaskTitle;
            this.bucket = bucket;
            this.rewardAmount = rewardAmount.intValue();
            this.rewardType = rewardType;
            this.imageUrl = imageUrl;
            this.totalTasks = totalTasks;
            this.completedTasks = completedTasks;
            this.rewardAmountV2 = getMoneyDTOObject(rewardAmount.toString(), countryCode);
        }

        public MqttDailyMissionDTO(String subtaskTitle, String bucket, BigDecimal rewardAmount, String rewardType,
                                   String imageUrl, Integer totalTasks, Integer completedTasks, String title,
                                   String subtitle, String buttonText, String state, String type, ObjectNode extraData,
                                   String countryCode) {
            this.subtaskTitle = subtaskTitle;
            this.bucket = bucket;
            this.rewardAmount = rewardAmount.intValue();
            this.rewardType = rewardType;
            this.imageUrl = imageUrl;
            this.totalTasks = totalTasks;
            this.completedTasks = completedTasks;
            this.title = title;
            this.subtitle = subtitle;
            this.buttonText = buttonText;
            this.state = state;
            this.type = type;
            this.extraData = extraData == null ? "" : extraData.toString();
            this.rewardAmountV2 = getMoneyDTOObject(rewardAmount.toString(), countryCode);
        }

        public String getSubtaskTitle() { return subtaskTitle; }

        public void setSubtaskTitle(String subtaskTitle) { this.subtaskTitle = subtaskTitle; }

        public String getBucket() { return bucket; }

        public void setBucket(String bucket) { this.bucket = bucket; }

        public Integer getRewardAmount() { return rewardAmount; }

        public void setRewardAmount(Integer rewardAmount) { this.rewardAmount = rewardAmount; }

        public String getRewardType() { return rewardType; }

        public void setRewardType(String rewardType) { this.rewardType = rewardType; }

        public String getImageUrl() { return imageUrl; }

        public void setImageUrl(String imageUrl) { this.imageUrl = imageUrl; }

        public Integer getTotalTasks() { return totalTasks; }

        public void setTotalTasks(Integer totalTasks) { this.totalTasks = totalTasks; }

        public Integer getCompletedTasks() { return completedTasks; }

        public void setCompletedTasks(Integer completedTasks) { this.completedTasks = completedTasks; }

        public String getTitle() { return title; }

        public void setTitle(String title) { this.title = title; }

        public String getSubtitle() { return subtitle; }

        public void setSubtitle(String subtitle) { this.subtitle = subtitle; }

        public String getButtonText() { return buttonText; }

        public void setButtonText(String buttonText) { this.buttonText = buttonText; }

        public String getCurrency() { return currency; }

        public void setCurrency(String currency) { this.currency = currency; }

        public String getState() { return state; }

        public void setState(String state) { this.state = state; }

        public String getType() { return type; }

        public void setType(String type) { this.type = type; }

        public String getExtraData() { return extraData; }

        public void setExtraData(String extraData) { this.extraData = extraData; }

        public MoneyDTO getRewardAmountV2() { return rewardAmountV2; }

    }
}
