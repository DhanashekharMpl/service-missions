package com.mpl.services.missions.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

/**
 * Author: Srijan Singh
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SegmentedConfigurationsDTO {
    public DailyMissionConfig dailyMissionConfigs;
    private List<String> validApkTypes;
    private List<String> mqttDisabledBuckets;
    private List<String> disabledMissionsBuckets;
}
