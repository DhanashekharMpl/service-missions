package com.mpl.services.missions.models;

import java.io.Serializable;

/**
 * Author: Srijan Singh
 */
public class RedisDataNew implements Serializable {

    Long signupDate;
    Long endDate;
    Bucket games;
    Bucket fantasy;
    Bucket wallet;
    Bucket rummy;

    public Long getSignupDate() {
        return signupDate;
    }

    public void setSignupDate(Long signupDate) {
        this.signupDate = signupDate;
    }

    public Bucket getGames() {
        return games;
    }

    public void setGames(Bucket games) {
        this.games = games;
    }

    public Bucket getFantasy() {
        return fantasy;
    }

    public void setFantasy(Bucket fantasy) {
        this.fantasy = fantasy;
    }

    public Bucket getWallet() {
        return wallet;
    }

    public void setWallet(Bucket wallet) {
        this.wallet = wallet;
    }

    public Bucket getRummy() { return rummy; }

    public void setRummy(Bucket rummy) { this.rummy = rummy; }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public static class Bucket implements Serializable {
        String missionId;
        Integer lastIndex;
        String status;
        Long start;
        Long end;
        Integer eventCount;
        Integer eventThreshold;

        public Integer getEventThreshold() {
            return eventThreshold != null ? eventThreshold : 3;
        }

        public void setEventThreshold(Integer eventThreshold) {
            this.eventThreshold = eventThreshold;
        }

        public Integer getEventCount() {
            return eventCount;
        }

        public void setEventCount(Integer eventCount) {
            this.eventCount = eventCount;
        }

        public String getMissionId() {
            return missionId;
        }

        public void setMissionId(String missionId) {
            this.missionId = missionId;
        }

        public Integer getLastIndex() {
            return lastIndex;
        }

        public void setLastIndex(Integer lastIndex) {
            this.lastIndex = lastIndex;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Long getStart() {
            return start;
        }

        public void setStart(Long start) {
            this.start = start;
        }

        public Long getEnd() {
            return end;
        }

        public void setEnd(Long end) {
            this.end = end;
        }
    }
}
