package com.mpl.services.missions.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.math.BigDecimal;

/**
 * Author: Srijan Singh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReactivationRewards {
    Integer minAgeInMins;
    Integer maxAgeInMins;
    String segmentId;
    String rewardType;
    String ticketType;
    BigDecimal rewardAmount;
    String rewardCurrency;
    Integer expiryTimeInMins;
    CouponData couponData;
    Boolean enabled;
    private ObjectNode extraData;

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    public Integer getMinAgeInMins() {
        return minAgeInMins;
    }

    public void setMinAgeInMins(Integer minAgeInMins) {
        this.minAgeInMins = minAgeInMins;
    }

    public Integer getMaxAgeInMins() {
        return maxAgeInMins;
    }

    public void setMaxAgeInMins(Integer maxAgeInMins) {
        this.maxAgeInMins = maxAgeInMins;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public BigDecimal getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public String getRewardCurrency() {
        return rewardCurrency;
    }

    public void setRewardCurrency(String rewardCurrency) {
        this.rewardCurrency = rewardCurrency;
    }

    public Integer getExpiryTimeInMins() {
        return expiryTimeInMins;
    }

    public void setExpiryTimeInMins(Integer expiryTimeInMins) {
        this.expiryTimeInMins = expiryTimeInMins;
    }

    public CouponData getCouponData() {
        return couponData;
    }

    public void setCouponData(CouponData couponData) {
        this.couponData = couponData;
    }

    public ObjectNode getExtraData() {
        return extraData;
    }

    public void setExtraData(ObjectNode extraData) {
        this.extraData = extraData;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public static class CouponData {
        BigDecimal cashbackPercentage;
        Integer maximumCashback;
        Integer validForMinutes;
        BigDecimal minAmount;
        String toBalance;
        String segmentGroup;

        public BigDecimal getCashbackPercentage() {
            return cashbackPercentage;
        }

        public void setCashbackPercentage(BigDecimal cashbackPercentage) {
            this.cashbackPercentage = cashbackPercentage;
        }

        public Integer getMaximumCashback() {
            return maximumCashback;
        }

        public void setMaximumCashback(Integer maximumCashback) {
            this.maximumCashback = maximumCashback;
        }

        public Integer getValidForMinutes() {
            return validForMinutes;
        }

        public void setValidForMinutes(Integer validForMinutes) {
            this.validForMinutes = validForMinutes;
        }

        public BigDecimal getMinAmount() {
            return minAmount;
        }

        public void setMinAmount(BigDecimal minAmount) {
            this.minAmount = minAmount;
        }

        public String getToBalance() {
            return toBalance;
        }

        public void setToBalance(String toBalance) {
            this.toBalance = toBalance;
        }

        public String getSegmentGroup() {
            return segmentGroup;
        }

        public void setSegmentGroup(String segmentGroup) {
            this.segmentGroup = segmentGroup;
        }
    }
}
