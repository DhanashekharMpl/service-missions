package com.mpl.services.missions.models;

import com.mpl.services.missions.DailyMissionsServiceError;
import com.mpl.services.missions.MissionsServiceError;

/**
 * Author: Kaustubh Bhoyar
 */
public class MissionsServiceException extends Exception {
    /******************************** Global Variables & Constants ********************************/
    public enum Reason {
        NONE,
        UNKNOWN,
        BAD_REQUEST,
        EXTERNAL_ERROR,
        INTERNAL_ERROR,
        VALIDATION_FAILED,
        ALREADY_CLAIMED
    }

    private Reason reason;

    /**************************************** Constructors ****************************************/
    public MissionsServiceException(Reason reason) {
        this(reason, reason.toString());
    }

    public MissionsServiceException(Reason reason, String message) {
        super(message);
        this.reason = reason;
    }

    public static MissionsServiceError getUnknownError() {
        return MissionsServiceError.newBuilder().setReason(MissionsServiceError.Reason.UNKNOWN).
                setMessage("Unexpected exception in Missions Service").build();
    }

    public static DailyMissionsServiceError getUnknownErrorx() {
        return DailyMissionsServiceError.newBuilder().setReason(DailyMissionsServiceError.Reason.UNKNOWN).
                setMessage("Unexpected exception in Missions Service").build();
    }

    /************************************** Public Functions **************************************/
    public Reason getReason() {
        return reason;
    }

    public MissionsServiceError toGrpcError() {
        return MissionsServiceError.newBuilder().setReason(getLSEReason()).
                setMessage(getMessage()).build();
    }

    public DailyMissionsServiceError toDailyMissionGrpcError() {
        return DailyMissionsServiceError.newBuilder().setReason(getLSEReasonForDailyMission()).
                setMessage(getMessage()).build();
    }

    private MissionsServiceError.Reason getLSEReason() {
        return MissionsServiceError.Reason.valueOf(reason.name());
    }

    private DailyMissionsServiceError.Reason getLSEReasonForDailyMission() {
        return DailyMissionsServiceError.Reason.valueOf(reason.name());
    }
}