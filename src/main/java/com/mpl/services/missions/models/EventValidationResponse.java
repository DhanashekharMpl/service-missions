package com.mpl.services.missions.models;

import com.mpl.services.missions.utils.Constants.bucketEnum;

/**
 * Author: Srijan Singh
 */
public class EventValidationResponse {
    Integer userId;
    bucketEnum bucketEnum;
    Integer incrementValue;
    String missionId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public bucketEnum getBucketEnum() {
        return bucketEnum;
    }

    public void setBucketEnum(bucketEnum bucketEnum) {
        this.bucketEnum = bucketEnum;
    }

    public Integer getIncrementValue() {
        return incrementValue;
    }

    public void setIncrementValue(Integer incrementValue) {
        this.incrementValue = incrementValue;
    }

    public String getMissionId() {
        return missionId;
    }

    public void setMissionId(String missionId) {
        this.missionId = missionId;
    }
}
