package com.mpl.services.missions.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.math.BigDecimal;
import java.util.List;

/**
 * Author: Srijan Singh
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DailyMissionConfig {
    String header;
    Integer days;
    Integer timeDiff;

    private List<ZkBucket> games;
    private List<ZkBucket> fantasy;
    private List<ZkBucket> wallet;
    private List<ZkBucket> rummy;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public List<ZkBucket> getGames() { return games; }

    public void setGames(List<ZkBucket> games) {
        this.games = games;
    }

    public List<ZkBucket> getFantasy() {
        return fantasy;
    }

    public void setFantasy(List<ZkBucket> fantasy) {
        this.fantasy = fantasy;
    }

    public List<ZkBucket> getWallet() { return wallet; }

    public void setWallet(List<ZkBucket> wallet) {
        this.wallet = wallet;
    }

    public List<ZkBucket> getRummy() { return rummy; }

    public void setRummy(List<ZkBucket> rummy) { this.rummy = rummy; }

    public int getTimeDiff() {
        return timeDiff;
    }

    public void setTimeDiff(int timeDiff) {
        this.timeDiff = timeDiff;
    }

    public static class ZkBucket {
        private String type;
        private String missionId;
        private String title;
        private String subTitle;
        private String subtaskTitle;
        private String buttonText;
        private String rewardCurrency;
        private BigDecimal rewardAmount;
        private String imageUrl;
        private Integer priority;
        private ObjectNode extraData;
        private Boolean enabled;
        private Integer eventCount;
        private BigDecimal secondaryEventCount;
        private List<String> includedGameIds;
        private List<String> excludedGameIds;

        public Integer getEventCount() {
            return eventCount;
        }

        public void setEventCount(Integer eventCount) {
            this.eventCount = eventCount;
        }

        public String getButtonText() {
            return buttonText;
        }

        public void setButtonText(String buttonText) {
            this.buttonText = buttonText;
        }

        public String getMissionId() {
            return missionId;
        }

        public void setMissionId(String missionId) {
            this.missionId = missionId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubTitle() {
            return subTitle;
        }

        public void setSubTitle(String subTitle) {
            this.subTitle = subTitle;
        }

        public String getRewardCurrency() {
            return rewardCurrency;
        }

        public void setRewardCurrency(String rewardCurrency) {
            this.rewardCurrency = rewardCurrency;
        }

        public BigDecimal getRewardAmount() {
            return rewardAmount;
        }

        public void setRewardAmount(BigDecimal rewardAmount) {
            this.rewardAmount = rewardAmount;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public int getPriority() {
            return priority;
        }

        public void setPriority(Integer priority) {
            this.priority = priority;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        public ObjectNode getExtraData() {
            return extraData;
        }

        public void setExtraData(ObjectNode extraData) {
            this.extraData = extraData;
        }

        public Boolean getEnabled() {
            return enabled;
        }

        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getSubtaskTitle() { return subtaskTitle; }

        public void setSubtaskTitle(String subtaskTitle) { this.subtaskTitle = subtaskTitle; }

        public BigDecimal getSecondaryEventCount() { return secondaryEventCount; }

        public List<String> getIncludedGameIds() { return includedGameIds; }

        public List<String> getExcludedGameIds() { return excludedGameIds; }

    }
}
