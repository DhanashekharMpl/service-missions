package com.mpl.services.missions.db;

import com.mpl.services.missions.db.models.ReactivationRewardsTableEntry;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;

/**
 * Author: Srijan Singh
 */
public class ReactivationRewardsTable extends TableBase {

    private static final String QUERY_INSERT = "INSERT INTO reactivation_rewards " +
            "(user_id, user_age_range, reward_type, amount, currency, ticket_type, coupon_code, txn_state, expiry_time ," +
            "country_currency)" +
            "VALUES (?, ?, ?, ?, ?, ?, ?, 'PENDING',?,?) " +
            "RETURNING *;";
    private static final String QUERY_SET_TRANSACTION_ID_AND_STATE = "UPDATE reactivation_rewards " +
            "SET coupon_code = ?, txn_id = ?, txn_state = ? " +
            "WHERE id = ?;";
    /************************************** Public Functions **************************************/


    /**************************************** Constructors ****************************************/
    public ReactivationRewardsTable(DataSource dataSource) { super(dataSource); }

    public ReactivationRewardsTableEntry insert(int userId, String userAgeRange, String rewardType, BigDecimal amount,
                                                String rewardCurrency, String ticketType, String couponCode, long expiryTime,
                                                String currency) throws SQLException {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = super.getReadWriteConnection();
            ps = c.prepareStatement(QUERY_INSERT, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, userId);
            ps.setString(2, userAgeRange);
            ps.setString(3, rewardType);
            ps.setBigDecimal(4, amount);
            ps.setString(5, rewardCurrency);
            ps.setString(6, ticketType);
            ps.setString(7, couponCode);
            ps.setLong(8, expiryTime);
            ps.setString(9,currency );
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            return rs.next() ? new ReactivationRewardsTableEntry(rs) : null;
        } finally {
            if (rs != null) try { rs.close(); } catch (Exception ignored) { }
            if (ps != null) try { ps.close(); } catch (Exception ignored) { }
            if (c != null) try { c.close(); } catch (Exception ignored) { }
        }
    }

    public void setTransactionIdAndState(int entryId, String txnId, String txnState, String couponCode) throws SQLException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = super.getReadWriteConnection();
            ps = c.prepareStatement(QUERY_SET_TRANSACTION_ID_AND_STATE);
            ps.setString(1, couponCode);
            ps.setString(2, txnId);
            ps.setString(3, txnState);
            ps.setInt(4, entryId);
            ps.executeUpdate();
        } finally {
            if (ps != null) try { ps.close(); } catch (Exception ignored) { }
            if (c != null) try { c.close(); } catch (Exception ignored) { }
        }
    }
}
