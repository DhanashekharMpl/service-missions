package com.mpl.services.missions.db;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Author: Kaustubh Bhoyar
 */
public class TableBase {
    /******************************** Global Variables & Constants ********************************/
    private DataSource dataSource;

    /**************************************** Constructors ****************************************/
    public TableBase(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /************************************** Public Functions **************************************/
    /************************************ Protected Functions *************************************/
    protected Connection getReadOnlyConnection() throws SQLException {
        Connection c = dataSource.getConnection();
        c.setReadOnly(true);
        return c;
    }
    protected Connection getReadWriteConnection() throws SQLException {
        Connection c = dataSource.getConnection();
        c.setReadOnly(false);
        return c;
    }

    /************************************* Core Private Logic *************************************/
    /************************************** Private Classes ***************************************/
}
