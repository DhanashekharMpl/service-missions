package com.mpl.services.missions.db.models;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Author: Srijan Singh
 */
public class ReactivationRewardsTableEntry {

    private final int id;
    private final int userId;
    private final String userAgeRange;
    private final String rewardType;
    private final BigDecimal amount;
    private final String currency;
    private final String TicketType;
    private final String couponCode;
    private final String transactionId;
    private final String transactionState;
    private final long expiryTime;
    private final long createdOn;
    private final long modifiedOn;
    private final String countryCurrency;

    /**************************************** Constructors ****************************************/
    public ReactivationRewardsTableEntry(ResultSet rs) throws SQLException {
        id = rs.getInt("id");
        userId = rs.getInt("user_id");
        userAgeRange = rs.getString("user_age_range");
        rewardType = rs.getString("reward_type");
        amount = rs.getBigDecimal("amount");
        currency = rs.getString("currency");
        TicketType = rs.getString("ticket_type");
        couponCode = rs.getString("coupon_code");
        transactionId = rs.getString("txn_id");
        transactionState = rs.getString("txn_state");
        expiryTime = rs.getLong("expiry_time");
        createdOn = rs.getTimestamp("created_on").getTime();
        modifiedOn = rs.getTimestamp("modified_on").getTime();
        countryCurrency = rs.getString("country_currency");
    }

    /************************************** Public Functions **************************************/
    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getTransactionState() {
        return transactionState;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public long getModifiedOn() {
        return modifiedOn;
    }

    public String getUserAgeRange() { return userAgeRange; }

    public String getRewardType() { return rewardType; }

    public String getTicketType() { return TicketType; }

    public String getCouponCode() { return couponCode; }

    public long getExpiryTime() { return expiryTime; }

    public String getCountryCurrency() { return countryCurrency; }
}
