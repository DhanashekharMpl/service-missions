package com.mpl.services.missions.db;

import com.mpl.services.missions.db.models.DailyRewardsTableEntry;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;

/**
 * Author: Srijan Singh
 */
public class DailyRewardsTable extends TableBase {
    private static final String QUERY_INSERT = "INSERT INTO daily_rewards " +
            "(mission_id, user_id, currency, amount, txn_state ,country_currency) " +
            "VALUES (?, ?, ?, ?, 'PENDING',?) " +
            "RETURNING *;";
    private static final String QUERY_SELECT_COMPLETED_MISSIONS = "SELECT mission_id FROM daily_rewards " +
            "WHERE user_id = ? AND mission_id = ANY(?);";
    private static final String QUERY_SET_TRANSACTION_ID_AND_STATE = "UPDATE daily_rewards " +
            "SET txn_id = ?, txn_state = ? " +
            "WHERE id = ?;";
    private static final String QUERY_GET_ALL_REWARDS = "SELECT SUM(amount) FROM daily_rewards " +
            "WHERE user_id = ?";

    /**************************************** Constructors ****************************************/
    public DailyRewardsTable(DataSource dataSource) {
        super(dataSource);
    }

    /************************************** Public Functions **************************************/

    public DailyRewardsTableEntry insert(String missionId, int userId, String currency, BigDecimal amount, String intCurrency) throws SQLException {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = super.getReadWriteConnection();
            ps = c.prepareStatement(QUERY_INSERT, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, missionId);
            ps.setInt(2, userId);
            ps.setString(3, currency);
            ps.setBigDecimal(4, amount);
            ps.setString(5, intCurrency);
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            return rs.next() ? new DailyRewardsTableEntry(rs) : null;
        } finally {
            if (rs != null) try { rs.close(); } catch (Exception ignored) { }
            if (ps != null) try { ps.close(); } catch (Exception ignored) { }
            if (c != null) try { c.close(); } catch (Exception ignored) { }
        }
    }

    public BigDecimal getAllRewardsForUser(int userId) throws SQLException {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = super.getReadOnlyConnection();
            ps = c.prepareStatement(QUERY_GET_ALL_REWARDS);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            return rs.next() ? rs.getBigDecimal(1) : null;
        } finally {
            if (rs != null) try { rs.close(); } catch (Exception ignored) { }
            if (ps != null) try { ps.close(); } catch (Exception ignored) { }
            if (c != null) try { c.close(); } catch (Exception ignored) { }
        }
    }

    public void setTransactionIdAndState(int entryId, String txnId, String txnState) throws SQLException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = super.getReadWriteConnection();
            ps = c.prepareStatement(QUERY_SET_TRANSACTION_ID_AND_STATE);
            ps.setString(1, txnId);
            ps.setString(2, txnState);
            ps.setInt(3, entryId);
            ps.executeUpdate();
        } finally {
            if (ps != null) try { ps.close(); } catch (Exception ignored) { }
            if (c != null) try { c.close(); } catch (Exception ignored) { }
        }
    }
}
