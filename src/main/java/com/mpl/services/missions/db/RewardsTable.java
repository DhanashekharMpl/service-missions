package com.mpl.services.missions.db;

import com.mpl.services.missions.db.models.RewardsTableEntry;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Author: Kaustubh Bhoyar
 */
public class RewardsTable extends TableBase {
    /******************************** Global Variables & Constants ********************************/
    private static final String QUERY_INSERT = "INSERT INTO rewards " +
            "(mission_id, user_id, currency, amount, txn_state , country_currency) " +
            "VALUES (?, ?, ?, ?, 'PENDING',?) " +
            "RETURNING *;";
    private static final String QUERY_SELECT_COMPLETED_MISSIONS = "SELECT mission_id FROM rewards " +
            "WHERE user_id = ? AND mission_id = ANY(?);";
    private static final String QUERY_SET_TRANSACTION_ID_AND_STATE = "UPDATE rewards " +
            "SET txn_id = ?, txn_state = ? " +
            "WHERE id = ?;";


    /**************************************** Constructors ****************************************/
    public RewardsTable(DataSource dataSource) {
        super(dataSource);
    }

    /************************************** Public Functions **************************************/
    public RewardsTableEntry insert(String missionId, int userId, String currency, BigDecimal amount , String countryCurrency) throws SQLException {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = super.getReadWriteConnection();
            ps = c.prepareStatement(QUERY_INSERT, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, missionId);
            ps.setInt(2, userId);
            ps.setString(3, currency);
            ps.setBigDecimal(4, amount);
            ps.setString(5,countryCurrency);
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            return rs.next() ? new RewardsTableEntry(rs) : null;
        } finally {
            if(rs != null) try { rs.close(); } catch (Exception ignored) { }
            if(ps != null) try { ps.close(); } catch (Exception ignored) { }
            if(c != null) try { c.close(); } catch (Exception ignored) { }
        }
    }

    public Set<String> selectCompletedMissions(int userId, String[] filter) throws SQLException {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            c = super.getReadOnlyConnection();
            ps = c.prepareStatement(QUERY_SELECT_COMPLETED_MISSIONS);
            ps.setInt(1, userId);
            ps.setArray(2, c.createArrayOf("text", filter));
            rs = ps.executeQuery();
            Set<String> result = new HashSet<>();
            while (rs.next()) result.add(rs.getString(1));
            return result;
        } finally {
            if(rs != null) try { rs.close(); } catch (Exception ignored) { }
            if(ps != null) try { ps.close(); } catch (Exception ignored) { }
            if(c != null) try { c.close(); } catch (Exception ignored) { }
        }
    }

    public void setTransactionIdAndState(int entryId, String txnId, String txnState) throws SQLException {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = super.getReadWriteConnection();
            ps = c.prepareStatement(QUERY_SET_TRANSACTION_ID_AND_STATE);
            ps.setString(1, txnId);
            ps.setString(2, txnState);
            ps.setInt(3, entryId);
            ps.executeUpdate();
        } finally {
            if(ps != null) try { ps.close(); } catch (Exception ignored) { }
            if(c != null) try { c.close(); } catch (Exception ignored) { }
        }
    }

    public void template() throws SQLException {
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {

        } finally {
            if(rs != null) try { rs.close(); } catch (Exception ignored) { }
            if(ps != null) try { ps.close(); } catch (Exception ignored) { }
            if(c != null) try { c.close(); } catch (Exception ignored) { }
        }
    }

    /************************************* Core Private Logic *************************************/
    /************************************** Private Classes ***************************************/
}
