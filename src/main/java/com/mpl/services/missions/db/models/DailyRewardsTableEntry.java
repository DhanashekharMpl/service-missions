package com.mpl.services.missions.db.models;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Author: Srijan Singh
 */
public class DailyRewardsTableEntry {

    private final BigDecimal amount;
    private final String transactionId;
    private final String transactionState;
    private final long createdOn;
    private final long modifiedOn;
    private final int id;
    private final int userId;
    private final String missionId;
    private final String currency;
    private final String countryCurrency;

    /**************************************** Constructors ****************************************/
    public DailyRewardsTableEntry(ResultSet rs) throws SQLException {
        id = rs.getInt("id");
        userId = rs.getInt("user_id");
        missionId = rs.getString("mission_id");
        currency = rs.getString("currency");
        amount = rs.getBigDecimal("amount");
        transactionId = rs.getString("txn_id");
        transactionState = rs.getString("txn_state");
        createdOn = rs.getTimestamp("created_on").getTime();
        modifiedOn = rs.getTimestamp("modified_on").getTime();
        countryCurrency = rs.getString("country_currency");
    }

    /************************************** Public Functions **************************************/
    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public String getMissionId() {
        return missionId;
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getTransactionState() {
        return transactionState;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public long getModifiedOn() {
        return modifiedOn;
    }

    public String getCountryCurrency() { return countryCurrency; }
}
