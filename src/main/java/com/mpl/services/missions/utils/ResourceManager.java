package com.mpl.services.missions.utils;

import com.mpl.commons.libpf.Pathfinder;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.accounts.AccountsServiceGrpc;
import com.mpl.services.grpc.battlechallenges.BattleChallengeServiceGrpc;
import com.mpl.services.kyc.KycServiceGrpc;
import com.mpl.services.leaderboards.grpc.LeaderBoardServiceGrpc;
import com.mpl.services.mqtt.MqttServiceGrpc;
import com.mpl.services.offers.OffersServiceGrpc;
import com.mpl.services.offers.OffersServiceV2Grpc;
import com.mpl.services.segmentationengine.grpc.SegmentEngineServiceGrpc;
import com.mpl.services.superteam.SuperTeamServiceGrpc;
import com.mpl.services.uge.UserGeneratedServiceGrpc;
import com.mpl.services.relation.grpc.UserRelationServiceGrpc;
import com.mpl.services.userdata.grpc.UserDataServiceGrpc;
import com.mpl.services.video.VideoServiceGrpc;
import io.grpc.ManagedChannel;
import org.apache.commons.dbcp2.BasicDataSource;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import javax.sql.DataSource;
import java.util.concurrent.TimeUnit;

/**
 * Author: Kaustubh Bhoyar
 */
public class ResourceManager {
    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(ResourceManager.class);
    private static final Object LOCK = new Object();
    private static final String SERVICE_ACCOUNTS = "service-accounts";
    private static final String SERVICE_USER_PROFILE= "service-user-profile";
    private static final String SERVICE_KYC = "service-kyc";
    private static final String SERVICE_VIDEOS = "service-videos";
    private static final String SERVICE_LEADERBOARD = "service-global-leaderboards";
    public static final String SERVICE_USER_DATA = "service-user-data";
    private static final String SERVICE_SUPERTEAM_CRICKET = "service-superteam-cricket";
    private static final String SERVICE_UGX = "service-user-generated";
    private static final String SERVICE_USER_RELATIONS = "service-user-relation";
    private static final String SERVICE_BATTLES = "service-tournament-internal";
    private static final String SERVICE_MQTT = "service-mqtt";
    private static final String SERVICE_OFFERS = "service-offers";
    private static final String SERVICE_SEGMENTATION = "service-segmentation-engine";

    private static ResourceManager mInstance;

    private final BasicDataSource pgConnectionPool;

    private final RedissonClient redisson;

    /**************************************** Constructors ****************************************/
    private ResourceManager() throws Exception{
        Configurations config = Configurations.getInstance();
        logger.d("Initializing DB Connection Pool");
        pgConnectionPool = new BasicDataSource();
        pgConnectionPool.setUrl(config.getDbConnectionString());
        pgConnectionPool.setUsername(config.getDbUsername());
        pgConnectionPool.setPassword(config.getDbPassword());
        logger.d("DB Connection Pool initialized");
        logger.i("Initializing Redisson");
        Config rc = Config.fromJSON(config.getRedissonConfig().toString());
        redisson = Redisson.create(rc);
        logger.i("Redisson Initialized");
    }

    public static void initialize() throws Exception{
        if(mInstance != null) {
            logger.w("Already initialized");
            return;
        }
        synchronized (LOCK) {
            logger.d("Initializing");
            mInstance = new ResourceManager();
            logger.d("Initialized");
        }
    }

    public static ResourceManager getInstance() {
        return mInstance;
    }

    /************************************** Public Functions **************************************/
    public DataSource getPgConnectionPool() {
        return pgConnectionPool;
    }

    public AccountsServiceGrpc.AccountsServiceBlockingStub getAccountsServiceBlocking(String countryCode) {
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_ACCOUNTS, countryCode);
        return AccountsServiceGrpc.newBlockingStub(channel)
                .withDeadlineAfter(5, TimeUnit.SECONDS);
    }

    public KycServiceGrpc.KycServiceBlockingStub getKYCServiceBlocking() {
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_KYC);
        return KycServiceGrpc.newBlockingStub(channel)
                .withDeadlineAfter(5, TimeUnit.SECONDS);
    }
    public VideoServiceGrpc.VideoServiceBlockingStub getVideoServiceBlocking() {
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_VIDEOS);
        return VideoServiceGrpc.newBlockingStub(channel)
                .withDeadlineAfter(5, TimeUnit.SECONDS);
    }
    public UserDataServiceGrpc.UserDataServiceBlockingStub getUserDataServiceBlocking() {
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_USER_DATA);
        return UserDataServiceGrpc.newBlockingStub(channel)
                .withDeadlineAfter(5, TimeUnit.SECONDS);
    }

    public LeaderBoardServiceGrpc.LeaderBoardServiceBlockingStub getLeaderBoardServiceBlocking() {
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_LEADERBOARD);
        return LeaderBoardServiceGrpc.newBlockingStub(channel)
                                     .withDeadlineAfter(10, TimeUnit.SECONDS);
    }

    public SuperTeamServiceGrpc.SuperTeamServiceBlockingStub getSuperteamCricketServiceBlockingStub() {
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_SUPERTEAM_CRICKET);
        return SuperTeamServiceGrpc.newBlockingStub(channel).withDeadlineAfter(10, TimeUnit.SECONDS);
    }

    public UserRelationServiceGrpc.UserRelationServiceBlockingStub getUserRelationsV2ServiceBlockingStub(){
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_USER_RELATIONS);
        return UserRelationServiceGrpc.newBlockingStub(channel).withDeadlineAfter(5,TimeUnit.SECONDS);
    }

    public BattleChallengeServiceGrpc.BattleChallengeServiceBlockingStub getBattleChallengeServiceBlockingStub(){
        ManagedChannel channel= Pathfinder.getInstance().getChannel(SERVICE_BATTLES);
        return BattleChallengeServiceGrpc.newBlockingStub(channel).withDeadlineAfter(5,TimeUnit.SECONDS);
    }

    public UserGeneratedServiceGrpc.UserGeneratedServiceBlockingStub getUserGeneratedServiceBlockingStub(){
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_UGX);
        return UserGeneratedServiceGrpc.newBlockingStub(channel).withDeadlineAfter(5,TimeUnit.SECONDS);
    }

    public MqttServiceGrpc.MqttServiceBlockingStub getMqttServiceBlockingStub(){
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_MQTT);
        return MqttServiceGrpc.newBlockingStub(channel).withDeadlineAfter(5, TimeUnit.SECONDS);
    }

    public OffersServiceGrpc.OffersServiceBlockingStub getOffersServiceBlockingStub() {
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_OFFERS);
        return OffersServiceGrpc.newBlockingStub(channel).withDeadlineAfter(10, TimeUnit.SECONDS);
    }

    public OffersServiceV2Grpc.OffersServiceV2BlockingStub getOffersServiceV2BlockingStub() {
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_OFFERS);
        return OffersServiceV2Grpc.newBlockingStub(channel).withDeadlineAfter(10, TimeUnit.SECONDS);
    }

    public SegmentEngineServiceGrpc.SegmentEngineServiceBlockingStub getSegmentationServiceBlockingStub() {
        ManagedChannel channel = Pathfinder.getInstance().getChannel(SERVICE_SEGMENTATION);
        return SegmentEngineServiceGrpc.newBlockingStub(channel).withDeadlineAfter(10, TimeUnit.SECONDS);
    }

    public RedissonClient getRedisson() {
        return redisson;
    }
}
