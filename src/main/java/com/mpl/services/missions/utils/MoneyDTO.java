package com.mpl.services.missions.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mpl.commons.lci.utils.CountryInfoUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Author: Srijan Singh
 */
@Data
@AllArgsConstructor
@Setter
@NoArgsConstructor
public class MoneyDTO {
    @JsonProperty("amount")
    String value = "";
    String currencyId = "";

    public static MoneyDTO getMoneyDTOObject(String value, String countryCode) {
        String currencyId = CountryInfoUtil.getCountryInfoProto(countryCode).getCurrency().getCurrencyId();
        return new MoneyDTO(value, currencyId);
    }
}
