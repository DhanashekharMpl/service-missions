package com.mpl.services.missions.utils;

import com.mpl.commons.logging.MLogger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;

/**
 * Author: Srijan Singh
 */
public class DateTimeUtils {

    private static final MLogger logger = new MLogger(DateTimeUtils.class);

    public static DateTime addMinutes(Long dt, Integer val) {
        val = val == null ? 1440 : val;
        DateTime dateTime = new DateTime(dt);
        return dateTime.plusMinutes(val);
    }

    public static DateTime minusMinutes(Long dt, Integer val) {
        DateTime dateTime = new DateTime(dt);
        return dateTime.minusMinutes(val);
    }

    public static Long getEpochTime(DateTime dateTime) {
        return dateTime.getMillis();
    }

    public static DateTime roundOffTime() {
        DateTimeZone zone = DateTimeZone.forID("Asia/Kolkata");
        DateTime dateTime = new DateTime(zone);
        return dateTime.hourOfDay().roundCeilingCopy();
    }

    public static Integer getDaysElapsed(Long oldTime) {
        try {
            DateTime dateTime = new DateTime(oldTime);
            Duration duration = new Duration(dateTime, new DateTime());
            return Math.toIntExact(duration.getStandardDays());
        } catch (Exception e) {
            logger.e("Error in getDaysElapsed", e);
            return 0;
        }
    }

    public static Long getEndTime(Long now, Long start, Long end, String countryCode) {
        int timeDiff = CountryWiseConfig.getInstance(countryCode).getDailyMissionConfigs().getTimeDiff();
        if (timeDiff == 0)
            return start;
        while (now > end) {
            end = (DateTimeUtils.getEpochTime(DateTimeUtils.addMinutes(start, timeDiff)));
            start = end;
        }
        return end;
    }

}