package com.mpl.services.missions.utils;

/**
 * Author: Srijan Singh
 */
public class Constants {

    public enum bucketEnum {GAMES, FANTASY, WALLET , RUMMY}

    public enum missionStatusEnum {ACTIVE, LOCKED, COMPLETED}

    public enum mqttType {MISSION_SUB_TASK_COMPLETED, MISSION_COMPLETED}
}
