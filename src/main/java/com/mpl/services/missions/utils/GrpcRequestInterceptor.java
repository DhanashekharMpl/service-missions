package com.mpl.services.missions.utils;

import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.models.MissionsServiceException;
import io.grpc.*;
import io.grpc.internal.GrpcUtil;

import java.net.SocketAddress;
import java.util.UUID;

/**
 * Author: Kaustubh Bhoyar
 */
public class GrpcRequestInterceptor implements ServerInterceptor {
    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(GrpcRequestInterceptor.class);

    /************************************** Public Functions **************************************/
    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(
            final ServerCall<ReqT, RespT> serverCall,
            Metadata metadata,
            ServerCallHandler<ReqT, RespT> serverCallHandler)
    {
        final String internalRequestId = getUid();
        final String methodName = getPrettyMethodName(serverCall);
        final SocketAddress origin = serverCall.getAttributes().get(Grpc.TRANSPORT_ATTR_REMOTE_ADDR);
        final String userAgent = metadata.get(GrpcUtil.USER_AGENT_KEY);
        logger.d("INIT", internalRequestId, methodName, origin);

        ServerCall.Listener<ReqT> wrapperCall = serverCallHandler.startCall(new ForwardingServerCall.SimpleForwardingServerCall<ReqT, RespT>(serverCall) {
            @Override
            public void sendMessage(RespT message) {
                super.sendMessage(message);
            }
        }, metadata);

        return new ForwardingServerCallListener.SimpleForwardingServerCallListener<ReqT>(wrapperCall) {
            long startTime;
            boolean isEndLogged;
            ReqT requestMessage;
            String originRequestId;

            @Override
            public void onMessage(ReqT message) {
                startTime = System.currentTimeMillis();
                originRequestId = extractOriginRequestId(message);
                requestMessage = message;
                super.onMessage(message);
            }

            @Override
            public void onHalfClose() {
                try {
                    super.onHalfClose();
                } catch (Exception e) {
                    int code;
                    logger.e("ERROR", internalRequestId, originRequestId, methodName, requestMessage, e);
                    if(e instanceof MissionsServiceException) {
                        code = 400;
                    } else {
                        code = 500;
                        e = new MissionsServiceException(
                                MissionsServiceException.Reason.UNKNOWN,
                                "Internal Server Error"
                        );
                    }
                    isEndLogged = true;
                    serverCall.close(Status.INTERNAL.withDescription(e.toString()), new Metadata());
                    logger.i("COMPLETE", internalRequestId, originRequestId, methodName, code,
                            (System.currentTimeMillis() - startTime), origin, userAgent);
                }
            }

            @Override
            public void onComplete() {
                if(!isEndLogged) {
                    logger.i("COMPLETE", internalRequestId, originRequestId, methodName, 200,
                            (System.currentTimeMillis() - startTime), origin, userAgent);
                }
                super.onComplete();
            }

            @Override
            public void onCancel() {
                isEndLogged = true;
                logger.i("COMPLETE", internalRequestId, originRequestId, methodName, 500,
                        (System.currentTimeMillis() - startTime), origin, userAgent);
                super.onCancel();
            }
        };
    }

    /************************************* Core Private Logic *************************************/
    private String getUid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    private String getPrettyMethodName(ServerCall call) {
        String mn = call.getMethodDescriptor().getFullMethodName();
        return mn.substring(mn.lastIndexOf("/") + 1);
    }

    private String extractOriginRequestId(Object o) {
        try {
            Class<?> cls = o.getClass();
            String reqId = (String) cls.getMethod("getRequestId").invoke(o);
            if(!reqId.isEmpty()) return reqId;
        } catch (Exception ignored) { }
        return "UNKNOWN";
    }
}
