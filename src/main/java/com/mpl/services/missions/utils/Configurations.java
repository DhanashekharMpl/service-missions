package com.mpl.services.missions.utils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.configs.ConfigProvider;
import com.mpl.commons.configs.ConfigurationsBase;
import com.mpl.commons.configs.RegistrationConfig;
import com.mpl.commons.logging.MLogger;
import com.mpl.commons.zookeeper.ZKWatchEvent;
import com.mpl.commons.zookeeper.ZKWatchListener;
import com.mpl.commons.zookeeper.ZKWatcher;
import com.mpl.commons.zookeeper.Zookeeper;
import com.mpl.services.missions.core.DailyMissionStrategies.StrategySelectorAndExecutor;
import com.mpl.services.missions.core.MissionCenter;
import com.mpl.services.missions.models.DailyMissionConfig;
import com.mpl.services.missions.models.MissionConfig;
import com.mpl.services.missions.models.ReactivationRewards;

import java.util.Arrays;
import java.util.List;

/**
 * Author: Kaustubh Bhoyar
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Configurations extends ConfigurationsBase {
    /******************************** Global Variables & Constants ********************************/
    private static final String ZK_ROOT_CONFIG_PATH = "/mpl/service-missions";
    private static final MLogger logger = new MLogger(Configurations.class);
    private static Configurations instance;

    private String myName;
    private String myId;
    private int myPort;

    private String dbUsername;
    private String dbPassword;
    private String dbConnectionString;

    private int accountsServicePort;
    private String accountsServiceHost;

    private int profileServicePort;
    private String profileServiceHost;

    private int kycServicePort;
    private String kycServiceHost;

    private int videoServicePort;
    private String videoServiceHost;

    private List<MissionConfig> missionConfigsV2;

    private ObjectNode redissonConfig;
    private JsonNode walkieTalkieConfig;

    private String defaultCountryId;

    private Boolean mqttEnabled;

    public static void initialize(){
        if(instance == null){
            ConfigProvider.register(Configurations.class, new RegistrationConfig(ZK_ROOT_CONFIG_PATH, "config",Boolean.FALSE));
            instance = ConfigProvider.get(Configurations.class);
            refreshDailyMissions();
        }
    }

    /**************************************** Constructors ****************************************/
    public static Configurations getInstance() { return instance; }

    private static void refreshDailyMissions() {
        StrategySelectorAndExecutor.resetValidators();
    }

    /************************************** Public Functions **************************************/
    public String getMyName() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName = myName;
    }

    public String getMyId() {
        return myId;
    }

    public void setMyId(String myId) {
        this.myId = myId;
    }

    public int getMyPort() {
        return myPort;
    }

    public void setMyPort(int myPort) {
        this.myPort = myPort;
    }

    public String getDbUsername() {
        return dbUsername;
    }

    public void setDbUsername(String dbUsername) {
        this.dbUsername = dbUsername;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public String getDbConnectionString() {
        return dbConnectionString;
    }

    public void setDbConnectionString(String dbConnectionString) {
        this.dbConnectionString = dbConnectionString;
    }

    public int getAccountsServicePort() {
        return accountsServicePort;
    }

    public void setAccountsServicePort(int accountsServicePort) {
        this.accountsServicePort = accountsServicePort;
    }

    public String getAccountsServiceHost() {
        return accountsServiceHost;
    }

    public void setAccountsServiceHost(String accountsServiceHost) {
        this.accountsServiceHost = accountsServiceHost;
    }

    public int getProfileServicePort() {
        return profileServicePort;
    }

    public void setProfileServicePort(int profileServicePort) {
        this.profileServicePort = profileServicePort;
    }

    public String getProfileServiceHost() {
        return profileServiceHost;
    }

    public void setProfileServiceHost(String profileServiceHost) {
        this.profileServiceHost = profileServiceHost;
    }

    public int getKycServicePort() {
        return kycServicePort;
    }

    public void setKycServicePort(int kycServicePort) {
        this.kycServicePort = kycServicePort;
    }

    public String getKycServiceHost() {
        return kycServiceHost;
    }

    public void setKycServiceHost(String kycServiceHost) {
        this.kycServiceHost = kycServiceHost;
    }

    public int getVideoServicePort() {
        return videoServicePort;
    }

    public void setVideoServicePort(int videoServicePort) {
        this.videoServicePort = videoServicePort;
    }

    public String getVideoServiceHost() {
        return videoServiceHost;
    }

    public void setVideoServiceHost(String videoServiceHost) {
        this.videoServiceHost = videoServiceHost;
    }

    public List<MissionConfig> getMissionConfigsV2() {
        return missionConfigsV2;
    }

    public void setMissionConfigsV2(List<MissionConfig> missionConfigsV2) {
        this.missionConfigsV2 = missionConfigsV2;
    }

    public JsonNode getWalkieTalkieConfig() {
        return walkieTalkieConfig;
    }

    public void setWalkieTalkieConfig(JsonNode walkieTalkieConfig) {
        this.walkieTalkieConfig = walkieTalkieConfig;
    }

    public Object getRedissonConfig() {
        return redissonConfig;
    }

    public String getDefaultCountryId() {
        return defaultCountryId;
    }

    public Boolean getMqttEnabled() {
        return mqttEnabled;
    }
}
