package com.mpl.services.missions.utils;

import java.util.Random;
import java.util.UUID;

/**
 * Author: Kaustubh Bhoyar
 */
public class GeneralUtils {
    /******************************** Global Variables & Constants ********************************/
    private static Random RANDOM = new Random();

    /**************************************** Constructors ****************************************/
    /************************************** Public Functions **************************************/
    public static String getUniqueId() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
    public static int randomInt() {
        return RANDOM.nextInt();
    }
    public static int randomInt(int max) {
        return RANDOM.nextInt(max);
    }
    public static int randomInt(int min, int max) {
        return min + RANDOM.nextInt(max - min);
    }

    /************************************* Core Private Logic *************************************/
    /************************************** Private Classes ***************************************/
}

