package com.mpl.services.missions.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mpl.commons.configs.ConfigProvider;
import com.mpl.commons.configs.ConfigurationsBase;
import com.mpl.commons.configs.RegistrationConfig;
import com.mpl.commons.lci.utils.CountryInfoUtil;
import com.mpl.commons.zookeeper.ZKWatchEvent;
import com.mpl.services.missions.core.MissionCenter;
import com.mpl.services.missions.models.DailyMissionConfig;
import com.mpl.services.missions.models.MissionConfig;
import com.mpl.services.missions.models.ReactivationRewards;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Anubhav Shukla
 */
public class CountryWiseConfig extends ConfigurationsBase {

    private List<MissionConfig> missionConfigs;
    private DailyMissionConfig dailyMissionConfigs;
    private List<String> validApkTypes;
    private Boolean reactivationRewardsActive;
    private List<ReactivationRewards> reactivationRewards;
    private static final String ZK_ROOT_CONFIG_PATH = "/mpl/service-missions/country-config";
    private String[] activeMissionIds;
    private List<String> mqttDisabledBuckets;
    private List<String> disabledMissionsBuckets;

    private static Map<String,CountryWiseConfig> appConfigMap = new HashMap<>();
    static List<String> countryCodeList = CountryInfoUtil.getCoutryCodes();

    public static void initialize(){
        if(appConfigMap.isEmpty()) {
            ConfigProvider.register(CountryWiseConfig.class, new RegistrationConfig(ZK_ROOT_CONFIG_PATH, "app-config", true));
            for(String countryCode : countryCodeList) {
                addAppConfigToMap(countryCode);
            }
            refreshMissions();
        }
    }

    public static void addAppConfigToMap(String countryCode){
        CountryWiseConfig appConfig = ConfigProvider.get(CountryWiseConfig.class,countryCode);
        appConfigMap.put(countryCode,appConfig);
    }

    public static CountryWiseConfig getInstance(String countryCode){
        return appConfigMap.get(countryCode);
    }

    public List<MissionConfig> getMissionConfigs() {
        return missionConfigs;
    }

    public void setMissionConfigs(List<MissionConfig> missionConfigs) {
        this.missionConfigs = missionConfigs;
    }

    public DailyMissionConfig getDailyMissionConfigs() {
        return dailyMissionConfigs;
    }

    public void setDailyMissionConfigs(DailyMissionConfig dailyMissionConfigs) {
        this.dailyMissionConfigs = dailyMissionConfigs;
    }

    public List<String> getValidApkTypes() {
        return validApkTypes;
    }

    public void setValidApkTypes(List<String> validApkTypes) {
        this.validApkTypes = validApkTypes;
    }

    public Boolean getReactivationRewardsActive() {
        return reactivationRewardsActive;
    }

    public void setReactivationRewardsActive(Boolean reactivationRewardsActive) {
        this.reactivationRewardsActive = reactivationRewardsActive;
    }

    public List<ReactivationRewards> getReactivationRewards() {
        return reactivationRewards;
    }

    public void setReactivationRewards(List<ReactivationRewards> reactivationRewards) {
        this.reactivationRewards = reactivationRewards;
    }

    public String[] getActiveMissionIds() {
        return activeMissionIds;
    }

    public List<String> getMqttDisabledBuckets() { return mqttDisabledBuckets; }

    public List<String> getDisabledMissionsBuckets() { return disabledMissionsBuckets; }

    @Override
    public void onChange(ZKWatchEvent zkWatchEvent) {
        try {
            logger.d("Refreshing Configurations");
            new ObjectMapper().readerForUpdating(this)
                    .readValue(zkWatcher.getData());
            refreshMissions();
            logger.d("New Config:", this);
        } catch (Exception e) {
            logger.e("Failed to load config from ZooKeeper", e);
        }
    }


    /************************************* Core Private Logic *************************************/
    private static void refreshMissions() {
        for(String countryCode : countryCodeList) {
            int index = 0;
            CountryWiseConfig countryWiseConfig = appConfigMap.get(countryCode);
            countryWiseConfig.activeMissionIds = new String[countryWiseConfig.missionConfigs.size()];
            MissionCenter.resetValidators();
            for (MissionConfig mc : countryWiseConfig.missionConfigs) {
                countryWiseConfig.activeMissionIds[index++] = mc.getId();
                if (mc.getId().startsWith("OPEN_URL_"))
                    MissionCenter.registerURLValidator(mc.getId());
            }
        }
    }


    public MissionConfig getMissionConfigById(String missionId) {
        for (MissionConfig mc : missionConfigs) {
            if (mc.getId().equals(missionId))
                return mc;
        }
        return null;
    }
}
