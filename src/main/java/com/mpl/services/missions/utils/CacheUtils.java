package com.mpl.services.missions.utils;

import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.models.RedisData;
import org.redisson.api.RBucket;

import java.util.concurrent.TimeUnit;

/**
 * Author: Srijan Singh
 */
public class CacheUtils {

    /******************************** Global Variables & Constants ********************************/
    private static final String MISSIONS_KEY_PREFIX = "ms:dm:";
    private static final MLogger logger = new MLogger(CacheUtils.class);
    private static final ResourceManager rm = ResourceManager.getInstance();
    private static final ObjectMapper mapper = new ObjectMapper();

    /************************************** Public Functions **************************************/

    public static <T> void addObject(String cacheKey, T object, int expiryInMinutes) {
        String redisKey = MISSIONS_KEY_PREFIX + cacheKey;
        try {
            RBucket<T> bucket = rm.getRedisson().getBucket(redisKey);
            bucket.set((T) mapper.writeValueAsString(object));
            bucket.expire(expiryInMinutes, TimeUnit.MINUTES);
            logger.i("Adding Redis Object for key", redisKey, "TTL:", expiryInMinutes, "mission config:", object);
        } catch (JsonProcessingException e) {
            logger.e("Error Processing JSON while adding Object for key : {}", redisKey, e);
        } catch (Exception e) {
            logger.e("Exception occurred while adding Object for key : {}", redisKey, e);
        }
    }

    public static <T> RedisData getMissionsObject(String cacheKey) {
        try {
            String redisKey = MISSIONS_KEY_PREFIX + cacheKey;
            deleteLogic(redisKey);
            String redisData = getObject(redisKey);
            if (!StringUtils.isNullOrEmpty(redisData)) {
                return mapper.readValue(redisData, RedisData.class);
            }
        } catch (Exception e) {
            logger.e("Error in getting Missions Object", e);
        }
        return null;
    }

    public static <T> T getObject(String cacheKey) {
        try {
            RBucket<T> bucket = rm.getRedisson().getBucket(cacheKey);
            if (bucket != null)
                return bucket.get();
        } catch (Exception e) {
            logger.e("Exception occurred while getting Object for key : {}", cacheKey, e);
        }
        return null;
    }

    private static void deleteLogic(String cacheKey) {
        try {
            //todo: this is added as a bug fix , delete after
            String cacheObject = getObject(cacheKey);
            if (!StringUtils.isNullOrEmpty(cacheObject)) {
                RedisData redisData = mapper.readValue(cacheObject, RedisData.class);
                if (redisData != null && System.currentTimeMillis() > redisData.getEndDate())
                    deleteObject(cacheKey);
            }
        } catch (Exception e) {
            logger.e("Error in delete Logic for cacheKey:", cacheKey, e);
        }
    }

    public static <T> void deleteObject(String cacheKey) {
        try {
            RBucket<T> bucket = rm.getRedisson().getBucket(cacheKey);
            if (bucket != null && bucket.remainTimeToLive() < 1) {
                bucket.deleteAsync();
            }
        } catch (Exception e) {
            logger.e("Exception occurred while Deleting Object for key : {}", cacheKey, e);
        }
    }

    public static <T> Boolean keyExists(String cacheKey) {
        try {
            long val = rm.getRedisson().getKeys().countExists(cacheKey);
            return val >= 1 ? Boolean.TRUE : Boolean.FALSE;

        } catch (Exception e) {
            logger.e("Exception occurred while checking Object for key : {}", cacheKey, e);
            return Boolean.FALSE;
        }
    }

    public static <T> T updateObject(String cacheKey, T object) {
        try {
            String redisKey = MISSIONS_KEY_PREFIX + cacheKey;
            RBucket<T> bucket = rm.getRedisson().getBucket(redisKey);
            if (bucket != null) {
                if (!bucket.get().equals(mapper.writeValueAsString(object))) {
                    long ttl = bucket.remainTimeToLive();
                    bucket.set((T) mapper.writeValueAsString(object));
                    if (ttl > 0) {
                        bucket.expire(ttl, TimeUnit.MILLISECONDS);
                        logger.i("Updating Redis Object for key", redisKey, "TTL:", ttl, "mission config:", object);
                    }
                }
            }
            else
                logger.e("Null Bucket Received for Redis Key", redisKey);
        } catch (Exception e) {
            logger.e("Error in updating Missions Object", e);
        }
        return null;
    }

    /************************************** Private Classes ***************************************/
}

