package com.mpl.services.missions.utils;

import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mpl.commons.configs.ConfigProvider;
import com.mpl.commons.configs.ConfigurationsBase;
import com.mpl.commons.configs.RegistrationConfig;
import com.mpl.commons.lci.utils.CountryInfoUtil;
import com.mpl.commons.zookeeper.ZKWatchEvent;
import com.mpl.services.missions.models.DailyMissionConfig;
import com.mpl.services.missions.models.RedisData;
import com.mpl.services.missions.models.SegmentedConfigurationsDTO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * Author: Srijan Singh
 */
@Slf4j
@Data
public class CountryWiseSegmentedConfigs extends ConfigurationsBase {

    private static final String ZK_ROOT_CONFIG_PATH = "/mpl/service-missions/country-config";
    static List<String> countryCodeList = CountryInfoUtil.getCoutryCodes();
    private static Map<String, CountryWiseSegmentedConfigs> segmentConfigMap = new HashMap<>();
    Map<String, SegmentedConfigurationsDTO> segmentMap;
    List<String> enabledSegments = new ArrayList<>();
    Set<String> existingSegments = new HashSet<>();

    public static void initialize() {
        if (segmentConfigMap.isEmpty()) {
            ConfigProvider.register(CountryWiseSegmentedConfigs.class,
                    new RegistrationConfig(ZK_ROOT_CONFIG_PATH, "segmentation", true));
            for (String countryCode : countryCodeList) {
                addAppConfigToMap(countryCode);
            }
        }
    }

    public static void addAppConfigToMap(String countryCode) {
        try {
            CountryWiseSegmentedConfigs segmentedConfigs = ConfigProvider.get(CountryWiseSegmentedConfigs.class, countryCode);
            segmentedConfigs.setExistingSegments(segmentedConfigs.getSegmentMap().keySet());
            segmentConfigMap.put(countryCode, segmentedConfigs);
        } catch (Exception e) {
            log.error("Error in adding Home country Config for Country{} Exception {}", countryCode, e);
        }
    }

    public static CountryWiseSegmentedConfigs getInstance(String countryCode) {
        return segmentConfigMap.get(countryCode);
    }

    public static SegmentedConfigurationsDTO getSegmentInstance(String countryCode, String segmentId) {
        return segmentConfigMap.get(countryCode).getSegmentMap().get(segmentId);
    }

    public static DailyMissionConfig getDailyMissionConfigOrDefault(String countryCode , String segmentId) {
        DailyMissionConfig config = CountryWiseConfig.getInstance(countryCode).getDailyMissionConfigs();
        if (!StringUtils.isNullOrEmpty(segmentId)) {
            SegmentedConfigurationsDTO configs = CountryWiseSegmentedConfigs
                    .getSegmentInstance(countryCode, segmentId);
            if (configs != null && configs.getDailyMissionConfigs() != null)
                config = configs.getDailyMissionConfigs();
        }
        return config;
    }



    /************************************* Core Private Logic *************************************/

    @Override
    public void onChange(ZKWatchEvent zkWatchEvent) {
        try {
            logger.d("Refreshing Configurations");
            new ObjectMapper().readerForUpdating(this)
                    .readValue(zkWatcher.getData());
            logger.d("New Config:", this);
        } catch (Exception e) {
            logger.e("Failed to load config from ZooKeeper", e);
        }
    }
}
