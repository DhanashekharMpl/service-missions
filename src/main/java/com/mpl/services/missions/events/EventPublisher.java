package com.mpl.services.missions.events;

import com.mpl.commons.logging.MLogger;
import com.mpl.commons.notification.publish.SNSEventPublisher;
import com.mpl.services.missions.events.models.EventBase;
import com.mpl.services.missions.events.models.InternalEvent;
import com.mpl.services.missions.events.models.InternalEventKey;
import org.json.JSONObject;

/**
 * Author: Srijan Singh
 */
public class EventPublisher {
    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(EventPublisher.class);

    /**************************************** Constructors ****************************************/
    /************************************** Public Functions **************************************/
    public static void publish(InternalEventKey key, EventBase payload) {
        try {
            SNSEventPublisher.publishEvent(
                    "int-service-missions",
                    new JSONObject(new InternalEvent(key, payload).toString()));
            //todo: remove this
            JSONObject jsonObject = new JSONObject(new InternalEvent(key, payload).toString());
            logger.d("Event Published :", jsonObject.toString());
        } catch (Throwable t) {
            logger.w("Failed to publish event", t);
        }
    }

    /************************************* Core Private Logic *************************************/
    /************************************** Private Classes ***************************************/
}