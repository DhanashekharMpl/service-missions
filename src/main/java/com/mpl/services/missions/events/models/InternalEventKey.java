package com.mpl.services.missions.events.models;

/**
 * Author: Srijan Singh
 */
public enum InternalEventKey {
    MS_USER_COMPLETED_MISSION,
    MS_USER_MISSIONS_ADDED
}
