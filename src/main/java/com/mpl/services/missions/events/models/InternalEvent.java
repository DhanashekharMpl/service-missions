package com.mpl.services.missions.events.models;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Author: Srijan Singh
 */
public class InternalEvent {
    /******************************** Global Variables & Constants ********************************/
    private String key;
    private long timestamp;
    private Object payload;

    /**************************************** Constructors ****************************************/
    public InternalEvent(InternalEventKey key, EventBase payload) {
        this.key = key.toString();
        this.payload = payload;
        this.timestamp = System.currentTimeMillis();
    }

    /************************************** Public Functions **************************************/
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (Exception e) {
            throw new RuntimeException("Failed to serialize event", e);
        }
    }
}

