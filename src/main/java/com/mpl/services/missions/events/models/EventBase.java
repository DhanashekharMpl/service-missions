package com.mpl.services.missions.events.models;

/**
 * Author: Srijan Singh
 */
public class EventBase {
    /******************************** Global Variables & Constants ********************************/
    private int userId;

    /**************************************** Constructors ****************************************/
    public EventBase() { }

    public EventBase(int userId) {
        this.userId = userId;
    }

    /************************************** Public Functions **************************************/
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
