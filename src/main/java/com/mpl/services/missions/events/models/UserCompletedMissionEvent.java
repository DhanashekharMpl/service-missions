package com.mpl.services.missions.events.models;

import com.amazonaws.util.StringUtils;

import java.math.BigDecimal;

/**
 * Author: Srijan Singh
 */
public class UserCompletedMissionEvent extends EventBase {
    Integer dayCount;
    String taskType;
    String taskName;
    Integer currentTask;
    Integer totalTask;
    String type;
    BigDecimal reward;
    String taskStatus;
    String segmentId;

    public UserCompletedMissionEvent(int userId, Integer dayCount, String taskType, String taskName, Integer currentTask,
                                     Integer totalTask, String type, BigDecimal reward, String taskStatus , String segmentId) {
        super(userId);
        this.dayCount = dayCount;
        this.taskType = taskType;
        this.taskName = taskName;
        this.currentTask = currentTask;
        this.totalTask = totalTask;
        this.type = type;
        this.reward = reward;
        this.taskStatus = taskStatus;
        this.segmentId = StringUtils.isNullOrEmpty(segmentId) ? "Default" : segmentId;
    }

    public Integer getDayCount() {
        return dayCount;
    }

    public void setDayCount(Integer dayCount) {
        this.dayCount = dayCount;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Integer getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(Integer currentTask) {
        this.currentTask = currentTask;
    }

    public Integer getTotalTask() {
        return totalTask;
    }

    public void setTotalTask(Integer totalTask) {
        this.totalTask = totalTask;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getReward() { return reward; }

    public void setReward(BigDecimal reward) {
        this.reward = reward;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getSegmentId() { return segmentId; }

    public void setSegmentId(String segmentId) { this.segmentId = segmentId; }
}
