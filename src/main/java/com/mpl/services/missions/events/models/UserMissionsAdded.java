package com.mpl.services.missions.events.models;

import com.amazonaws.util.StringUtils;
import lombok.Data;

/**
 * Author: Srijan Singh
 */
@Data
public class UserMissionsAdded extends EventBase {
    String segmentId;
    String countryCode;
    String apkType;
    String appType;

    public UserMissionsAdded(int userId, String segmentId, String countryCode, String apkType, String appType) {
        super(userId);
        this.segmentId = StringUtils.isNullOrEmpty(segmentId) ? "Default" : segmentId;
        this.countryCode = countryCode;
        this.apkType = apkType;
        this.appType = appType;
    }
}
