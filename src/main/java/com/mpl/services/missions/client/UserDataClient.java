package com.mpl.services.missions.client;

import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.userdata.grpc.BasicProfile;
import com.mpl.services.userdata.grpc.GetBasicProfileRequest;
import com.mpl.services.userdata.grpc.GetBasicProfileResponse;
import com.mpl.services.userdata.grpc.UserDataServiceGrpc;

/**
 * Author: Anubhav Shukla
 */
public class UserDataClient {


    private static final MLogger logger = new MLogger(UserDataClient.class);

    public static BasicProfile getBasicProfile(long userId) throws Exception {
        GetBasicProfileRequest request = GetBasicProfileRequest.newBuilder()
                .setUserId(userId)
                .build();
        try {
            UserDataServiceGrpc.UserDataServiceBlockingStub stub = ResourceManager.getInstance().getUserDataServiceBlocking();
            GetBasicProfileResponse response = stub.getBasicProfile(request);
            if (response != null && response.getProfile() != null) {
                return response.getProfile();
            }
            logger.i("GetBasicProfileResponse response {}", response);
        } catch (Exception e) {
            logger.e("Error getting profile for userId : ", userId, " ; Exception : ", e);
        }
        throw new Exception(
                "Failed to fetch BasicUserProfile from UserDataService"
        );
    }

}
