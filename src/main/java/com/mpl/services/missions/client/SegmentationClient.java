package com.mpl.services.missions.client;

import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.segmentationengine.grpc.GetSegmentsForUserRequest;
import com.mpl.services.segmentationengine.grpc.GetSegmentsForUserResponse;
import com.mpl.services.segmentationengine.grpc.SegmentEngineServiceGrpc;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Srijan Singh
 */
public class SegmentationClient {
    private static final MLogger logger = new MLogger(SegmentationClient.class);

    public static List<String> getSegmentsForUser(Integer userId) {
        try {
            SegmentEngineServiceGrpc.SegmentEngineServiceBlockingStub stub = ResourceManager.getInstance().getSegmentationServiceBlockingStub();
            GetSegmentsForUserRequest request = GetSegmentsForUserRequest.newBuilder()
                    .setUserId(userId)
                    .build();
            GetSegmentsForUserResponse response = stub.getSegmentsForUser(request);
            logger.i("Get Segments for User Response", response);
            return response.getSegmentList();
        } catch (Exception e) {
            logger.e("Error getting Segments for User", userId, e);
        }
        return null;
    }
}
