package com.mpl.services.missions.client;

import com.mpl.services.accounts.AccountsServiceError;
import com.mpl.services.accounts.TransactionResponse;
import com.mpl.services.accounts.UpdateBalanceRequest;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.userdata.grpc.BasicProfile;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Srijan Singh
 */
public class PaymentClient {

    private final ResourceManager resourceManager = ResourceManager.getInstance();

    public String transfer(String referenceType, String description, int userId, String currency, BigDecimal amount, String rsTransactionId) throws Exception {
        Map<Integer, String> bonusMap = new HashMap<>();
        bonusMap.put(userId, String.valueOf(amount));
        UpdateBalanceRequest creditRequest = UpdateBalanceRequest.newBuilder()
                .putAllUserAmount(bonusMap)
                .setTransactionType("CREDIT")
                .setMoneyType(currency)
                .setReferenceId(rsTransactionId)
                .setReferenceType(referenceType)
                .setDescription(description)
                .build();
        BasicProfile basicProfile = UserDataClient.getBasicProfile(userId);
        TransactionResponse response = resourceManager.getAccountsServiceBlocking(basicProfile.getCountryCode()).processUserBalance(creditRequest);
        if (response.getError().getReason() != AccountsServiceError.Reason.NONE) {
            throw new Exception("Credit request failed. Reason: "
                    + response.getError().getReason() +
                    " Message: " + response.getError().getMessage());
        }
        return ("" + response.getNewTransactionId());
    }
}
