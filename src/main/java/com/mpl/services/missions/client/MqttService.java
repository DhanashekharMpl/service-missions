package com.mpl.services.missions.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.models.DailyMissionConfig;
import com.mpl.services.missions.models.MqttPayload;
import com.mpl.services.missions.models.RedisData;
import com.mpl.services.missions.utils.Configurations;
import com.mpl.services.missions.utils.Constants;
import com.mpl.services.missions.utils.Constants.bucketEnum;
import com.mpl.services.missions.utils.Constants.mqttType;
import com.mpl.services.missions.utils.CountryWiseConfig;
import com.mpl.services.missions.utils.ResourceManager;
import com.mpl.services.mqtt.MqttSendRequest;
import com.mpl.services.mqtt.MqttSendResponse;
import com.mpl.services.mqtt.QosLevel;
import com.mpl.services.mqtt.ServiceType;

import java.util.List;

/**
 * Author: Srijan Singh
 */
public class MqttService {
    private static final MLogger logger = new MLogger(MqttService.class);
    private static final ObjectMapper mapper = new ObjectMapper();
    private final Configurations config = Configurations.getInstance();

    public void sendMqttNotification(int userId, String json) {
        if (config.getMqttEnabled()) {
            logger.i("Publish MQTT Events", userId);
            try {
                MqttSendRequest request = MqttSendRequest.newBuilder()
                        .setPayloadJson(json)
                        .setQosLevel(QosLevel.GAURANTEED_NO_DUP)
                        .addUserId(userId)
                        .setRetained(false)
                        .setType(ServiceType.NOTIFICATION)
                        .build();

                logger.d("===MQTT Request======", request);
                MqttSendResponse mqttSendResponse = ResourceManager.getInstance().getMqttServiceBlockingStub().sendMqttRequest(request);
                logger.d("===MQTT Response======", mqttSendResponse);
                logger.d("Publish MQTT Events SUCCESS", userId);
            } catch (Exception e) {
                logger.e("Publish MQTT Events ERROR", userId, e);
            }
        }
    }

    public void sendMqttHelper(Integer userId, RedisData.Bucket bucket, DailyMissionConfig.ZkBucket missionConfig,
                               DailyMissionConfig.ZkBucket nextMissionConfig, bucketEnum bucketEnum, mqttType mqttType, String countryCode) {
        if (checkValidBucket(countryCode, bucketEnum)) {
            MqttPayload.MqttDailyMissionDTO mqttDailyMissionDTO = new MqttPayload.MqttDailyMissionDTO
                    (missionConfig.getSubtaskTitle(), bucketEnum.toString(), missionConfig.getRewardAmount(), missionConfig.getRewardCurrency(),
                            missionConfig.getImageUrl(), bucket.getEventThreshold(), bucket.getEventCount(),countryCode);

            MqttPayload.MqttDailyMissionDTO nextMqttDailyMissionDTO = new MqttPayload.MqttDailyMissionDTO();
            if (nextMissionConfig != null) {
                nextMqttDailyMissionDTO = new MqttPayload.MqttDailyMissionDTO
                        (nextMissionConfig.getSubtaskTitle(), bucketEnum.toString(), nextMissionConfig.getRewardAmount(), nextMissionConfig.getRewardCurrency(),
                                nextMissionConfig.getImageUrl(), nextMissionConfig.getEventCount(), 0,
                                nextMissionConfig.getTitle(), nextMissionConfig.getSubTitle(), nextMissionConfig.getButtonText(),
                                Constants.missionStatusEnum.LOCKED.toString(), nextMissionConfig.getType(), nextMissionConfig.getExtraData(),countryCode);
            }

            MqttPayload mqttPayload = new MqttPayload(mqttType.toString(), mqttDailyMissionDTO, nextMqttDailyMissionDTO);

            try {
                String json = mapper.writeValueAsString(mqttPayload);
                MqttService mqttService = new MqttService();
                mqttService.sendMqttNotification(userId, json);
            } catch (JsonProcessingException e) {
                logger.d("Error in parsing mqtt payload", e);
            }
        }
    }

    private Boolean checkValidBucket(String countryCode, bucketEnum bucketEnum) {
        List<String> mqttDisabledBuckets = CountryWiseConfig.getInstance(countryCode).getMqttDisabledBuckets();
        if (mqttDisabledBuckets != null && mqttDisabledBuckets.size() > 0) {
            return !mqttDisabledBuckets.contains(bucketEnum.toString());
        }
        else return true;
    }
}
