package com.mpl.services.missions;

import com.mpl.commons.lci.CountryInfoProvider;
import com.mpl.commons.libpf.Pathfinder;
import com.mpl.commons.libpf.grpc.PFServer;
import com.mpl.commons.libpf.grpc.PFServerBuilder;
import com.mpl.commons.logging.MLogger;
import com.mpl.commons.notification.NotificationManager;
import com.mpl.services.missions.impl.DailyMissionsService;
import com.mpl.services.missions.impl.MissionsService;
import com.mpl.services.missions.models.SegmentedConfigurationsDTO;
import com.mpl.services.missions.utils.Configurations;
import com.mpl.services.missions.utils.CountryWiseConfig;
import com.mpl.services.missions.utils.CountryWiseSegmentedConfigs;
import com.mpl.services.missions.utils.ResourceManager;
import org.json.JSONObject;

/**
 * Author: Kaustubh Bhoyar
 */
public class MainClass {
    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(MainClass.class);

    /**************************************** Constructors ****************************************/
    public static void main(String[] args) {
        try {
            // Adds Network address cache TTL to take care of machine restarts
            java.security.Security.setProperty("networkaddress.cache.ttl" , "10");

            logger.d("Initializing Configurations");
            Configurations.initialize();
            Configurations config = Configurations.getInstance();
            logger.d("Config loaded", config);

            CountryInfoProvider.initialize();
            logger.d("Country Info Provider Initialized");

            logger.d("Initializing App Configurations");
            CountryWiseConfig.initialize();

            logger.d("Initializing Segment Wise App Configurations");
            CountryWiseSegmentedConfigs.initialize();

            logger.d("Initializing Resource Manager");
            ResourceManager.initialize();
            logger.d("Resource Manager Initialized");

            Pathfinder.initialize(config.getMyName(), config.getMyPort() - 10000);
            PFServer server = PFServerBuilder.forPort(config.getMyName(), config.getMyPort())
                    .addService(new MissionsService())
                    .addService(new DailyMissionsService())
                    .build();

            logger.d("Initializing Walkie Talkie");
            NotificationManager.init(new JSONObject(
                    config.getWalkieTalkieConfig().toString()));
            logger.d("Walkie Talkie Initialized");

            server.start();
            logger.d("Service started on", config.getMyPort());
            server.awaitTermination();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /************************************** Public Functions **************************************/
    /************************************* Core Private Logic *************************************/
    /************************************** Private Classes ***************************************/
}
