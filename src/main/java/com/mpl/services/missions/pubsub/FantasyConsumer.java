package com.mpl.services.missions.pubsub;

import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.DailyMissionCentre;
import com.mpl.services.missions.core.DailyMissionStrategies.StrategySelectorAndExecutor;
import com.mpl.services.missions.models.RedisData;
import com.mpl.services.missions.utils.CacheUtils;
import com.mpl.services.missions.utils.Configurations;
import com.mpl.services.missions.utils.Constants;

public class FantasyConsumer extends ConsumerBase {

    /******************************** Global Variables & Constants ********************************/
    public static final String USER_REGISTERED = "USER_REGISTERED";
    private static final MLogger logger = new MLogger(FantasyConsumer.class);
    private final Configurations config = Configurations.getInstance();

    /************************************** Public Functions **************************************/
    @Override
    protected String getConsumerName() {
        return "FantasyConsumer";
    }

    @Override
    protected String[] getEventsOfInterest() {
        return new String[]{
                USER_REGISTERED
        };
    }

    @Override
    protected int consumeInternal(long timestamp, String key, ObjectNode payload) throws Exception {
        try {
            if (payload.has("User ID")) {
                Integer userId = payload.get("User ID").asInt();
                RedisData redisData = refreshMissions(userId);
                if (redisData != null && redisData.getFantasy() != null &&
                        !StringUtils.isNullOrEmpty(redisData.getFantasy().getMissionId())) {
                    logger.d("Fantasy event Consumed : ", payload, "key:", key);
                    StrategySelectorAndExecutor strategy = new StrategySelectorAndExecutor();
                    strategy.select(redisData.getFantasy().getMissionId(), key, payload ,
                            Constants.bucketEnum.FANTASY,redisData.getSegmentId());
                }
            }
        } catch (Exception e) {
            logger.e("Error in Consuming Fantasy event", e);
        }
        return 200;
    }

    private RedisData refreshMissions(Integer userId) throws Exception {
        RedisData redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
        if (userId != null && userId > 0 && redisData != null) {
            DailyMissionCentre missionCenter = new DailyMissionCentre();
            Long now = System.currentTimeMillis();
            missionCenter.getDailyMissionsForUser(userId, redisData, now , null , null);
            redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
            return redisData;
        }
        return redisData;
    }
}
