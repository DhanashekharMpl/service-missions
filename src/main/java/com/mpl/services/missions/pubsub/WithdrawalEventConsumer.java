package com.mpl.services.missions.pubsub;

import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.DailyMissionCentre;
import com.mpl.services.missions.core.DailyMissionStrategies.StrategySelectorAndExecutor;
import com.mpl.services.missions.models.RedisData;
import com.mpl.services.missions.utils.CacheUtils;
import com.mpl.services.missions.utils.Constants;

/**
 * Author: Srijan Singh
 */
public class WithdrawalEventConsumer extends GenericConsumerBase {

    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(WithdrawalEventConsumer.class);
    private static final String WITHDRAWAL_EVENT = "USER_MONEY_WITHDRAWAL";

    /************************************** Public Functions **************************************/

    protected int consumeInternal(long timestamp, ObjectNode payload) {
        try {
            if (payload.has("userId")) {
                Integer userId = payload.get("userId").asInt();
                RedisData redisData = refreshMissions(userId);
                if (redisData != null && redisData.getWallet() != null &&
                        !StringUtils.isNullOrEmpty(redisData.getWallet().getMissionId())) {
                    logger.d("Withdrawal Event Consumed : ", payload);
                    StrategySelectorAndExecutor strategy = new StrategySelectorAndExecutor();
                    strategy.select(redisData.getWallet().getMissionId(), WITHDRAWAL_EVENT, payload, Constants.bucketEnum.WALLET, redisData.getSegmentId());
                }
            }
        } catch (Exception e) {
            logger.e("Error in Consuming Withdrawal event", e);
        }
        return 200;
    }

    @Override
    protected String getConsumerName() {
        return "WithdrawalEventConsumer";
    }

    private RedisData refreshMissions(Integer userId) throws Exception {
        RedisData redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
        if (userId != null && userId > 0 && redisData != null) {
            DailyMissionCentre missionCenter = new DailyMissionCentre();
            Long now = System.currentTimeMillis();
            missionCenter.getDailyMissionsForUser(userId, redisData, now, null, null);
            redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
            return redisData;
        }
        return redisData;
    }
}
