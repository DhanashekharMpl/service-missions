package com.mpl.services.missions.pubsub;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.commons.notification.subscribe.EventConsumer;
import org.json.JSONObject;

/**
 * Author: Srijan Singh
 */
public abstract class GenericConsumerBase implements EventConsumer {

    private static final MLogger logger = new MLogger(ConsumerBase.class);

    @Override
    public boolean consume(JSONObject jsonObject) {
        int responseCode;
        long startTime = System.currentTimeMillis();
        String consumerName = getConsumerName();
        try {
            long timestamp = jsonObject.getLong("timestamp");
            ObjectNode payload = new ObjectMapper().readValue(
                    jsonObject.toString(),
                    ObjectNode.class
            );
            responseCode = consumeInternal(timestamp, payload);
        } catch (Exception e) {
            responseCode = 500;
            logger.e("ERROR", consumerName, e);
        }
        logger.i("COMPLETE", consumerName, responseCode, (System.currentTimeMillis() - startTime));
        return responseCode == 200;
    }

    protected abstract int consumeInternal(long timestamp, ObjectNode payload) throws Exception;

    protected abstract String getConsumerName();
}
