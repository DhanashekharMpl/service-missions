package com.mpl.services.missions.pubsub;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.client.UserDataClient;
import com.mpl.services.missions.core.DailyMissionCentreAsync;
import com.mpl.services.missions.utils.Configurations;
import com.mpl.services.missions.utils.CountryWiseConfig;
import com.mpl.services.userdata.grpc.BasicProfile;

/**
 * Author: Srijan Singh
 */
public class NewUserEventConsumer extends ConsumerBase {
    /******************************** Global Variables & Constants ********************************/
    public static final String LOGIN_SUCESS = "AUTH_LOGIN_SUCCESS";
    private static final MLogger logger = new MLogger(NewUserEventConsumer.class);
    private final Configurations config = Configurations.getInstance();

    @Override
    protected String getConsumerName() {
        return "NewUserEventConsumer";
    }

    @Override
    protected String[] getEventsOfInterest() {
        return new String[]{
                LOGIN_SUCESS
        };
    }

    @Override
    protected int consumeInternal(long timestamp, String key, ObjectNode payload) {
        try {
            if (payload.has("userId") && payload.has("newUser") && (payload.has("apkType"))) {
                boolean isNewUserLogin = payload.get("newUser").asBoolean();
                Integer userId = payload.get("userId").asInt();
                BasicProfile basicProfile = UserDataClient.getBasicProfile(userId);
                String apkType = payload.get("apkType").asText();
                String appType = payload.has("appType") ? payload.get("appType").asText() : "";
                CountryWiseConfig countryWiseConfig = CountryWiseConfig.getInstance(basicProfile.getCountryCode());
                boolean isValidAppApk = countryWiseConfig.getValidApkTypes().contains(apkType)
                        || countryWiseConfig.getValidApkTypes().contains(appType);
                if (isNewUserLogin && isValidAppApk) {
                    logger.i("Received valid auth event for adding into missions for apk type:",
                            apkType, "appType:", appType, "user:", userId, "countryCode:", basicProfile.getCountryCode());
                    logger.d("New User event : ", payload, "key:", key);
                    DailyMissionCentreAsync dmc = new DailyMissionCentreAsync();
                    dmc.addNewUserToDailyMissions(userId, basicProfile.getCountryCode(),apkType,appType);
                }
                else if (isNewUserLogin) {
                    logger.i("Not adding user into Missions for apk type:",
                            apkType, "appType:", appType, "user:", userId, "countryCode:", basicProfile.getCountryCode());
                }
            }
            else
                logger.w("Validation failed for new user Event", payload);
        } catch (Exception e) {
            logger.e("Error in Consuming NewUserEvent", e);
        }
        return 200;
    }
}
