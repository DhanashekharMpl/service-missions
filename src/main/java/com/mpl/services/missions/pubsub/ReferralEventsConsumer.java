package com.mpl.services.missions.pubsub;

import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.DailyMissionCentre;
import com.mpl.services.missions.core.DailyMissionStrategies.StrategySelectorAndExecutor;
import com.mpl.services.missions.models.RedisData;
import com.mpl.services.missions.utils.CacheUtils;
import com.mpl.services.missions.utils.Constants;

/**
 * Author: Srijan Singh
 */
public class ReferralEventsConsumer extends ConsumerBase {
    private static final MLogger logger = new MLogger(ReferralEventsConsumer.class);
    private static final String USER_GOT_REFERRED = "RS_USER_GOT_REFERRED";

    @Override
    protected String getConsumerName() {
        return "ReferralEventsConsumer";
    }

    @Override
    protected String[] getEventsOfInterest() {
        return new String[]{
                USER_GOT_REFERRED
        };
    }

    @Override
    protected int consumeInternal(long timestamp, String key, ObjectNode payload) {
        try {
            if (payload.has("referrerId")) {
                Integer userId = payload.get("referrerId").asInt();
                RedisData redisData = refreshMissions(userId);
                if (redisData != null && redisData.getWallet() != null &&
                        !StringUtils.isNullOrEmpty(redisData.getWallet().getMissionId())) {
                    logger.d("Referral event Consumed : ", payload, "key:", key);
                    StrategySelectorAndExecutor strategy = new StrategySelectorAndExecutor();
                    strategy.select(redisData.getWallet().getMissionId(), key, payload, Constants.bucketEnum.WALLET,
                            redisData.getSegmentId());
                }
            }
        } catch (Exception e) {
            logger.e("Error in Consuming Referral event", e);
        }
        return 200;
    }

    private RedisData refreshMissions(Integer userId) throws Exception {
        RedisData redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
        if (userId != null && userId > 0 && redisData != null) {
            DailyMissionCentre missionCenter = new DailyMissionCentre();
            Long now = System.currentTimeMillis();
            missionCenter.getDailyMissionsForUser(userId, redisData, now, null, null);
            redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
            return redisData;
        }
        return redisData;
    }
}
