package com.mpl.services.missions.pubsub;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.commons.notification.subscribe.EventConsumer;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * @author aakash
 */
public abstract class ConsumerBase implements EventConsumer {
    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(ConsumerBase.class);

    private Set<String> eventsOfInterest;

    /**************************************** Constructors ****************************************/
    public ConsumerBase() {
        String[] eoi = getEventsOfInterest();
        if(eoi == null) eoi = new String[0];
        this.eventsOfInterest = new HashSet<>(Arrays.asList(eoi));
    }

    /************************************** Public Functions **************************************/
    @Override
    public boolean consume(JSONObject jsonObject) {
        int responseCode;
        String key = null;
        String consumerName = getConsumerName();
        long startTime = System.currentTimeMillis();
        String internalRequestId = UUID.randomUUID().toString().replace("-", "");
        try {
            long timestamp = jsonObject.getLong("timestamp");
            key = jsonObject.getString("key");
            if(!eventsOfInterest.contains(key)) {
                logger.d("REJECT", consumerName, internalRequestId, key, 400,
                        (System.currentTimeMillis() - startTime));
                return true;
            }
            logger.d("INIT", consumerName, internalRequestId, jsonObject);
            ObjectNode payload = new ObjectMapper().readValue(
                    jsonObject.getJSONObject("payload").toString(),
                    ObjectNode.class
            );

            responseCode = consumeInternal(timestamp, key, payload);
        } catch (Exception e) {
            responseCode = 500;
            logger.e("ERROR", consumerName, internalRequestId, key, e);
        }
        logger.d("COMPLETE", consumerName, internalRequestId, key, responseCode,
                (System.currentTimeMillis() - startTime));
        return responseCode == 200;
    }

    protected abstract String getConsumerName();
    protected abstract String[] getEventsOfInterest();
    protected abstract int consumeInternal(long timestamp, String key, ObjectNode payload) throws Exception;

    /************************************* Core Private Logic *************************************/
    /************************************** Private Classes ***************************************/

}
