package com.mpl.services.missions.pubsub;

import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.DailyMissionCentre;
import com.mpl.services.missions.core.DailyMissionStrategies.StrategySelectorAndExecutor;
import com.mpl.services.missions.models.RedisData;
import com.mpl.services.missions.utils.CacheUtils;
import com.mpl.services.missions.utils.Configurations;
import com.mpl.services.missions.utils.Constants;

/**
 * Author: Srijan Singh
 */
public class TournamentEventsConsumer extends ConsumerBase {

    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(TournamentEventsConsumer.class);
    private static final String EVENT_USER_PLAYED_GAME = "USER_PLAYED_GAME";
    private static final String EVENT_USER_REGISTERED = "USER_REGISTERED";
    private static final String EVENT_USER_PLAYED_RUMMY = "User Played Rummy";
    private static final String EVENT_USER_WON = "USER_WON";

    /************************************** Public Functions **************************************/
    @Override
    protected String getConsumerName() {
        return "TournamentEventsConsumer";
    }

    @Override
    protected String[] getEventsOfInterest() {
        return new String[]{
                EVENT_USER_REGISTERED,
                EVENT_USER_PLAYED_GAME,
                EVENT_USER_PLAYED_RUMMY,
                EVENT_USER_WON
        };
    }

    @Override
    protected int consumeInternal(long timestamp, String key, ObjectNode payload) throws Exception {
        try {
            if (payload.has("User ID")) {
                Integer userId = payload.get("User ID").asInt();
                RedisData redisData = refreshMissions(userId);
                StrategySelectorAndExecutor strategy = new StrategySelectorAndExecutor();
                if (redisData != null && redisData.getGames() != null &&
                        !StringUtils.isNullOrEmpty(redisData.getGames().getMissionId())) {
                    logger.d("Tournament event Consumed for Games bucket : ", payload, "key:", key);
                    strategy.select(redisData.getGames().getMissionId(), key, payload, Constants.bucketEnum.GAMES,
                            redisData.getSegmentId());
                }
                if (redisData != null && redisData.getRummy() != null &&
                        !StringUtils.isNullOrEmpty(redisData.getRummy().getMissionId())) {
                    logger.d("Tournament event Consumed for Rummy Bucket : ", payload, "key:", key);
                    strategy.select(redisData.getRummy().getMissionId(), key, payload, Constants.bucketEnum.RUMMY,
                            redisData.getSegmentId());
                }
            }
        } catch (Exception e) {
            logger.e("Error in Consuming Tournament event", e);
        }
        return 200;
    }

    private RedisData refreshMissions(Integer userId) throws Exception {
        RedisData redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
        if (userId != null && userId > 0 && redisData != null) {
            DailyMissionCentre missionCenter = new DailyMissionCentre();
            Long now = System.currentTimeMillis();
            missionCenter.getDailyMissionsForUser(userId, redisData, now, null, null);
            redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
            return redisData;
        }
        return redisData;
    }
}
