package com.mpl.services.missions.pubsub;

import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mpl.commons.logging.MLogger;
import com.mpl.services.missions.core.DailyMissionCentre;
import com.mpl.services.missions.core.DailyMissionStrategies.StrategySelectorAndExecutor;
import com.mpl.services.missions.models.RedisData;
import com.mpl.services.missions.utils.CacheUtils;
import com.mpl.services.missions.utils.Constants;


public class DepositEventsConsumer extends GenericConsumerBase {

    /******************************** Global Variables & Constants ********************************/
    private static final MLogger logger = new MLogger(DepositEventsConsumer.class);
    private static final String DEPOSIT_EVENT = "ACCOUNT_DEPOSITED";

    /************************************** Public Functions **************************************/

    protected int consumeInternal(long timestamp, ObjectNode payload) {
        try {
            if (payload.has("userId")) {
                Integer userId = payload.get("userId").asInt();
                RedisData redisData = refreshMissions(userId);
                if (redisData != null && redisData.getWallet() != null &&
                        !StringUtils.isNullOrEmpty(redisData.getWallet().getMissionId())) {
                    logger.d("Deposit Event Consumed : ", payload);
                    StrategySelectorAndExecutor strategy = new StrategySelectorAndExecutor();
                    strategy.select(redisData.getWallet().getMissionId(), DEPOSIT_EVENT, payload,
                            Constants.bucketEnum.WALLET, redisData.getSegmentId());
                }
            }
        } catch (Exception e) {
            logger.e("Error in Consuming Deposit event", e);
        }
        return 200;
    }

    @Override
    protected String getConsumerName() {
        return "DepositEventConsumer";
    }

    private RedisData refreshMissions(Integer userId) throws Exception {
        RedisData redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
        if (userId != null && userId > 0 && redisData != null) {
            DailyMissionCentre missionCenter = new DailyMissionCentre();
            Long now = System.currentTimeMillis();
            missionCenter.getDailyMissionsForUser(userId, redisData, now, null, null);
            redisData = CacheUtils.getMissionsObject(String.valueOf(userId));
            return redisData;
        }
        return redisData;
    }
}
